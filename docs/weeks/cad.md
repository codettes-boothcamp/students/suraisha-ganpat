## 3D designing and 3D printing

3D design is the process of using software to create a mathematical representation of a 3-dimensional object or shape. 
3D design is used in a variety of industries to help artists shape, communicate, document, analyze, and share their ideas.

** 3D designing using Tinkercad**

Tinkercad can be used to 3D Design a model. In the christmas break we had the task to 3d design something. And I try to design a simple house.

![1](../img/cad/1.jpg)

Designing a part of our Final project:
- Search for Stl.files and try to redisgn in on tinkercad and let it print using AnyCubic Maxpro using 3D printer.

![2](../img/cad/2.jpg)

To practice more with 3D designing on Tinkercad, we got to links to watch and design it:

![3](../img/cad/3.jpg)


**Fusion 360**

Install fusion 360 and learn how to work with it. For this we got to links to practice with fusion 360.

![4](../img/cad/4.jpg)

We learn the shortcuts for fusion360:

- S=Shorts
- C=Circle
- R=Ractangle
- L=Line
- E=Exstrude

For fusion360 we must install inkspace as well.

Using fusion360 to design your final Project:

- Search for a design -> watch silhouette
- Make a screenshot
  ![5](../img/cad/5.jpg)
  
- Open in inkspace
- Import the screenshot to inkspace
- Erase unnecessary parts
- Save it as a svg files
- Import this file to fusion360
- Redesign in fusion 360

![6](../img/cad/6.jpg)

**Files**

- Tiinkercad 3D designing

- Fusion360

- Inkspace

**Reference**

<a href="myFile.js/" download>watch3ddesign</a> 

<a href="myFile.js/" download>inkspace</a> 









