## Hack O'mation 2020 Final project

The Internet of Things (IoT) is a computing concept that describes a  future where everyday physical objects will be connected to the Internet  
and be able to identify themselves to other devices, exchange data without  requiring human-to-human or human-to-computer interaction.

**Hack O’mation Format:**

- Pre-phase (4 weeks):
  Open Sessions to all participants:
  IoT intro, Brainstorm & Idea-building, Teambuilding, Pitching
  Embedded and UI programming sessions
  Idea Pitching to Jury (first scoring)

- Main Event (6-8 weeks):
  Prototype building in teams 
  Weekly support meetups (professionals in mechanics, electronics,  programming etc)

- Final presentation of projects november 2020~ 
  Top 3 projects selected by jury

**Hack O’mation Tech:**

- Control something, connect to it over the internet, read-out sensor(s) data and
- control actuator(s) preferrably using an API
- Hardware based around RaspberryPi and Arduino as controllers
- Use any software-tools/frameworks you want
- Sensor-pack available (40 sensors), and special sensors and/or actuators will be  ordered after approval of your project
- Proto-boards available; your custom PCB can be printed/etched or milled also
- Project must be feasible within time-frame (8-12 weeks) of challenge
- Support available for: - Electronics), Software/IO control, Manufacturing  of parts & Mechanics
- Product prototype MUST FUNCTION to be qualified for judging!

**Hack O'mation theme for 2020:** Sustainable Tourism Suriname and Health(covid-19 related)

Everything that solves tourism’s (in the interior) problems:
- The product
- Facilities & comfort
- Communication
- Safety & health
- Experience

Group Codettes bootcamp 2 participants: 
- Shewishka
- Anjessa
- Devika
- Drishti
- Disha

Group name: **leadHers**

Project Hack O'mation: **hydropower plant**

Electricity can be generated in different ways. One of the ways is through hydropower.
We are building a small bluewater Hydropower that can be use by everyone. It will be inexpensive so everybody can buy it.
This will be use to generate electricity for small devices when you are outdoor. This can be very usefull when you are at a place with no electricity. It has a magnet coupling so it
is easy to switch propellers.

**Logo**

![1](../img/hackfp/1.jpg)

**Problem**

General Problem: No Electricity outdoors.

For tourists: Most of the time when tourists go camping or there is no electricity. You start panicking about a low battery and want to charge your phone. And there is no electricity.


**Solutions**

Building a water tribune that will generate electricity through the flow of water. It can charge small devices easily. And it is nature friendly. 

**Features**

- 24 hour of power
- It stores energy

**Unique Values**

- Affordable IoT Solutions
- Convenience: It is easy to use
- It is small so easy to transport

**The working Process**

First we started to research about our product and how we can make it. We searched how it will look and what we will need to make it.
We put our mind to it and planned to use a DC motor to make our propeller rotate.

For making our waterturbine we used the following components:
- diodes
- 5v regulator
- dc motor
- charge circuit
- esp32(we are using the ttgo) for the dashboard

### TTGO Esp32

ESP32 is a series of low-cost, low-power system on a chip microcontrollers with integrated Wi-Fi and dual-mode Bluetooth. 
The ESP32 series employs a Tensilica Xtensa LX6 microprocessor in both dual-core and single-core variations and includes built-in antenna switches, 
RF balun, power amplifier, low-noise receive amplifier, filters, and power-management modules

**The features of the ESP32:**

Processors:
- CPU: Xtensa dual-core (or single-core) 32-bit LX6 microprocessor, operating at 160 or 240 MHz and performing at up to 600 DMIPS
- Ultra low power (ULP) co-processor

Memory: 520 KiB SRAM
Wireless connectivity:
- Wi-Fi: 802.11 b/g/n
- Bluetooth: v4.2 BR/EDR and BLE (shares the radio with Wi-Fi)

Peripheral interfaces:
- 12-bit SAR ADC up to 18 channels
- 2 × 8-bit DACs
- 10 × touch sensors (capacitive sensing GPIOs)
- 4 × SPI
- 2 × I²S interfaces
- 2 × I²C interfaces
- 3 × UART
- SD/SDIO/CE-ATA/MMC/eMMC host controller
- SDIO/SPI slave controller
- Ethernet MAC interface with dedicated DMA and IEEE 1588 Precision Time Protocol support
- CAN bus 2.0
- Infrared remote controller (TX/RX, up to 8 channels)
- Motor PWM
- LED PWM (up to 16 channels)
- Hall effect sensor
- Ultra low power analog pre-amplifier

Security:
- IEEE 802.11 standard security features all supported, including WFA, WPA/WPA2 and WAPI
- Secure boot
- Flash encryption
- 1024-bit OTP, up to 768-bit for customers
- Cryptographic hardware acceleration: AES, SHA-2, RSA, elliptic curve cryptography (ECC), random number generator (RNG)

Power management:
- Internal low-dropout regulator
- Individual power domain for RTC
- 5µA deep sleep current
- Wake up from GPIO interrupt, timer, ADC measurements, capacitive touch sensor interrupt

![3](../img/hackfp/3.jpg)


The headers were not connected to the Esp32. So we soldered the headers first.

![4](../img/hackfp/4.jpg)

![5](../img/hackfp/5.jpg)


**Setup Esp32 to Arduino IDE**

For the set up I need to install the ESP32 in my Arduino IDE.

Open Arduino IDE-> Then go to file-> And then preferences

![6](../img/hackfp/6.jpg)


Enter https://dl.espressif.com/dl/package_esp32_index.json into the “Additional Board Manager URLs” field as shown in the figure below. 
Then, click the “OK” button

![7](../img/hackfp/7.jpg)


After that open the Board Manager. For that go to tools-> Board -> Click on  Boards Managers

![8](../img/hackfp/8.jpg)


The Board Managers screen will open and then search for the ESP32 library to install it

![9](../img/hackfp/9.jpg)

After a few secondes the Library will be installed.
Now we can use the ESP32.



### ESP32 Basic Over The Air (OTA) Programming In Arduino IDE

A fantastic feature of any WiFi-enabled microcontroller like ESP32 is the ability to update its firmware wirelessly. This is known as Over-The-Air (OTA) programming.


**What is OTA programming in ESP32?**

The OTA programming allows updating/uploading a new program to ESP32 using Wi-Fi instead of requiring the user to connect the ESP32 to a computer via USB to perform the update.
OTA functionality is extremely useful in case of no physical access to the ESP module. It helps reduce the amount of time spent for updating each ESP module at the time of maintenance.

One important feature of OTA is that one central location can send an update to multiple ESPs sharing same network.
The only disadvantage is that you have to add an extra code for OTA with every sketch you upload, so that you’re able to use OTA in the next update.


*Ways To Implement OTA In ESP32*

There are two ways to implement OTA functionality in ESP32.
- Basic OTA – Over-the-air updates are sent through Arduino IDE.
- Web Updater OTA – Over-the-air updates are sent through a web browser.

Each one has its own advantages. You can implement any one according to your project’s requirement.


**3 Simple Steps To Use Basic OTA with ESP32**

- Install Python 2.7.x seriesThe first step is to install Python 2.7.x series in your computer.
- Upload Basic OTA Firmware SeriallyUpload the sketch containing OTA firmware serially. It’s a mandatory step, so that you’re able to do the next updates/uploads over-the-air.
- Upload New Sketch Over-The-AirNow, you can upload new sketches to the ESP32 from Arduino IDE over-the-air.


**Step 1: Install Python 2.7.x series**

In order to use OTA functionality, you need to install the Python 2.7.x version, if not already installed on your machine.

Go to [Python's official website](https://www.python.org/downloads/) and download 2.7.x (specific release) for Windows (MSI installer)
Open the installer and follow the installation wizard.
In Customize Python 2.7.X section, make sure the last option Add python.exe to Path is enabled.

Make sure your ESP32 is connected with your computer.


**Step 2: Upload OTA Routine Serially**

The factory image in ESP32 doesn’t have an OTA Upgrade capability. So, you need to load the OTA firmware on the ESP32 through serial interface first.
It’s a mandatory step to initially update the firmware, so that you’re able to do the next updates/uploads over-the-air.

The ESP32 add-on for the Arduino IDE comes with a OTA library & BasicOTA example. 
For the basicOTA example gor File
You can access it through File -> Examples -> ArduinoOTA -> BasicOTA.

![10](../img/hackfp/10.jpg)


The following code should load. But, before you head for uploading the sketch, you need to make some changes to make it work for you. 
You need to modify the following two variables with your network credentials, so that ESP32 can establish a connection with existing network.

```
const char* ssid = "telew_f28";
const char* password = "(your code)";
```

The BasicOTA code:

```
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

const char* ssid = "..........";
const char* password = "..........";

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  ArduinoOTA.handle();
}
```



After that I uploaded my sketch to the ESP32> Before uploading make sure you select you port. For that go to tools and select your port

While uploading I got an error:

![11](../img/hackfp/11.jpg)

I research my error and it means that it failed to connect to the board so I had to press my boot/flash bottton on my ESP32 while uploading.

If everything is done properly, open the Serial Monitor at a baud rate of 115200. And press the EN button on ESP32. 
If everything is OK, it will output the dynamic IP address obtained from your router. Note it down

![12](../img/hackfp/12.jpg)

Now we can use the ESP32 On The Air.



### Display a Text On The Oled with OTA

I wanted to try display a text on the Oled and I did some research. I first downloaded a file that I used for the Oled. I search for 

ESP32-OLED0.96-ssd1306 and downloaded it.

![13](../img/hackfp/13.jpg)

I added this file in my Arduino Folder that is in my Documents.


After adding the File I open it a
![14](../img/hackfp/14.jpg)


Before Uploading the sketch I need to install the library for the Oled. I had already installed the library, but I will put the names for the library that I used.

To install the library for the OLED display I clicked on Sketch-> Include Library-> Manage Library

![15](../img/hackfp/15.jpg)


Then I searched for SSD1306 Library and installed it:

![16](../img/hackfp/16.jpg)


I wanted the Oled to work over OTA. So I had to Combine the SSD1306SimpleDemo Sketch into my BasicOtA Sketch. This is the same sketch I used to blink the LED.
I combined to sketches into one.

*The complete sketch for the Oled with OTA*

```
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include "images.h"

const char* ssid = "telew_f28";
const char* password = "your password";

//variabls for blinking an LED with Millis
const int led = 16; // ESP32 Pin to which onboard LED is connected
unsigned long previousMillis = 0;  // will store last time LED was updated
const long interval = 1000;  // interval at which to blink (milliseconds)
int ledState = LOW;  // ledState used to set the LED

SSD1306  display(0x3c, 5, 4);

#define DEMO_DURATION 3000
typedef void (*Demo)(void);

int demoMode = 0;
int counter = 1;

void setup() {
  
  // Initialising the UI will init the display too.
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  pinMode(led, OUTPUT);
  
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void drawFontFaceDemo() {
    // Font Demo1
    // create more fonts at http://oleddisplay.squix.ch/
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "Hello world");
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 10, "Hello world");
    display.setFont(ArialMT_Plain_24);
    display.drawString(0, 26, "Shewishka");
}

void drawTextFlowDemo() {
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawStringMaxWidth(0, 0, 128,
      "Lorem ipsum\n dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore." );
}

void drawTextAlignmentDemo() {
    // Text alignment demo
  display.setFont(ArialMT_Plain_10);

  // The coordinates define the left starting point of the text
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 10, "Left aligned (0,10)");

  // The coordinates define the center of the text
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 22, "Center aligned (64,22)");

  // The coordinates define the right end of the text
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(128, 33, "Right aligned (128,33)");
}

void drawRectDemo() {
      // Draw a pixel at given position
    for (int i = 0; i < 10; i++) {
      display.setPixel(i, i);
      display.setPixel(10 - i, i);
    }
    display.drawRect(12, 12, 20, 20);

    // Fill the rectangle
    display.fillRect(14, 14, 17, 17);

    // Draw a line horizontally
    display.drawHorizontalLine(0, 40, 20);

    // Draw a line horizontally
    display.drawVerticalLine(40, 0, 20);
}

void drawCircleDemo() {
  for (int i=1; i < 8; i++) {
    display.setColor(WHITE);
    display.drawCircle(32, 32, i*3);
    if (i % 2 == 0) {
      display.setColor(BLACK);
    }
    display.fillCircle(96, 32, 32 - i* 3);
  }
}

void drawProgressBarDemo() {
  int progress = (counter / 5) % 100;
  // draw the progress bar
  display.drawProgressBar(0, 32, 120, 10, progress);

  // draw the percentage as String
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 15, String(progress) + "%");
}

void drawImageDemo() {
    // see http://blog.squix.org/2015/05/esp8266-nodemcu-how-to-create-xbm.html
    // on how to create xbm files
    display.drawXbm(34, 14, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
}

Demo demos[] = {drawFontFaceDemo, drawTextFlowDemo, drawTextAlignmentDemo, drawRectDemo, drawCircleDemo, drawProgressBarDemo, drawImageDemo};
int demoLength = (sizeof(demos) / sizeof(Demo));
long timeSinceLastModeSwitch = 0;


void loop() {
  ArduinoOTA.handle();
    
//loop to blink without delay
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
  // save the last time you blinked the LED
  previousMillis = currentMillis;
  // if the LED is off turn it on and vice-versa:
  ledState = not(ledState);
  // set the LED with the ledState of the variable:
  digitalWrite(led,  ledState);
  }
  
    // clear the display
  display.clear();
  // draw the current demo method
  demos[demoMode]();

  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(10, 128, String(millis()));
  // write the buffer to the display
  display.display();

  if (millis() - timeSinceLastModeSwitch > DEMO_DURATION) {
    demoMode = (demoMode + 1)  % demoLength;
    timeSinceLastModeSwitch = millis();
  }
  counter++;
  delay(10);
}
```

After doing this I selected my board:*ESP32 Dev Module* and I selected my Port. Then I compiled and uploaded my Sketch. 
While uploading I got alot of errors in my code so I had to reorder my code till it was fine to upload. 

Then finally I uploaded my code. And I was glad it worked.

![17](../img/hackfp/17.jpg)


I used this example to see if the Oled works. After this I used the same code and recode it for the Voltage of the battery.



### Battery Voltage and Status on OLED

After testing everything I can use the same steps for displaying the battery voltage and the status. I used the code for the Oled that I tried before. And recode it.

I managed to display the voltage on the Oled display

*The code I used*

```
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include "images.h"
#include "U8glib.h"

U8GLIB_SSD1306_ADAFRUIT_128X64 u8g(9, 10, 15, 14);  

const uint8_t blue = 2;
const uint8_t vbatPin = 35;
float VBAT;  // battery voltage from ESP32 ADC read

const char* ssid = "IOTLAB_GUEST";
const char* password = "IoT@Guest";

//variabls for blinking an LED with Millis
const int led = 16; // ESP32 Pin to which onboard LED is connected
unsigned long previousMillis = 0;  // will store last time LED was updated
const long interval = 1000;  // interval at which to blink (milliseconds)
int ledState = LOW;  // ledState used to set the LED

SSD1306  display(0x3c, 5, 4);

#define DEMO_DURATION 3000
typedef void (*Demo)(void);

int demoMode = 0;
int counter = 1;

void setup() 
{
  Serial.begin(115200);
  pinMode(blue, OUTPUT);
  pinMode(vbatPin, INPUT);

  pinMode (16, OUTPUT);
  
  digitalWrite (16, LOW); // set GPIO16 low to reset OLED
  delay (50);
  digitalWrite (16, HIGH); // while OLED is running, GPIO16 must go high
  
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
}
char string[25];
void drawFontFaceDemo(float Vbat) {
    // Font Demo1
    // create more fonts at http://oleddisplay.squix.ch/
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "Battery");
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 10, "Monitoring");
    display.setFont(ArialMT_Plain_24);
    itoa(Vbat,string,10);
    sprintf(string,"%7.5f",Vbat);
    display.drawString(0, 26, string);
}

void draw(void) {
  // graphic commands to redraw the complete screen should be placed here 
  u8g.setFont(u8g_font_unifont);
  //u8g.setFont(u8g_font_osb21);
  u8g.drawFrame(99,0,27,10);
  u8g.drawBox(101,2,23,6);
  u8g.drawBox(126,3,2,4);

}


void percentage(void) {

  u8g.getMode() == U8G_MODE_BW;
    u8g.setColorIndex(1);         // pixel on
  }

void loop() 
{
  digitalWrite(blue, 1);
  delay(100);
  digitalWrite(blue, 0);
  delay(100);
  digitalWrite(blue, 1);
  delay(100);
  
  // Battery Voltage
  VBAT = (float)(analogRead(vbatPin)) / 4095*2*3.3*1.1;
  /*
  The ADC value is a 12-bit number, so the maximum value is 4095 (counting from 0).
  To convert the ADC integer value to a real voltage you’ll need to divide it by the maximum value of 4095,
  then double it (note above that Adafruit halves the voltage), then multiply that by the reference voltage of the ESP32 which 
  is 3.3V and then vinally, multiply that again by the ADC Reference Voltage of 1100mV.
  */
  Serial.println("Vbat = "); Serial.print(VBAT); Serial.println(" Volts");
  display.clear();
  drawFontFaceDemo(VBAT);
   display.display();
  digitalWrite(blue, 0);
  delay(700);

  // picture loop
  u8g.firstPage();

}
```

After editig the I select my board and port for uploading. The board I used *TTGO Lora32-Oled V1*
Then I uploaded my code and see the voltage of the battery:

![18](../img/hackfp/18.jpg)


### Create a webserver with websockets using an ESP32  in Arduino for displaying ou Voltage and Battery percentage on a Dashboard


Rather than just host a simple web page, we’re going to build on the WebSocket idea. Let’s have our ESP32 be an access point (AP) and host a web page. 
When a browser requests that page, the ESP32 will serve it. As soon as the page loads, the client will immediately make a WebSocket connection back to the ESP32. 
That allows us to have fast control of our hardware connected to the ESP32.

The WebSocket connection is two-way. Whenever the page loads, it first inquires about the state of the LED from the ESP32. If the LED is on, the page will update a circle (fill in red) to reflect that. The circle on the page will be black if the LED is off. 
Then, whenever a user presses the “Toggle LED” button, the client will send a WebSocket packet to the ESP32, telling it to toggle the LED. 
This packet is followed by another request asking about the LED state so that the client can keep the browser updated with the state of the LED.

I am going to use the led that is on the Arduino. So I don't have to connect a led to my ESP32.


**Install SPIFFS Plugin**

The Serial Peripheral Interface Flash File System, or SPIFFS for short. It's a light-weight file system for microcontrollers with an SPI flash chip. 
SPIFFS let's you access the flash memory as if it was a normal file system like the one on your computer (but much simpler of course): you can read and write files, create folders ...

We need to use a special program to upload files over SPI.
Go to [Arduino ESP32fs plugin](https://github.com/me-no-dev/arduino-esp32fs-plugin) and follow the instructions to install the Arduino plugin.


The instructions: **Arduino ESP32 filesystem uploader**

Arduino plugin which packs sketch data folder into SPIFFS filesystem image, and uploads the image to ESP32 flash memory.

**Installation**

- Make sure you use one of the supported versions of Arduino IDE and have ESP32 core installed.
- Download the tool archive from [Release page](https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/tag/1.0)

  ![19](../img/hackfp/19.jpg)
  
- In your Arduino sketchbook directory, create tools directory if it doesn't exist yet.
  In my Arduino directory, the tool directory was already created
- Unpack the tool into tools directory (by me the path look like <This Pc>/Documents/Arduino/tools/ESP32FS/tool/esp32fs.jar).
  
   ![20](../img/hackfp/20.jpg)

- Restart Arduino IDE

  To check if the plugin was successfully installed, open your Arduino IDE. Select your ESP32 board, go to Tools and check that you have the option “ESP32 Sketch Data Upload“.
  
  ![21](../img/hackfp/21.jpg)
  

**Usage**

- Open a sketch (or create a new one and save it).For this I opened a new sketch in Arduino IDE
  For this click on File-> Then New
  
   ![22](../img/hackfp/22.jpg)


Delete the code for the sketch and upload the code for the webserver


*The code*

```
#include <WiFi.h>
#include <SPIFFS.h>
#include <ESPAsyncWebServer.h>
#include <WebSocketsServer.h>


// Constants
const char *ssid = "PROFUSU";
const char *password =  "hacker";
const char *msg_toggle_led = "toggleLED";
const char *msg_get_led = "getLEDState";
const char *msg_get_BATV = "getBattVolt";
const char *msg_get_perc = "getBattPerc";
const int dns_port = 53;
const int http_port = 80;
const int ws_port = 1337;
const int led_pin = 15;
const int vbatPin = 33; 

// Globals
AsyncWebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(1337);
char msg_buf[10];
int led_state = 0;

// Battery globals
float VBAT =0;
float PBAT =0;
int battPerc=0;

/***********************************************************
 * Functions
 */

// Callback: receiving any WebSocket message
void onWebSocketEvent(uint8_t client_num,
                      WStype_t type,
                      uint8_t * payload,
                      size_t length) {

  // Figure out the type of WebSocket event
  switch(type) {

    // Client has disconnected
    case WStype_DISCONNECTED:
      Serial.printf("[%u] Disconnected!\n", client_num);
      break;

    // New client has connected
    case WStype_CONNECTED:
      {
        IPAddress ip = webSocket.remoteIP(client_num);
        Serial.printf("[%u] Connection from ", client_num);
        Serial.println(ip.toString());
      }
      break;

    // Handle text messages from client
    case WStype_TEXT:

      // Print out raw message
      Serial.printf("[%u] Received text: %s\n", client_num, payload);

      // Toggle LED
      if ( strcmp((char *)payload, "toggleLED") == 0 ) {
        led_state = led_state ? 0 : 1;
        Serial.printf("Toggling LED to %u\n", led_state);
        digitalWrite(led_pin, led_state);

      // Report the state of the LED
      } else if ( strcmp((char *)payload, "getLEDState") == 0 ) {
        sprintf(msg_buf, "%d", led_state);
        Serial.printf("Sending to [%u]: %s\n", client_num, msg_buf);
        webSocket.sendTXT(client_num, msg_buf);

      // Report the battery Voltage
      } else if ( strcmp((char *)payload, "getBattVolt") == 0 ) {
        sprintf(msg_buf, "%d", led_state);
        Serial.printf("Sending to [%u]: %s\n", client_num, msg_buf);
        webSocket.sendTXT(client_num, msg_buf);

      // Report battery percantage
      } else if ( strcmp((char *)payload, "getBattPerc") == 0 ) {
        sprintf(msg_buf, "%d", led_state);
        Serial.printf("Sending to [%u]: %s\n", client_num, msg_buf);
        webSocket.sendTXT(client_num, msg_buf);

      // Message not recognized
      } else {
        Serial.println("[%u] Message not recognized");
      }
      break;

    // For everything else: do nothing
    case WStype_BIN:
    case WStype_ERROR:
    case WStype_FRAGMENT_TEXT_START:
    case WStype_FRAGMENT_BIN_START:
    case WStype_FRAGMENT:
    case WStype_FRAGMENT_FIN:
    default:
      break;
  }
}

// Callback: send homepage
void onIndexRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/index.html", "text/html");
}
// Callback: send jquery
void onJQRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/jquery.js", "text/js");
}

// Callback: send stylonCSSRequeste sheet
void onCSSRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/style.css", "text/css");
}

// Callback: send 404 if requested file does not exist
void onPageNotFound(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(404, "text/plain", "Not found");
}



/***********************************************************
 * Main
 */

void setup() {
  // Init LED and turn off
  pinMode(led_pin, OUTPUT);
  digitalWrite(led_pin, LOW);

  // Start Serial port
  Serial.begin(115200);

  // Make sure we can read the file system
  if( !SPIFFS.begin()){
    Serial.println("Error mounting SPIFFS");
    while(1);
  }

  // Start access point
  WiFi.softAP(ssid, password);

  // Print our IP address
  Serial.println();
  Serial.println("AP running");
  Serial.print("My IP address: ");
  Serial.println(WiFi.softAPIP());

  // On HTTP request for root, provide index.html file
  server.on("/", HTTP_GET, onIndexRequest);

  // On HTTP request for style sheet, provide style.css
  server.on("/style.css", HTTP_GET, onCSSRequest);

    // On HTTP request for style sheet, provide style.css
  server.on("/jquery.js", HTTP_GET, onJQRequest);

  // Handle requests for pages that do not exist
  server.onNotFound(onPageNotFound);

server.on("/voltage", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", String(VBAT,2));
});
server.on("/percentage", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", String(PBAT,2));
});
server.on("/img", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(SPIFFS, "/logo.jpg", "image/jpg");
});

  // Start web server
  server.begin();

  // Start WebSocket server and assign callback
  webSocket.begin();
  webSocket.onEvent(onWebSocketEvent);
  
}

void loop() {
  
  // Look for and handle WebSocket data
  webSocket.loop();
  VBAT = (float)(analogRead(vbatPin)) / 4095*2*3.3*1.1;
  PBAT = ((VBAT-3)/0.9)*100;
  if(PBAT >100){
    PBAT =100;
  }
}
```

After putting the webserver code. Save it and give it a name "webserver"

![23](../img/hackfp/23.jpg)

After putting the webserver code. Save it and give it a name "webserver"
  
![24](../img/hackfp/24.jpg)
  
- After saving it go back to Arduino IDE and then go to sketch directory (choose Sketch > Show Sketch Folder). Make sure you see where the sketch folder is.
  Mine is in This Pc-> Documents-> Arduino-> webserver folder

  ![25](../img/hackfp/25.jpg)

- In the webserver folder create another folder and name it "data". This folder is needed to put the files in it for the filesystem upload.
  
  ![26](../img/hackfp/26.jpg)


**Install Arduino Libraries**

Now I need libraries to let the webserver and websockets work.

I just donloaded these three libraries:

- [AsyncTCP](https://github.com/me-no-dev/AsyncTCP)
  
  ![27](../img/hackfp/27.jpg)


- [ESPAsyncWebServer](https://github.com/me-no-dev/ESPAsyncWebServer)

- [arduinoWebSockets](https://github.com/Links2004/arduinoWebSockets)


Now use the same Webserver code and take note of the SSID and password. We will need these to connect our phone/computer to the ESP32’s AP.

```
const char *ssid = "ESP32-AP";
const char *password = "letmeinplz";
```

You can enter your own password. "Just remember it"

After editing the code, I included the libraries that I downloaden

For this go to Sketch-> Include Library-> Add Zip library

![28](../img/hackfp/28.jpg)

Do this for all the three libraries

I then uploaded my code to my ESP32 by pressing on the boot button that is on my ESP32.

I succesfully uploaded my code.

![29](../img/hackfp/29.jpg)



Open a serial monitor with a baud rate of 115200. 

![30](../img/hackfp/30.jpg)


You should see the IP address of the ESP32 printed to the screen. 
When you run the Arduino soft access point software on the ESP32, the default IP address is 192.168.4.1.

![30](../img/hackfp/31.jpg)


**Webpage Code**

For the webpage code I created a file in the data folder that I created before in the webserver folder.
Go in the data folder and create a file. Name it index.html. In this data folder you can also add the CSS file. I didn't do that just to make it simple.

![32](../img/hackfp/32.jpg)


After created de index.html. Open the index.html with notepad or any editer. 

Edit this code in index.html

```
<html>
  <head>
  <title>Display Battery Status using HTML and CSS</title>
      <link href="style.css" rel="stylesheet" type="text/css">  
	  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>

<body>
	</div>
	<p>
      <h2 class="text-align:left "> Battery Status  </h2>
	</p>

	<link href='https://fonts.googleapis.com/css?family=Josefin+Slab' rel='stylesheet' type='text/css'>
	<div class="volt">
		<ul class="meter">
			<li class="low"></li>
			<li class="normal"></li>
			<li class="high"></li>
		</ul>

		<div class="dial">
				<div class="inner">
					<div class="arrow">
					</div>
				</div>			
		</div>

		<div class="value">
			0 V
		</div>

	</div>
	<div class="perc">
		<ul class="meter">
			<li class="low"></li>
			<li class="normal"></li>
			<li class="high"></li>
		</ul>

		<div class="dial">
				<div class="inner">
					<div class="arrow">
					</div>
				</div>			
		</div>

		<div class="value">
			0%
		</div>
	</div>

		<script type="text/javascript" src="jquery.js"></script>

	<script>
	$( document ).ready(function() {
		var dialV = $(".volt .dial .inner");
		var dialP = $(".perc .dial .inner");
		var gauge_volt= $(".volt .value");
		var gauge_perc= $(".perc .value");
		let endpointV = '/voltage';
		let endpointP = '/percentage'

		function rotateDial()
		{
			var deg = 0;
			var volt = 0;
			var perc = 0;
			// show voltage
			$.ajax({
			  url: endpointV,
			  contentType: "application/json",
			  dataType: 'json',
			  success: function(result){
				console.log(result);
				//var valueV = result;
				degV = 180*(result/12);
				gauge_volt.html(result + " Volt");
				dialV.css({'transform': 'rotate('+degV+'deg)'});
				dialV.css({'-ms-transform': 'rotate('+degV+'deg)'});
				dialV.css({'-moz-transform': 'rotate('+degV+'deg)'});
				dialV.css({'-o-transform': 'rotate('+degV+'deg)'}); 
				dialV.css({'-webkit-transform': 'rotate('+degV+'deg)'});
			  }
			})
			$.ajax({
			  url: endpointP,
			  contentType: "application/json",
			  dataType: 'json',
			  success: function(result){
				console.log(result);
				value = result;
				deg = (value * 177.5) / 100;
				gauge_perc.html(value + "%");
				dialP.css({'transform': 'rotate('+deg+'deg)'});
				dialP.css({'-ms-transform': 'rotate('+deg+'deg)'});
				dialP.css({'-moz-transform': 'rotate('+deg+'deg)'});
				dialP.css({'-o-transform': 'rotate('+deg+'deg)'}); 
				dialP.css({'-webkit-transform': 'rotate('+deg+'deg)'});
			  }
			})
		}
		setInterval(rotateDial, 2000);
	});
	</script>
				
</body>
</html>		
```



*The style.css code*

```
	html, body
	
text {
				 
   text-align:center;
   color: Green;
}
  

#centeredpage {
   width:100%;
   margin:0 auto;
   text-align:left;
   position:relative;
  
}
			{
				padding: 0;
				margin: 0;
			}

			body,
		
		    .dial
			{
				background-color: black;

			}

			.gauge
			{
				position: fixed;
				width: 500px;
				height: 500px;
				top: 100px;
				left: 50%;
				margin-left: -250px;
				border-radius: 100%;
				transform-origin: 50% 50%;
				-webkit-transform-origin: 50% 50%;
				-ms-transform-origin: 50% 50%;
				-webkit-transform: rotate(0deg);

			}

			.meter
			{
				margin: 0;
				padding: 0;
			}

			.meter > li
			{
				width: 250px;
				height: 250px;
				list-style-type: none;
				position: absolute;
				border-top-left-radius: 250px;
				border-top-right-radius: 0px;
				transform-origin:  100% 100%;;
				-webkit-transform-origin:  100% 100%;;
				-ms-transform-origin:  100% 100%;;
				transition-property: -webkit-transform;
				pointer-events: none;
			}

			.meter .low
			{
				transform: rotate(0deg); /* W3C */
				-webkit-transform: rotate(0deg); /* Safari & Chrome */
				-moz-transform: rotate(0deg); /* Firefox */
				-ms-transform: rotate(0deg); /* Internet Explorer */
				-o-transform: rotate(0deg); /* Opera */
				z-index: 8;
				background-color: #09B84F;
			}

			.meter .normal
			{
				transform: rotate(47deg); /* W3C */
				-webkit-transform: rotate(47deg); /* Safari & Chrome */
				-moz-transform: rotate(47deg); /* Firefox */
				-ms-transform: rotate(47deg); /* Internet Explorer */
				-o-transform: rotate(47deg); /* Opera */
				z-index: 7;
				background-color: #FEE62A;
			}

			.meter .high
			{
				transform: rotate(90deg); /* W3C */
				-webkit-transform: rotate(90deg); /* Safari & Chrome */
				-moz-transform: rotate(90deg); /* Firefox */
				-ms-transform: rotate(90deg); /* Internet Explorer */
				-o-transform: rotate(90deg); /* Opera */
				z-index: 6;
				background-color: #FA0E1C;
			}

			
			.dial,
			.dial .inner
			{
				width: 470px;
				height: 470px;
				position: relative;
				top: 10px;
				left: 5px;
				border-radius: 100%;
				border-color: purple;
				z-index: 10;
				transition-property: -webkit-transform;
				transition-duration: 1s;
				transition-timing-function: ease-in-out;
				transform: rotate(0deg); /* W3C */
				-webkit-transform: rotate(0deg); /* Safari & Chrome */
				-moz-transform: rotate(0deg); /* Firefox */
				-ms-transform: rotate(0deg); /* Internet Explorer */
				-o-transform: rotate(0deg); /* Opera */
			}

			.dial .arrow
			{
				width: 0; 
				height: 0; 
				position: absolute;
				top: 214px;
				left: 24px;
				border-left: 5px solid transparent;
				border-right: 5px solid transparent;
				border-bottom: 32px solid white;
				-webkit-transform: rotate(-88deg); /* Safari & Chrome */
				-moz-transform: rotate(88deg); /* Firefox */
				-ms-transform: rotate(88deg); /* Internet Explorer */
				-o-transform: rotate(88deg); /* Opera */

			}

			.gauge .value
			{
				font-family: 'Josefin Slab', serif;
				font-size: 50px;
				color: white;
				position: absolute;
				top: 142px;
				left: 45%;
				z-index: 11;
			}
			
			.volt .value
			{
				font-family: 'Josefin Slab', serif;
				font-size: 50px;
				color: white;
				position: absolute;
				top: 185px;
				left: 13%;
				z-index: 11;
			}
			.perc .value
			{
				font-family: 'Josefin Slab', serif;
				font-size: 50px;
				color: white;
				position: absolute;
				top: 700px;
				left: 15%;
				z-index: 11;
			}
			
			}
```

Save this and go the Arduino IDE. The ESP32 shoulde be plugged in.
Go to Tools-> Click ESP32 Sketch Data Upload

![33](../img/hackfp/33.jpg)


By clicking that the image will upload. The SPIFFS succesfully uploaded

![34](../img/hackfp/34.jpg)


**Run it!**

With the index.html file uploaded and the Arduino code running, you should be able to connect to the ESP32’s access point. 
Using the computer, search for open WiFi access points and connect to the one named ESP32-AP. When asked for a password, enter "hacker". We need to be connected to the ESP32 to open our dashboard.

![35](../img/hackfp/35.jpg)


After connected to the ESp32 I opened my dashboard and it looks like this

![36](../img/hackfp/36.jpg)


### Digital Mnaufacturing

We started designing our propeller and water turbine in OpenScad. It is a paramateric design and it was easy to scale it.

First we design how our waterturbine will look.

![39](../img/hackfp/39.jpg)


*e design different propellers for different uses:*

![40](../img/hackfp/40.jpg)

![41](../img/hackfp/41.jpg)

![42](../img/hackfp/42.jpg)

![43](../img/hackfp/43.jpg)


*We printed two propellers to see if both work*

![44](../img/hackfp/44.jpg)

![45](../img/hackfp/45.jpg)

![46](../img/hackfp/46.jpg)


*We printed our other materials that we needed for our waterturbine*

For the motor we used 2 pvc pipe coupling

![47](../img/hackfp/47.jpg)


*Designing in Thinkercad*

We designed the ring, the lid,the magnet motor holder, the stand for holding the waterturbine

![48](../img/hackfp/48.jpg)

![53](../img/hackfp/53.jpg)



After designing. We made a stl file and export it to cura to print it on a 3d printer.

![49](../img/hackfp/49.jpg)

![50](../img/hackfp/50.jpg)

![54](../img/hackfp/54.jpg)


The other part of the magnet holder that is connected to the propeller

![55](../img/hackfp/55.jpg)

![56](../img/hackfp/56.jpg)


*The ring and the magnet motor holder*

![51](../img/hackfp/51.jpg)

![52](../img/hackfp/52.jpg)


After designing and printing everything, we cut a piece of pipe to put the turbine in it and 2 pipe for the floats

![57](../img/hackfp/57.jpg)

![58](../img/hackfp/58.jpg)


After cuttting the pipes we cleaned it up and paint it

![59](../img/hackfp/59.jpg)

![60](../img/hackfp/60.jpg)

![61](../img/hackfp/61.jpg)


**The enclosure**

We designed ou enclosure in thinkercad

![62](../img/hackfp/62.jpg)


*Export stl fil to cura*

![63](../img/hackfp/63.jpg)


*On the 3d printer*

![64](../img/hackfp/64.jpg)


*The propeller came out really nice*

![65](../img/hackfp/65.jpg)


### Electronics Design


**Diode Bridge in Tinkercad**

We first made a diode Bridge. A diode bridge is an arrangement of four (or more) diodes in a bridge circuit configuration that provides the same polarity of output for either polarity of input.
When used in its most common application, for conversion of an alternating-current (AC) input into a direct-current (DC) output, it is known as a bridge rectifier

For making a diode Bridge. We need to do it step by step:
- Take 4 diodes for example the 1N4007 rectiifer diodes.
- Pick two diodes, make an "L" of their two ends marked with the white bands (cathodes)
- Just as above do it for the remaining two diodes, this time with their ends having no bands (anodes)

We used tinkercad to make our Diode bridge to see if it work. We also used a 5V regulator.

![68](../img/hackfp/68.jpg)


After testing it in Tinkercad we make a phyical diode bridge using a breadboard, diodes, some jumpers and a 5V regulator.

![67](../img/hackfp/67.jpg)


We tested our diode bridge with the dc motor

![66](../img/hackfp/66.jpg)


After tested the diode bridge. We connected the esp32 to 2 other batteries and connected the diode bridge to the batteries and the jack plug. We also put the usb port to charge the devices

![69](../img/hackfp/69.jpg)


### The product

After designing, printing, building our stack and made our electronic design we set everuthing together. We made sure that the DC motor is waterproof. So there won't go any water in it. We also made the floats for the waterturbine.

![70](../img/hackfp/70.jpg)

![71](../img/hackfp/71.jpg)


**The Poster**

![72](../img/hackfp/72.jpg)