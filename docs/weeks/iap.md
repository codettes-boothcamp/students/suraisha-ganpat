## Raspberry Pi 

The Raspberry Pi is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse. 
It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python. 
It’s capable of doing everything you’d expect a desktop computer to do, from browsing the internet and playing high-definition video, 
to making spreadsheets, word-processing, and playing games.

*Raspberry Pi 3 features and specifications*

1.CPU: Broadcom BCM2836 900MHz quad-core ARM Cortex-A7 processor
2.RAM: 1 GB SDRAM
3.USB Ports: 4 USB 2.0 ports
4.Network: 10/100 Mbit/s Ethernet
5.Power Ratings: 600 mA (3.0 W)
6.Power Source: 5V Micro USB
7.Size: 85.60 mm × 56.5 mm
8.Weight: 45 g

![1](../img/iap/1.jpg)


### Setting up Rpi 2-3

*Configuring your SD-Card for your RaspberryPi*
Insert a SD-Card in your SD-Card reader slot on your laptop and open win32diskmanager and look for the Wheezy-image on your laptop, which we will  use to configer the SD-Card.
This will write the image into the SD-Card.

![23](../img/iap/23.jpg)

2 important things  to check after configuring the SD Card with your PC.
After configuring the SD Card, you will find a Boot-drive on your PC. Open this and then,
Open cmdline.txt in notepad ++ and then hardcoded my adress on my RaspberryPi network: 192.168.1.153.
This is needed to install or work with putty. Go to your SD-card drive and make a new text file, name it SSh, without the text extention.
To allow putty to access the Raspberry Pi remotely.

![24](../img/iap/24.jpg)

Open the SSh file and enable everything:

![25](../img/iap/25.jpg)


### Nodejs

- Node.js is an open source server environment
- Node.js is free
- Node.js runs on various platforms (Windows, Linux, Unix, Mac OS X, etc.)
- Node.js uses JavaScript on the server

**What Can Node.js Do?**

- Node.js can generate dynamic page content
- Node.js can create, open, read, write, delete, and close files on the server
- Node.js can collect form data
- Node.js can add, delete, modify data in your database

**What is a Node.js File?**

- Node.js files contain tasks that will be executed on certain events
- A typical event is someone trying to access a port on the server
- Node.js files must be initiated on the server before having any effect
- Node.js files have extension ".js"

JavaScript is the programming Language for the web, it can update and change both HTML and CSS. It can calculate, manipulate and validate data.

[Node.js](https://auth0.com/blog/create-a-simple-and-stylish-node-express-app/)

**Install putty and winscp**

PuTTy is a free and open-source terminal emulator, serial console and network file transfer application. 
It supports several network protocols, including SCP, SSH, Telnet, rlogin, and raw socket connection. 
It can also connect to a serial port.

WinSCP is a free, open-source file-transfer application that uses File Transfer Protocol, 
Secure Shell File Transfer Protocol and Secure Copy Protocol for plain or secure file transfer

Both is working together: In Putty you must command line to make a folder and files. 
And in Winscp you see which file or folder you make


**Connect your Raspberry Pi**

Search for command prompt on your laptop and check if the raspberrry Pi is connected
Do this by typing Ping 192.168.1.153. This is the Ip adress for your Raspberry Pi

If it is connected you see a reply from the raspberry Pi

![3](../img/iap/3.jpg)

**Open Winscp**

![2](../img/iap/2.jpg)

**Open putty and login in putty and winscp**

- User: Pi for putty and winscp
- Password:raspberry for putty and winscp
- Ip adress of raspberry: 192.168.1.153

![4](../img/iap/4.jpg)

After you succesfully login, it opens like this

![5](../img/iap/5.jpg)

**Creat a nodejs folder structure/frame**

Basic linux commands:

- ls: list
- cd: to go in the folder/directory
- cd ..: to ga back from the folder/directory
- mkdir eg.folder name : make a folder/directory
- touch eg file name: make a file
- rm.file name: remove the file
- rmdir folder name: remove the folder

For example:

![6](../img/iap/6.jpg)

The structure that must be created:

![7](../img/iap/7.jpg)

![8](../img/iap/8.jpg)

Before installing nodemon, python, pip:
We must check the version first

![9](../img/iap/9.jpg)

To initialize my nodejs project with default setting command line
*npm init-Y*

![10](../img/iap/10.jpg)

**Creating an npm script to run application**

Nodemon is use to monitor my project source code and to automatically restart Node.js server when it changes.
Install nodemon as development dependency: *npm i-D nodemon*

![11](../img/iap/11.jpg)

This is equivalent of running:*npm install --save-dev nodemon*

**Integrating the Express Web Framework with Node.js**

To use the Express framework in my application install it as project dependency: *npm i express* (i means install)

![12](../img/iap/12.jpg)

Open the package.json file and change "test..." to " "dev": "nodemon ./index.js"

```
{
  "name": "project1",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "dev": "nodemon ./index.js"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "nodemon": "^2.0.2"
  },
  "dependencies": {
    "express": "^4.17.1"
  }
}

```
All npm packages contain a file, usually in the project root, called package.json - this file holds various metadata relevant to the project.
This file is used to give information to npm that allows it to identify the project as well as handle the project's dependencies.

Open the index.js file and edit the code:

```
/**
 *Required External Modules
 */

const express =require("express");
const path = require("path");

/**
 *App Variables
 */

const app = express();
const port = process.env.PORT || "8000";

/**
 *App Configuration
 */

app.use(express.static(path.join(__dirname, "public")));

/**
 *Routes Definitions
 */



/**
 *Server Activation
 */
app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});

```
Required express modules:
Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.
The path is initialize for working with files and path where later, templates like images or stylesheet can be edit.

App configuration:
This reverse to the public folder where the css file is that the application needs to use.

Server Activation:
Define to a server listening for incoming request on a port and to display a message to confirm it's listening.

Open index.html file and edit this basic code:

```
<html>
<body>
<h1>My website</h1>
</body>
</html>

```

Execute *dev* to test the script and run the app: *npm run dev*

![13](../img/iap/13.jpg)

Now visit http://localhost:8000/ to see your webpage

![14](../img/iap/14.jpg)



### Python

In technical terms, Python is an object-oriented, high-level programming language with integrated dynamic semantics primarily for web and app development

[Python](https://projects.raspberrypi.org/en/projects/python-web-server-with-flask/2)

Create a Python folder structure, similair to Node.js:

![15](../img/iap/15.jpg)

![16](../img/iap/16.jpg)

In the index.py file copy this code

```
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

```
- @app.route('/'): this determines the entry point; the / means the root of the website, so http://127.0.0.1:5000/
- def index(): this is the name you give to the route; this one is called index, because it is the index (or home page) of the website
- return 'Hello world': this is the content of the web page, which is returned when the user goes to this URL.

In the index.html file copy this code:
This is necessary to create the webpage:

```
<html>
<body>
<h1>My website</h1>
</body>
</html>

```
After running the program visit http://localhost:5000/ to see the webpage:

![17](../img/iap/17.jpg)


### Arduino Uno Serial Communication

A Serial communication is a connection between 2 devices over a cable.
In this case we are using The Arduino uno and the RaspberryPi.

Building a simple circuit using one Led:

![18](../img/iap/18.jpg)

Use this code to turn the led on:

```

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```
Open Arduino-> File -> Example -> Communication -> Open PhysicalPixel

![19](../img/iap/19.jpg)

Now compile and upload this code to the Arduino:

```
const int ledPin = 13; // the pin that the LED is attached to
int incomingByte;      // a variable to read incoming serial data into

void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // see if there's incoming serial data:
  if (Serial.available() > 0) {
    // read the oldest byte in the serial buffer:
    incomingByte = Serial.read();
    // if it's a capital H (ASCII 72), turn on the LED:
    if (incomingByte == 'H') {
      digitalWrite(ledPin, HIGH);
    }
    // if it's an L (ASCII 76) turn off the LED:
    if (incomingByte == 'L') {
      digitalWrite(ledPin, LOW);
    })
  }
}
```
 
**Make a serial connection between the Arduino and raspberry Pi**

Create another folder in Python folder and called it project2
In project2 folder create a file called arduino.py.

Open the arduino.py file and set the code in it:

```
import serial
import time
# Define the serial port and baud rate.
# Ensure the 'COM#' corresponds to what was seen in the Windows Device Manager

ser = serial.Serial('/dev/ttyACM0', 9600)   # ttyACM0 serial port for pi 2 #/dev/ttyAMA0 pi 3
def led_on_off():
    user_input = input("\n Type on / off / quit : ")
    if user_input =="on":
        print("LED is on...")
        time.sleep(0.1) 
        ser.write(b'H') 
        led_on_off()
    elif user_input =="off":
        print("LED is off...")
        time.sleep(0.1)
        ser.write(b'L')
        led_on_off()
    elif user_input =="quit" or user_input == "q":
        print("Program Exiting")
        time.sleep(0.1)
        ser.write(b'L')
        ser.close()
    else:
        print("Invalid input. Type on / off / quit.")
        led_on_off()

time.sleep(2) # wait for the serial connection to initialize

led_on_off()
```

The code explained:

First we import two libaries that we need for Python. This is the serial and the time libaries. 
The Serial libary was needed to make the serial connection between the 2 devices and the time libary was needed for the delay function in this code.
Further we devine the serial port and its bound rate, which we have put it to 9600 in this code.
We make a function named: led_on_off which we will call to turn the LED on or off. We have a user input to show on the screen with the text "\n Type on / off / quit : "
Then we use a If then Else statement to response to the user input.If it its put to "On" then we get the text that the LED is "on". Else we get the text "off". If the LED is put to off after a delay of 1 sec again.
The Arduino then sent a signal to the RaspberryPi to turn the LED On by converting it to b (bytes) for the "H". And "L" for off.

**Installing Arduino Uno to Raspberry Pi**

- Open project2 using putty
- Type type lsub to see a list of your usbports on which your Arduino UNO is connected
- Type mesg | grep tty to see the serial port for your RaspberryPi(/dev/ttyACM0  (for raspberry pi 3))
- Type sudo python arduino.py

![20](../img/iap/20.jpg)

![21](../img/iap/21.jpg)

The led is on:

![22](../img/iap/22.jpg)



### Html5/CSS/JavaScript

Building the IoT stack

**Html5**

HTML or HyperText Mark-up Language is a language to produce visual output on a website or modern web-app. The HTML5 standard, the latest version, was released by the W3C Consortium in 2014. 
W3C is a group that standardizes the world wide web languages and ++protocols to enable streamlined experience and integration between all browsers, and back-ends of participating members

*Links*

- [w3schools](https://www.w3schools.com/html/html5_new_elements.asp/)
- Guide to Hmtl5: [Html5](https://drive.google.com/file/d/0BwwL5RPpTcRNRWNTVWZPRFhGSGJ6Q2xrbkE1VE5KUDBib2JN/view)
- Fix your code: [fix code](https://beautifier.io/)
- Review how to create responsive website: [responsive](https://www.youtube.com/watch?v=eOG90Q8EfR)
- Resulting index.html with Semantic layout would be. Code cleaned via [code cleaned](http://hilite.me/)

*New Semantic/Structural Element for Html5*

```
Tag              Description
<article>        Defines an article in a document
<aside>          Defines content aside from the page content
<bdi>            Isolates a part of text that might be formatted in a different direction from other text outside it
<details>        Defines additional details that the user can view or hide
<dialog>         Defines a dialog box or window
<figcaption>     Defines a caption for a <figure> element
<figure>         Defines self-contained content
<footer>         Defines a footer for a document or section
<header>         Defines a header for a document or section
<main>           Defines the main content of a document
<mark>           Defines marked/highlighted text
<meter>          Defines a scalar measurement within a known range (a gauge)
<nav>            Defines navigation links
<progress>       Represents the progress of a task
<rp>             Defines what to show in browsers that do not support ruby annotations
<rt>             Defines an explanation/pronunciation of characters (for East Asian typography)
<ruby>           Defines a ruby annotation (for East Asian typography)
<section>        Defines a section in a document
<summary>        Defines a visible heading for a <details> element
<time>           Defines a date/time
<wbr>            Defines a possible line-break
```

*The basic Html5 page*

```
<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <title>Title MyPage (shows in browser window)</title>
</head>
<body>
    <!-- body content starts here -->
    Here is the visible Content of the page......
</body>
</html>
```

*Steps to create a website:*

- Determine Message/Goal/Target audience (why the website)

- Make a site structure / site map of pages and sections of website

![26](../img/iap/26.jpg)

- Setup website folder structure

```
    public: - CSS
	           - img
			   - button.css
			   -style.css
	        - img
			- js
			   -libaries(lib)
			      - Chart.js
				      - file Chart,js
			- index.html
			- indexcss.html(backup)
```

- Collect content

- Select the look & feel

- Build Mockup

![27](../img/iap/27.jpg)
   
- Target devices

- Supported Browsers

*Create Layout with Divs & Sections*

In the body create a sementic layout(Thisis the layout without styling):

```
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Forest deforstation</title>
    <link href="css/style.css" rel="stylesheet" />
    <meta name = "viewport" content = "width=device-width, initial-scale=1.0">
</head>

<body>
    <header Class = "mainHeader">
    	<img src="img/logo.png">
		<nav>
			<ul>
				<li><a href="#" Class="active">Home</a></li>
				<li><a href="#">About</a></li>
				<li><a href="#">Portfolio</a></li>
				<li><a href="#">Gallery</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
		</nav>
    </header>
    
    <div Class="mainContent">
	    <div Class="content">
    		<article class="articleContent">
	            <header>
	                <h2>First Article #1</h2>
	            </header>
	            
	            <footer>
	            	<p Class="post-info">Written by Super Woman</p>	            	
	            </footer>
	            <content>
	            	<p> Deforestation is the permanent removal of trees to make room for something besides forest. 
					   This can include clearing the land for agriculture or grazing, or using the timber for fuel, construction or manufacturing. </p>
			    </content>
    		</article> 
    		
    		<article Class="articleContent">
	            <header>
	                <h2>2nd Article #2</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>

	            <content>
					<p>This is the actual article content ... in this case a teaser for some other page ... read more</p>
        		</content>
    		</article> 
    		
	   </div>
	</div>
	
	<aside Class="top-sidebar">
		<article>
			<h2>Top sidebar</h2>
			<p>Lorum ipsum dolorum on top</p>
		</article>
	</aside>
	
	<aside Class="middle-sidebar">
		<article>
			<h2>Middle sidebar</h2>
			<p>Lorum ipsum doloru in the middle</p>
		</article>
	</aside>
	
	<aside Class="bottom-sidebar">
		<article>
			<h2>Bottom sidebar</h2>
			<p>Lorum ipsum dolorum at the bottom</p>
		</article>
	</aside>
	
	<footer Class="mainFooter">
		<p>Copyright &copy; <a href="#" title = " 
 MyDesign">mywebsite.com</a></p>
	</footer>
		
</body>

</html> 

```

The website looks like this without styling:

![28](../img/iap/28.jpg)



**CSS3**

CSS3 or  Cascading Style Sheets enables the separation of presentation and content, including layout, colors, and fonts.[3] 
To improve content accessibility, more flexibility and control of presentation characteristics, web pages share formatting by specifying the relevant CSS in a separate .css file

*Links:*

- Styling: [Styling](https://www.youtube.com/watch?v=x4q86IjJFag&feature=youtu.be)
- CSS tricks: [CSS tricks](https://css-tricks.com/almanac/)

Link the style-sheet
Create the style/style.css file and simply assign colors to each of the above mentioned sections and add the reference to this stylesheet inside your HTML page <head> section with the following tag:

```
<link href="css/style.css" rel="stylesheet" />
```

The code for CSS(This will give the website a style, like the color of background, setting article, aside on their place, etc)

```
/* 
	Stylesheet for:
	HTML5-CSS3 Responsive page
*/

/* body default styling */

body {
    /* border: 5px solid red; */
    background-image: url(img/bg.png);
    background-color: #87bac4;
    font-size: 87.5%;
    /* base font 14px */
    font-family: Arial, 'Lucinda Sans Unicode';
    line-height: 1.5;
    text-align: left;
    margin: 0 auto;
    width: 70%;
    clear: both;
}


/* style the link tags */
a {
    text-decoration: none;
}

a:link a:visited {
    color: #8B008B;
}

a:hover,
a:active {
    background-color: #87bac4;
    color: #FFF;
}


/* define mainHeader image and navigation */
.mainHeader img {
    width: 18%;
    height: auto;
    margin: 2% 0;
}

.mainHeader nav {
    background-color: #666;
    height: 40px;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainHeader nav ul {
    list-style: none;
    margin: 0 auto;
}

.mainHeader nav ul li {
    float: left;
    display: inline;
}

.mainHeader nav a:link,
mainHeader nav a:visited {
    color: #FFF;
    display: inline-block;
    padding: 10px 25px;
    height: 20px;
}

.mainHeader nav a:hover,
.mainHeader nav a:active,
.mainHeader nav .active a:link,
.mainHeader nav a:active a:visited {
    background-color: #8B008B;
    /* Color purple */
    text-shadow: none;
}

.mainHeader nav ul li a {
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}


/* style the contect sections */

.mainContent {
    line-height: 20px;
    overflow: none;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.content {
    width: 70%;
    float: left;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.articleContent {
    background-color: #FFF;
    padding: 3% 5%;
    margin-top: 2%;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.post-info {
    font-style: italic;
    color: #999;
    font-size: 85%;
}

.top-sidebar {
    width: 18%;
    margin: 1.5% 0 2% 2%;
    padding: 3% 5%;
    float: right;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.middle-sidebar {
    width: 18%;
    margin: 1.% 0 2% 2%;
    padding: 3% 5%;
    float: right;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.bottom-sidebar {
    width: 18%;
    margin: 1.5% 0 2% 2%;
    padding: 3% 5%;
    float: right;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter {
    width: 100%;
    height: 40px;
    float: left;
    border-color: #d99090;
    background-color:#666;
    margin: 2% 0;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter p {
    width: 92%;
    margin: 10px auto;
    color: #FFF;
}
```

For the logo:
Put an Image in the image folder that is in the Css folder.

The website now looks like this:*

![29](../img/iap/29.jpg)

*Page responsiveness to your website*

To build in page responsiveness, meaning the page will be displayed different for each device/screen dimension add this line to the <head> section.
Responsiveness allows the webpage to be viewed differently on devices like mobiles and Tablets. 
So we can alight layout sections different for devices based on the observed screen dimensions.

```
<meta name = "viewport" content = "width=device-width, initial-scale=1.0">
```

This line tells the browser that the width of the screen should be considered the "Full Width" of the page. 
Meaning no matter the width of the device you are on, whether on desktop or mobile. the website will follow the width of the device the user is on. You can read more about the viewport meta tag at W3Schools.

After this the whole aspect of responsiveness is writting in the CSS file. Strategy is to create the full blown page CSS first and after that create a section tag like as shown below and add display format specific styling. Just overrule the CSS tags u want different than standard.

@media only screen and (min-width:150px) and (max-width:600){ }

```
/* Add responsiveness overrules values for different screen resolution */

@media only screen and (min-width:150px) and (max-width:600) {
    .body {
        width: 95%;
        font-size: 95%;
    }
    .mainHeader img {
        width: 100%;
    }
    .mainHeader nav {
        height: 160px;
    }
    .mainHeader nav ul {
        padding-left: 0;
    }
    .mainHeader nav ul li {
        width: 100%;
        text-align: center;
    }
    .mainHeader nav a:link,
    mainHeader nav a:visited {
        padding: 10px 25px;
        height: 20px;
        display: block;
    }
    .content {
        width: 100%;
        float: left;
        margin-top: 2%;
    }
    .post-info {
        display: none;
    }
    .topContent,
    .bottomContent {
        background-color: #FFF;
        padding: 3% 5%;
        margin-top: 2%;
        margin-bottom: 4%;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
    }
    .top-sidebar,
    .middle-sidebar,
    .bottom-sidebar {
        width: 94%;
        margin: 2% 0 2% 0;
        padding: 2% 3%;
    }
}
```

**Libraries, Charts and Button**

*Add a basic linechart from chart.js to article to my website*

Download chart: [chart](https://sourceforge.net/projects/chartjs.mirror/)
Charts:[chart example](https://www.chartjs.org/)

- Download Chart.js and add this to Public/js/lib/Chart.js:
  Chart.js is a library that we must have to put a linechart otherwise it won't show.
  
- Add this code in index.html <head> to host the chart

```
  <script src="js/lib/Chart.js/Chart.js"></script>
  <script src="js/utils.js"></script>
```

- Add this code to article 2 in the <body>

```
<div style="width:100%; height:100%">
		<canvas id="canvas"></canvas>
		<button id="randomizeData">Randomize Data</button>
	    <button id="addDataset">Add Dataset</button>
		<button id="removeDataset">Remove Dataset</button>
		<button id="addData">Add Data</button>
		<button id="removeData">Remove Data</button>
</div>
```

- Add Javascript below </body>

```
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Forest deforstation</title>
    <link href="css/style.css" rel="stylesheet" />
	<link href="css/button.css" rel="stylesheet" />
	<script src="js/lib/Chart.js/Chart.js"></script>
    <script src="js/utils.js"></script>
    <meta name = "viewport" content = "width=device-width, initial-scale=1.0">
	
	
</head>

<body>
    <header Class = "mainHeader">
    	<img src="img/logo.png">
		<nav>
			<ul>
				<li><a href="#" Class="active">Home</a></li>
				<li><a href="#">About</a></li>
				<li><a href="#">Portfolio</a></li>
				<li><a href="#">Gallery</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
		</nav>
    </header>
    
    <div Class="mainContent">
	    <div Class="content">
    		<article class="articleContent">
	            <header>
	                <h2>First Article #1</h2>
	            </header>
	            
	            <footer>
	            	<p Class="post-info">Written by Super Woman</p>	            	
	            </footer>
	            <content>
	            	<p> Deforestation is the permanent removal of trees to make room for something besides forest. 
					   This can include clearing the land for agriculture or grazing, or using the timber for fuel, construction or manufacturing. </p>
			    </content>
    		</article> 
    		
    		<article Class="articleContent">
	            <header>
	                <h2>2nd Article #2</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>

	            <content>
	            	<div style="width:100%; height:100%">
						<canvas id="canvas"></canvas>
                        <button id="randomizeData">Randomize Data</button>
						<button id="addDataset">Add Dataset</button>
						<button id="removeDataset">Remove Dataset</button>
						<button id="addData">Add Data</button>
						<button id="removeData">Remove Data</button>
		           </div>
        		</content>
    		</article> 
	   </div>
	</div>
	
	<aside Class="top-sidebar">
		<article>
			<h2>Top sidebar</h2>
			<p>Lorum ipsum dolorum on top</p>
			  <div class="onoffswitch">
				<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
				<label class="onoffswitch-label" for="myonoffswitch"></label>
              </div>
		</article>
	</aside>
	
	<aside Class="middle-sidebar">
		<article>
			<h2>Middle sidebar</h2>
			<p>Lorum ipsum doloru in the middle</p>
		</article>
	</aside>
	
	<aside Class="bottom-sidebar">
		<article>
			<h2>Bottom sidebar</h2>
			<p>Lorum ipsum dolorum at the bottom</p>
		</article>
	</aside>
	
	<footer Class="mainFooter">
		<p>Copyright &copy; <a href="#" title = " 
 MyDesign">mywebsite.com</a></p>
	</footer>
	
	<script>
		var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var config = {
			type: 'line',
			data: {
				labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
				datasets: [{
					label: 'My First dataset',
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: false,
				}, {
					label: 'My Second dataset',
					fill: false,
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
				}]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: 'Chart.js Line Chart'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			config.data.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});

			});

			window.myLine.update();
		});

		var colorNames = Object.keys(window.chartColors);
		document.getElementById('addDataset').addEventListener('click', function() {
			var colorName = colorNames[config.data.datasets.length % colorNames.length];
			var newColor = window.chartColors[colorName];
			var newDataset = {
				label: 'Dataset ' + config.data.datasets.length,
				backgroundColor: newColor,
				borderColor: newColor,
				data: [],
				fill: false
			};

			for (var index = 0; index < config.data.labels.length; ++index) {
				newDataset.data.push(randomScalingFactor());
			}

			config.data.datasets.push(newDataset);
			window.myLine.update();
		});

		document.getElementById('addData').addEventListener('click', function() {
			if (config.data.datasets.length > 0) {
				var month = MONTHS[config.data.labels.length % MONTHS.length];
				config.data.labels.push(month);

				config.data.datasets.forEach(function(dataset) {
					dataset.data.push(randomScalingFactor());
				});

				window.myLine.update();
			}
		});

		document.getElementById('removeDataset').addEventListener('click', function() {
			config.data.datasets.splice(0, 1);
			window.myLine.update();
		});

		document.getElementById('removeData').addEventListener('click', function() {
			config.data.labels.splice(-1, 1); // remove the label first

			config.data.datasets.forEach(function(dataset) {
				dataset.data.pop();
			});

			window.myLine.update();
		});
	</script>
		
</body>

</html> 
```
- The utilis.js code(javascript)

```
'use strict';

window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};

(function(global) {
	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
		'#4dc9f6',
		'#f67019',
		'#f53794',
		'#537bc4',
		'#acc236',
		'#166a8f',
		'#00a950',
		'#58595b',
		'#8549ba'
	];

	var Samples = global.Samples || (global.Samples = {});
	var Color = global.Color;

	Samples.utils = {
		// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
		srand: function(seed) {
			this._seed = seed;
		},

		rand: function(min, max) {
			var seed = this._seed;
			min = min === undefined ? 0 : min;
			max = max === undefined ? 1 : max;
			this._seed = (seed * 9301 + 49297) % 233280;
			return min + (this._seed / 233280) * (max - min);
		},

		numbers: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 1;
			var from = cfg.from || [];
			var count = cfg.count || 8;
			var decimals = cfg.decimals || 8;
			var continuity = cfg.continuity || 1;
			var dfactor = Math.pow(10, decimals) || 0;
			var data = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = (from[i] || 0) + this.rand(min, max);
				if (this.rand() <= continuity) {
					data.push(Math.round(dfactor * value) / dfactor);
				} else {
					data.push(null);
				}
			}

			return data;
		},

		labels: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 100;
			var count = cfg.count || 8;
			var step = (max - min) / count;
			var decimals = cfg.decimals || 8;
			var dfactor = Math.pow(10, decimals) || 0;
			var prefix = cfg.prefix || '';
			var values = [];
			var i;

			for (i = min; i < max; i += step) {
				values.push(prefix + Math.round(dfactor * i) / dfactor);
			}

			return values;
		},

		months: function(config) {
			var cfg = config || {};
			var count = cfg.count || 12;
			var section = cfg.section;
			var values = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = MONTHS[Math.ceil(i) % 12];
				values.push(value.substring(0, section));
			}

			return values;
		},
*
		color: function(index) {
			return COLORS[index % COLORS.length];
		},

		transparentize: function(color, opacity) {
			var alpha = opacity === undefined ? 0.5 : 1 - opacity;
			return Color(color).alpha(alpha).rgbString();
		}
	};

	// DEPRECATED
	window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(-100, 100));
	};

	// INITIALIZATION

	Samples.utils.srand(Date.now());

	// Google Analytics
	/* eslint-disable */
	if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-28909194-3', 'auto');
		ga('send', 'pageview');
	}
	/* eslint-enable */

}(this));
```

*Add a button on the aside*

Button: [button](https://proto.io/freebies/onoff/)

- Make a button.css file in the CSS folder

- Add this code in it for the button

```
.onoffswitch {
    position: relative; width: 90px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}
.onoffswitch-checkbox {
    display: none;
}
.onoffswitch-label {
    display: block; overflow: hidden; cursor: pointer;
    height: 30px; padding: 0; line-height: 30px;
    border: 0px solid #807878; border-radius: 22px;
    background-color: #EEEEEE;
}
.onoffswitch-label:before {
    content: "";
    display: block; width: 22px; margin: 4px;
    background: #A1A1A1;
    position: absolute; top: 0; bottom: 0;
    right: 56px;
    border-radius: 22px;
    box-shadow: 0 6px 12px 0px #757575;
}
.onoffswitch-checkbox:checked + .onoffswitch-label {
    background-color: #292529;
}
.onoffswitch-checkbox:checked + .onoffswitch-label, .onoffswitch-checkbox:checked + .onoffswitch-label:before {
   border-color: #292529;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label:before {
    right: 0px; 
    background-color: #C91CAC; 
    box-shadow: 3px 6px 18px 0px rgba(0, 0, 0, 0.2);
}
```

- Link the button in Html in the <head>, otherwise nothing will show:

```
<link href="css/button.css" rel="stylesheet" />

```

*The website with the chart and the button*

![30](../img/iap/30.jpg)




**Javascript**
	
JavaScript is the Programming Language for the Web.

JavaScript can update and change both HTML and CSS.

JavaScript can calculate, manipulate and validate data.

*What can it do?*
- Add new HTML to the page, change the existing content, modify styles.
- React to user actions, run on mouse clicks, pointer movements, key presses.
- Send requests over the network to remote servers, download and upload files (so-called AJAX and COMET technologies).
- Get and set cookies, ask questions to the visitor, show messages.
- Remember the data on the client-side (“local storage”)

*Javascript the basics*

- Lets walk through these chapters of W3Schools.com

-Variables

-Operators

-Functions

-Events & Listeners

- Data

-Arrays

-Objects

-JSON

- Remote Calls

-API / JSON

*Variables*

JavaScript variables are containers for storing data values.

In this example, x, y, and z, are variables:

```

<html>
<head>
<script></script>
</head>
<body>
	<div id=”main”>Here is the InnerHTML</div>
<script>
/	/ play/practice in here
	var x,y,z;
	x = 11;
	y = 22;
	z = x*y;
	// write your results to the main DIV
	document.getElementById("main").innerHTML =  “antwoord is “ + z;
</script>
</body>
</html>
```

*Operators*

Example:
Assign values to variables and add them together:

```
var x = 5;         // assign the value 5 to x
var y = 2;         // assign the value 2 to y
var z = x + y;     // assign the value 7 to z (x + y)
```

*JavaScript Arithmetic Operators*

Arithmetic operators are used to perform arithmetic on numbers:

```
Operator	Description
+	        Addition
-	        Subtraction
*	        Multiplication
**	        Exponentiation (ES2016)
/	        Division
%	        Modulus (Division Remainder)
++	        Increment
--	        Decrement
```

*JavaScript Assignment Operators*

Assignment operators assign values to JavaScript variables.

```
Operator	Example	Same As
=	        x = y	x = y
+=	        x += y	x = x + y
-=	        x -= y	x = x - y
*=	        x *= y	x = x * y
/=	        x /= y	x = x / y
%=	        x %= y	x = x % y
**=	        x **= y	x = x ** y
```

The addition assignment operator (+=) adds a value to a variable.

*JavaScript String Operators*

The + operator can also be used to add (concatenate) strings.

Example

```
var txt1 = "John";
var txt2 = "Doe";
var txt3 = txt1 + " " + txt2;
```

*Adding Strings and Numbers*

Adding two numbers, will return the sum, but adding a number and a string will return a string:

Example

```
var x = 5 + 5;
var y = "5" + 5;
var z = "Hello" + 5;
```

*JavaScript Comparison Operators*

```
Operator	Description
==	        equal to
===	        equal value and equal type
!=	        not equal
!==	        not equal value or not equal type
>	        greater than
<	        less than
>=	        greater than or equal to
<=	        less than or equal to
?	        ternary operator
```

*Javascript Functions*

A JavaScript function is a block of code designed to perform a particular task.
A JavaScript function is executed when "something" invokes it (calls it).

Example:

```
function myFunction(p1, p2) {
  return p1 * p2;   // The function returns the product of p1 and p2
}
```

Functions are procedures that create an output based on given input(s)

```
<div id="main">i am InnerHTML of main</div>
<script>
var x,y,z;
x = 1;
y = 2;

// Functie die 2 inputs vermenigvuldigd
function maal(p1, p2) {
	return p1 * p2;   // The function returns the product of p1 and p2
}

// jQuery like function to point to DIV's and insert HTML content
function $(id,val) {
	document.getElementById(id).innerHTML=val;
}
</script>
```

*Javascript events and listener*

HTML events are "things" that happen to HTML elements.

When JavaScript is used in HTML pages, JavaScript can "react" on these events.

HTML Events
An HTML event can be something the browser does, or something a user does.

Here are some examples of HTML events:

- An HTML web page has finished loading
- An HTML input field was changed
- An HTML button was clicked

Often, when events happen, you may want to do something.

JavaScript lets you execute code when events are detected.

HTML allows event handler attributes, with JavaScript code, to be added to HTML elements.

Common HTML Events
Here is a list of some common HTML events:

Event	    Description
onchange	An HTML element has been changed
onclick	    The user clicks an HTML element
onmouseover	The user moves the mouse over an HTML element
onmouseout	The user moves the mouse away from an HTML element
onkeydown	The user pushes a keyboard key
onload	    The browser has finished loading the page

The code:
```

<div id="main">i am InnerHTML of main</div>
<!-- button onclick event triggers to write the date into the main -->
<button id="button1" onclick="$('main','b1 : ' + Date())" onmouseover="$('button2','haha')">button1</button>

<!-- button onclick event triggers to write the date into the main and onmouseover to change button1 -->
<button id="button2" onclick="$('main','b2 ' + Date());$('button2','button2')">button2</button>

</script>
// jQuery style selection: $(#main).innerHTML=val
	function $(id,val) {
		document.getElementById(id).innerHTML = val;
			}
	// add a Listener for button1 click event!
	document.getElementById("button1").addEventListener('onclick', function() {
		console.log(“button1 has been clicked);
	})
</script>
```

The code with variables,operators,events and functions:

```
<html>
<head>
   <script></script>
<head>
<body>
   <div id="main">i am InnerHTML</div>
   <!-- button onclick event triggers  to write the date into the main -->
   <button id="button1" onmouseover="$('button1', Date())" > Click me </button>
   <script>
        //declare variables 
        var x, y, z;       // Statement 1
		//Assign values to variables
		x = 22;            // Statement 2
		y = 22;            // Statement 3
		//Arithmic Operators
		//adding
		z = x + y;         // Statement 4
		document.getElementById("main").innerHTML = " x+y = " + (x+y)+ "<br>";
		//division
		z = x/y;
		document.getElementById("main").innerHTML += " x/y = " + z + " <br>";
		//multiply
		z = x*y;
		document.getElementById("main").innerHTML += " x*y = " + multiply(x,y) + " <br>";
		//comparisons
		z=(x==y)
		document.getElementById("main").innerHTML += " (x==y) = " + z + " <br>";
		//debugging to console
		console.log(x===y);
		
		function multiply (p1, p2) {
			console.log("someone is calling multiply with: " + p1 + "and" + p2)
			return p1 * p2;   // The function returns the product of p1 and p2
        }
		
		function $(id,val) {
		    document.getElementById(id).innerHTML = val;
			//jQuery: $(#main).innerHTML=val
	    }
		 
		//Add a listener to the button and multiply
		document.getElementById("button1").addEventListener('click', function(){
			console.log("Evenklikken: button1 has been clicked");
			document.getElementById('main').innerHTML = 'button1 clicked on :' + Date();
			//OF
			//$('main', 'button1 Clicked on : ' + Date();
		});
		//document.getElementById("Date").innerHTML = "op deze tijd" + Date();
		
		//Schrijf het antwoord naar "main" element
		//document.getElementById("main").innerHTML = " x " + x + " +<br>;
		//document.getElementById("main").innerHTML += " y " + y + "<br>;
		//document.getElementById("main").innerHTML = " = "+z;
   </script>
</body>
</html>
```

![32](../img/iap/32.jpg))


*Building a dashboard*

A dashboard is a type of graphical user interface which often provides at-a-glance views of key performance indicators (KPIs) relevant to a particular objective or business process. 
In other usage, "dashboard" is another name for "progress report" or "report."
The "dashboard" is often displayed on a web page which is linked to a database that allows the report to be constantly updated. 
For example, a manufacturing dashboard may show numbers related to productivity such as number of parts manufactured, or number of failed quality inspections per hour.

- Connect raspberry Pi
- Install Mosquitto MQTT & Test
    > sudo su  in putty 
    > apt-get update  in putty
    > apt-get install mosquitto in putty

Download greetubes for the index.html and app.js file
On your Rpi nodejs/project folder:
Make another Folder in your public folder and named it project3
In the project3 folder, put the greentubes folder 

**Interconnect NodeJs to MQTT**

Install this in the greentubes folder on Raspberry Pi and edit app.js and index.html file
> sudo npm install --save mqtt


```
var mqtt  = require('mqtt');
var client  = mqtt.connect('mqtt://127.0.0.1');

client.on('connect', function () {
client.subscribe('#');
client.publish('/', 'Connected to MQTT-Server');
console.log("\nNodeJS Connected to MQTT-Server\n");
});
// send all messages from MQTT to the Websocket with MQTT topic
client.on('message', function(topic, message){
console.log(topic+'='+message);
io.sockets.emit('mqtt',{'topic':String(topic),payload':String(message)});
});
```


**Interconnect WebSockets with MQTT 2**

Add the reverse from socket backt to MQTT inside the SocketIO code: 

```
io.sockets.on('connection', function (socket) {
// Add to handle from Socket to MQTT:
socket.on('mqtt', function (data) {
console.log('Receiving for MQTT '+ data.topic + data.payload);
client.publish(data.topic, data.payload);
});
```


The code for app.js:

```
var util = require('util');
var net = require('net');
var mqtt = require('mqtt');
var http = require("http"),
    url = require("url"),
    path = require("path"),
    fs = require("fs"),
    port = process.argv[2] || 8888;

var io  = require('socket.io').listen(5000);
var client  = mqtt.connect('mqtt://192.168.1.150');

client.on('connect', function () {
  client.subscribe('/ESP/#');
  client.publish('/ESP/Dashboard/', 'Connected to MQTT-Server');
  console.log("\nDashboard App Connected to MQTT-Server\n");
});

io.sockets.on('connection', function (socket) {
	
	client.on('message', function(topic, message){
	  console.log(topic+'='+message);
	  io.sockets.emit('mqtt',{'topic':String(topic),
		'payload':String(message)});
	});
  socket.on('subscribe', function (data) {
    console.log('Subscribing to '+data.topic);
    socket.join(data.topic);
    client.subscribe(data.topic);
  });
  socket.on('control', function (data) {
    console.log('Receiving for MQTT '+ data.topic + data.payload);
	// TODO sanity check .. is it valid topic ... check ifchannel is "mqtt" and pass message to MQTT server
    client.publish(data.topic, data.payload);
  }); 

});

http.createServer(function(request, response) {

  var uri = url.parse(request.url).pathname
    , filename = path.join(process.cwd(), uri);

  fs.exists(filename, function(exists) {
    if(!exists) {
      response.writeHead(404, {"Content-Type": "text/plain"});
      response.write("404 Not Found\n");
      response.end();
      return;
    }

    if (fs.statSync(filename).isDirectory()) filename += '/index.html';

    fs.readFile(filename, "binary", function(err, file) {
      if(err) {
        response.writeHead(500, {"Content-Type": "text/plain"});
        response.write(err + "\n");
        response.end();
        return;
      }

      response.writeHead(200);
      response.write(file, "binary");
      response.end();
    });
  });
}).listen(parseInt(port, 10));

console.log("Static file server running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");
```

**Storing received data points in browser**

On the Client side when you receive data from MQTT over Socket you want to store them in an array for use in Charts or other guages with a historic aspect.

Things to factor in:
- Multiple streams of data (topic+data)
- Amount of Data coming will become too much so we need to shift old data (or store it elsewhere in a Database)
- Time is usually NOT shipped with sensor data so add timestamps (receipt time)

General approach = make a function that creates an array with Variable name and store subsequent values of that topic with the time of receipt. Also shift older data points (100-1000)

**Storing received data points in client**

```
/* Data Storage (global) eg. historicData={"/ESP/33/Temperature",[ data]}
 select either Arrays or  [new Date(),data] OR value pairs {timestamp: new Date(),data: data}
After array exceeded max size we will shift older data away (wont be available anymore)
*/
<script>
window.topics = {}; // Global object storing all data series from MQTT

function storeMQData(topic, data) {
    if (!window.topics[topic]) {
        window.topics[topic] = [];
    }
   If (data!==null){		// add the topic data here of not empty
 window.topics[topic].push({
        		timestamp: new Date(),
        		data: data
    	})
   }

}
</script>
</body>
```



**Connecting your chart to data & update**

- Source for Streaming ChartJS

```
/* Connect ChartJS to look at your stired data
Connect to appropriate series
Trigger updates (by Time or  EventListener
Shift/Clean out older data
*/
<head>
<script type="text/javascript" src="moment.js"></script><script type="text/javascript" src="Chart.js"></script><script type="text/javascript" src="chartjs-plugin-streaming.js"></script>

<script>

</script>
```



The code for index.html:

```
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/semantic.min.css">

  <title>GreenTubes Dashboard / Fabacademy 2019 </title>

  <script type="text/javascript" src="js/socket.io.min.js"></script>
  <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
  <script src="js/lib/moment/moment.min.js"></script>
  <!-- Chart inserts -->
  <script src="js/lib/Chart.js/Chart.min.js"></script>
  <script src="js/lib/chartjs-plugin-streaming/chartjs-plugin-streaming.min.js"></script>
<style>
		input[type="checkbox"]{
		  visibility:hidden;
		}
		#checklabel{
		  height:40px;
		  width:40px;
		  background-color:#b5cc18;
		  display:block;
		  border-radius:100%;
		}
		input[type="checkbox"]:checked + label{
		  border:2px solid red;
		}
</style>

  <script type="text/javascript">
    var socket = io.connect('192.168.1.150:5000');
      socket.on('connect', function () {
        socket.on('mqtt', function (msg) {
          var msgTopic=msg.topic.split("/");
          var topic=msgTopic[2];
          var id=msgTopic[3];
          console.log(msg.topic+' '+msg.payload);
          console.log(topic.concat(id));
          $('#'+topic.concat(id)).html(msg.payload);
       });
       socket.emit('subscribe',{topic:'/ESP/#'});
//###
	  //$('input.cb-value').prop("checked", true);
		$('.cb-value').click(function() {
		  var mainParent = $(this).parent('.toggle-btn');
		  if($(mainParent).find('input.cb-value').is(':checked')) {
			$(mainParent).addClass('active');
			socket.emit('control',{'topic':"/ESP/"+ $(this).parents('div').attr('id'),'payload':"0"});
			//io.sockets.emit('mqtt',"/ESP/unit2/pump=on");
			//socket.emit('opencmd', id);
		  } else {
			$(mainParent).removeClass('active');
			socket.emit('control',{'topic':"/ESP/"+ $(this).parents('div').attr('id'),'payload':"1"});
			//io.sockets.emit('mqtt',"/ESP/unit2/pump=off");
			//socket.emit('closecmd', id);
		  }

		});
//###

      });
	  

  </script>
  
</head>

<body>

<p><img class="logo" src="img/logo.png" height="10%" width="10%">
<h2 class="ui center dividing aligned header">GreenTubes Dashboard  </h2>
</p>

<div class="wrapper">
<div class="section group">
<!--Temp-->
	<div class="col span_1_of_5">
    <div class="ui cards">
      <div class="card">
        <div class="content">
          <img class="right floated mini ui image" src="img/thermometer.png">
          <div class="header">
            Temperature
          </div>
          <div class="meta">
            Dht11 Sensor
          </div>
          <div class="description"></div>
        </div>
        <div style="text-align:center;" class="extra content centered">
          <div class="ui labeled button" tabindex="0">
        <div id="Temperature33" class="ui basic blue button">
          <div class="ui active small inline loader"></div>
          </div>
            <button class="ui basic left pointing blue label">°C</button>
          </div>
        </div>
      </div>
    </div>
	</div>
	<!--Humidity-->
	<div class="col span_1_of_5">
    <div class="ui cards">
      <div class="card">
        <div class="content">
          <img class="right floated mini ui image" src="img/humidity.png">
          <div class="header">
            Humidity
          </div>
          <div class="meta">
            DHT22 Sensor
          </div>
          <div class="description"></div>
        </div>
        <div style="text-align:center;" class="extra content centered">
          <div class="ui labeled button" tabindex="0">
        <div id="Humidity33" class="ui basic blue button">
          <div class="ui active small inline loader"></div>
          </div>
            <button class="ui basic left pointing blue label">°C</button>
          </div>
        </div>
      </div>
    </div>
	</div>
	
	<!--Soil-->
	<div class="col span_1_of_5">
    <div class="ui cards">
    <div class="card">
      <div class="content">
        <img class="right floated mini ui image" src="img/soil.png">
        <div class="header">
          Soil Moisture
        </div>
        <div class="meta">
          Yl-69 Sensor
        </div>
        <div class="description">
        </div>
      </div>
      <div style="text-align:center;" class="extra content centered">
        <div class="ui labeled button" tabindex="0">
      <div id="33Soil" class="ui basic blue button">
        <div class="ui active small inline loader"></div>
        </div>
          <button class="ui basic left pointing blue label">%</button>
        </div>
      </div>
    </div>
  </div>
	</div>
<!--Battery Life-->
<!--<div class="col span_1_of_5">
    <div class="ui cards">
    <div class="card">
      <div class="content">
        <img class="right floated mini ui image" src="img/battery.png">
        <div class="header">
           Battery Life
        </div>
        <div class="meta">NodeMCU
        </div>
        <div class="description"></div>
      </div>
      <div style="text-align:center;" class="extra content centered">
        <div class="ui labeled button" tabindex="0">
      <div id="Uptime31" class="ui basic blue button">
        <div class="ui active small inline loader"></div>
        </div>
          <button class="ui basic left pointing blue label">min</button>
        </div>
      </div>
    </div>
  </div>
	</div>-->

<!--Pump Control-->

	<div class="col span_1_of_5">
    <div class="ui cards">
      <div class="card">
        <div class="content">
          <img class="right floated mini ui image" src="img/pump.png">
          <div class="header">
            Pump Control
          </div>
          <div class="meta">
            Status
          </div>
          <div class="description"></div>
        </div>
        <div style="text-align:center;" class="extra content centered">
          <div class="ui labeled button" tabindex="0">
        <!--<div id="33Soil">
          <!--<div class="ui active small inline loader"></div>--
          </div>
            <!--<button class="ui basic left pointing blue label">Button</button>-->
		<div id="44/pump/" class="toggle-btn active">
		<input value="1" type="checkbox" id="mycheck" checked class="cb-value" />
		<label id="checklabel" for="mycheck" checked class="cb-value">
		</div>
          </div>
        </div>
      </div>
    </div>
</div>

  
</div>
  

</label>

</div>
</div>
</div>

<!-- Chart placeholder -->
<div style="width:90%; height:95% ">
	<canvas id="myChart">
	</canvas>
</div>

<script>
    // Storage handler
    window.topics = {}; // Global object storing all data series from MQTT

    function storeMQData(topic, data, arr) {
        if (!(arr = window.topics[topic])) {
            arr = window.topics[topic] = [];
        }
       // If data available push to array (as object) 
        data && arr.push({
            x: new Date(),
            y: parseFloat(data)
        });
    }
</script>

<script>
      // initialize storage
      storeMQData("Humidity33");
      storeMQData("Temperature33");
      // Chart Configuration
      var chartConfig = {
          type: 'line',
          data: {
              datasets: [{
                  label: 'Temperature33',
                  borderColor: 'rgb(255, 99, 132)',
                  //backgroundColor: 'rgba(255, 99, 132, 0.5)',
                  fill: false,
                  cubicInterpolationMode: 'monotone',
                  borderDash: [8, 4],
                  data: topics.Temperature33
              }, {
                  label: 'Humidity33',
                  borderColor: 'rgb(54, 162, 235)',
                  //backgroundColor: 'rgba(54, 162, 235, 0.5)',
                  fill: false,
                  cubicInterpolationMode: 'monotone',
                  data: topics.Humidity33
              }]
          },
          options: {
              scales: {
                  xAxes: [{
                      type: 'realtime',
                      realtime: {
                          duration: 20000, // data in the past 20000 ms will be displayed
                          refresh: 1000, // onRefresh callback will be called every 1000 ms
                          delay: 1000, // delay of 1000 ms, so upcoming values are known before plotting a line
                          pause: false, // chart is not paused
                          ttl: 22000, // data will be automatically deleted as it disappears off the chart 
                      }
                  }],
                   yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        max: 100
                          }
                       }],
                     }
                   }
                 };
         window.onload = function() {
         var ctx = document.getElementById('myChart').getContext('2d');
         // Start chart
         var chart = new Chart(ctx,chartConfig );
      }
  </script>

 <script type="text/javascript">
      var socket = io.connect(window.location.hostname + ":5000");
      socket.on('connect', function() {
        socket.emit('subscribe', {
              topic: '/ESP/#'
          });

          socket.on('mqtt', function(msg) {
              var msgTopic = msg.topic.split("/");
              var topic = msgTopic[3];
              var id = msgTopic[2];
              console.log(msg.topic + ' ' + msg.payload);
              //console.log('#'+topic.concat(id));
              // Added for storage of all incoming MQTT messages in Array
              storeMQData(topic.concat(id), msg.payload);
              // end insert
              $('#' + topic.concat(id)).html(msg.payload);
          });
	   
	   
          //$('input.cb-value').prop("checked", true);
          $('.cb-value').click(function() {
              var mainParent = $(this).parent('.toggle-btn');
              if ($(mainParent).find('input.cb-value').is(':checked')) {
                  $(mainParent).addClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "on"
                  });
              } else {
                  $(mainParent).removeClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "off"
                  });
                  //io.sockets.emit('mqtt',"/ESP/unit2/pump=off");
                  //socket.emit('closecmd', id);
              }
          });
      });
	</script>  

</body>
</html>
```


The dashboard:

![31](../img/iap/31.jpg)


### Scada

Controlling Things using Socket.io / nodejs / RaspberryPi gpio

*Map Rpi gpIO Pins Hardware & Software*

![33](../img/iap/33.jpg)


*Build the circuit*

![34](../img/iap/34.jpg)


*Add scada project to new project folder*

```
nodejs

   projects

      project2

          scada
```

*npm install express*

![35](../img/iap/35.jpg)


*npm install socket.io*

![36](../img/iap/36.jpg)


*sudo apt-get install pigpio*

![37](../img/iap/37.jpg)


*npm install pigpio*

![38](../img/iap/38.jpg)


*sudo node app.js*

![39](../img/iap/39.jpg)


*The code in app.js*

```
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var path = require('path');
// Now setup the local hardware IO
var Gpio = require('pigpio').Gpio;
// start your server
var port=4000;
server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log('server is listening on port 4000')
})

// routing your client app (stored in the /public folder)
app.use(express.static(path.join(__dirname, 'public')));


// Handling Socket messages  as soon as socket becomes active
io.sockets.on('connection', function (socket) {
	// Hookup button behaviour within socket to submit an update when pressed (TO DO)

	// when the client emits 'opencmd', this listens and executes
	socket.on('opencmd', function (data) {
		// Add calls to IO here
		io.sockets.emit('opencmd', socket.username, data);
		setTimeout(function () {
			io.sockets.emit('openvalve', socket.username, data);
			console.log("user: "+socket.username+" opens LED" + data);
			// add some error handling here
			led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
			led.digitalWrite(1);
		}, 1000)
	});

	// when the client emits 'closecmd', this listens and executes
	socket.on('closecmd', function (data) {
		// Add calls to IO here
		io.sockets.emit('closecmd', socket.username, data);
		setTimeout(function () {
			io.sockets.emit('closevalve', socket.username, data);
			console.log("user: "+socket.username+" closes LED" + data);
			// add error handling here
			led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
			led.digitalWrite(0);
		}, 1000)
		
/* 		setTimeout(function () {
			led.digitalWrite(0);
			io.sockets.emit('closevalve', socket.username, data);
		}, 1000) */
	});

});
```

*The code in index.html*

```
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<meta http-equiv="X-UA-Compatible" content="IE=9" />

<head>
    <title>Codettes Tutorial</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <link type="text/css" rel="stylesheet" href="css/app.css" media="all">
</head>
<script src="js/socket.js"></script>
<script src="js/jquery.js"></script>
<script>
    var socket = io.connect(window.location.hostname + ":4000"); //'http://192.168.1.163:4000'); //set this to the ip address of your node.js server

    // listener, whenever the server emits 'openvalve', this updates the username list
    socket.on('opencmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('red').addClass('green');
    });
    socket.on('openvalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('red').addClass('green');
    });


    // listener, whenever the server emits 'openvalve', this updates the username list
     socket.on('closecmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('green').addClass('red');
    });
	socket.on('closevalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('green').addClass('red');
    });

    // on load of page
    $(function() {

        // when the client clicks OPEN
        $('.open').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked open : " + id);
            socket.emit('opencmd', id);
        });

        // when the client clicks CLOSE
        $('.close').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked on close : " + id);
            socket.emit('closecmd', id);
        });

    });
</script>

<body>
    <p>

        <div id="17" class="valve">
            <h3>Control 1</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="27" class="valve">
            <h3>Control 2</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="18" class="valve">
            <h3>Control 3</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="23" class="valve">
            <h3>Control 4</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
</body>
```

*Now you can turn on and off your Led's*

![40](../img/iap/40.jpg)


### Reference

<a href="myFile.js/" download>cmdline</a>  

<a href="myFile.js/" download>putty</a> 

<a href="myFile.js/" download>python</a> 

<a href="myFile.js/" download>winscp</a> 

<a href="myFile.js/" download>greentubesdashboard</a> 



































