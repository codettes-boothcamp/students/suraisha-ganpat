## Electronics design

**Kicad Software**

KiCad is a free software suite for electronic design automation. 
It facilitates the design of schematics for electronic circuits and their conversion to PCB designs.

- Install Kicad Software
- Install Flatcam Software
- Import Library (mod-pretty)

*For my first project I have made an Adruino uno, using the Atmega328p-Au as the processor*

Make a new folder and Open Kicad

![1](../img/ed/1.jpg)

Preference-> Manage Symbol libraries

![2](../img/ed/2.jpg)


Append library from the folder elctronics production and select the folder fab.pretty.
The fab library is used for the symbols.

![3](../img/ed/3.jpg)


Click on files-> New-> Projects

![4](../img/ed/4.jpg)


Kicad Component List: These are the component that has been used.

![5](../img/ed/5.jpg)


Atmega328p-Au Pinconfig pg2. For this I need the datasheet so I know which pin to use to connect it correctly. 

![6](../img/ed/6.jpg)


Atmega328p-Au - Arduino Pin

![7](../img/ed/7.jpg)


Place symbol using the fab library for the symbols:

![8](../img/ed/8.jpg)


Chose the symbols for the arduino sketch

![9](../img/ed/9.jpg)


Annonate schematic. 
Annotate is needed to see if the components are labeled correctly ( No double naming).

![10](../img/ed/10.jpg)

![11](../img/ed/11.jpg)


Assign footprints. The Footprint gives the exact dimension.

![12](../img/ed/12.jpg)

![13](../img/ed/13.jpg)


Adding the fab footprints:
Preference-> Manage footprints librarie

![14](../img/ed/14.jpg)


Browse library-> Select kicad-> fab.mod

![15](../img/ed/15.jpg)

![16](../img/ed/16.jpg)


Select the fab1 footprint library

![17](../img/ed/17.jpg)


Search for the footprints and change the names to the fab1 footprints

![18](../img/ed/18.jpg)


Connect the component to eachother building a Arduino Uno

![19](../img/ed/19.jpg)


After finishing building the arduino: Generate netlist

![20](../img/ed/20.jpg)


Open PCB(Printed Circuit Board) kicad-> Select net-> Search for the folder where the netlist is-> Read current netlist

![21](../img/ed/21.jpg)


Select setup-> Design rules

![22](../img/ed/22.jpg)


Organize the components and trace kicad routes

![23](../img/ed/23.jpg)


Auxiliary Axis

![24](../img/ed/24.jpg)


Add dimensions
The dimensension are: 35.560 mm x 57.150 mm

![25](../img/ed/25.jpg)


File-> Plot the design-> mark F.cut, use auxillary as origin and select output directory

![26](../img/ed/26.jpg)

**Faltcamm**


Open flatcam
File-> Open Gerber

![27](../img/ed/27.jpg)


Select options: Application in mm and project in mm

Flatcam Params:

![28](../img/ed/28.jpg)


Click Selected and open project1 kicad-F.Cu.gbr and change the params-> Click Generate 

![29](../img/ed/29.jpg)

![30](../img/ed/30.jpg)


Click Selected and open project1 kicad-F.Cu.gbr_iso and change the params-> Click Generate

![31](../img/ed/31.jpg)


Click Selected and open project1 kicad-F.Cu.gbr_cutout and change the params-> Click Generate

![32](../img/ed/32.jpg)


Export Gcode for both project1 kicad-F.Cu.gbr_cutout_cnc and project1 kicad-F.Cu.gbr_iso_cnc

![33](../img/ed/33.jpg)


* GrblControl Params/Grbl Controller(candel.exe)*

Open GrblControl(candle)
Click Service-> Settings

![34](../img/ed/34.jpg)


GrblControl Params
Settings? Panels ? Heightmap(checked)

![35](../img/ed/35.jpg)


Now used the CNC milling machine for the milling

![36](../img/ed/36.jpg)

### CNC milling

Now physically place a copper plate on the stepcraft base using double sided tape. 
I then went and physically placed my copper plate onto the stepcraft base plate using double sided tape and imported my front cut.cnc file and cutout.cnc file

The parameters that must be checked before milling:
- Cnc Mill Probe Z physical
- Gnd Pin Connected
- Magnet on pcb
- Probe Z
- Test pin vcc to pcb x2

Then jog the mill to preferred x y and z positions. After that check if the x y and z positions are good and then click on start to start milling the board.


### Soldering

After the board is milled, I cleaned it and then checked if the ground pin is connected. If all the connections are good, I started to solder my board. 


I searched the components that I needed for my board:

- Atmega328p-au 
- Ftdi
- Right Connector
- 1002 smd 10k resistor for led (AFTER BURNING BOOTLOADER REMOVE AND ADD 4990)
- Led
- Switch
- 1uf capaciter
- 0.1 or 10000 pf  capaciter
- 10k resistor 1002

It was really difficult to solder the Atmega328p-Au. After soldering the Atmega328p-Au. I checked if it was soldered good and if the pins were not connected to eachother.
Then I soldered the other components. That was much easier to solder. Afer soldering I checked all the connections.

![37](../img/ed/37.jpg)


### Programming

For bootloader I used the Arduino as ISP(In-System-Programmer)miso mosi vcc gnd  10-Rest 11-Mosi 12-Miso 13-Sck  VCC GND. Before programming I double checked the soldering and routes for shorts. 
And I made sure the Miso, Mosi and SCK pins have extra solder, otherwise the connections are weak.

Before bootloading I edit Boardtext, I click on Arduino on my desktop-> Hardware-> Arduino-> Avr-> And I click on board text and go to properties ->
Cick the Security tab, click on Edit grant the user write permission. Now I gave it permission to ad text to it.

![38](../img/ed/38.jpg)

I edited the Board text by putting this code.

```
##############################################################

hello.name=hello.arduino, ATmega328P, 5V, 8MHz internal oscillator
hello.upload.protocol=arduino
hello.upload.maximum_size=30720
hello.upload.speed=57600
hello.bootloader.low_fuses=0xE2
hello.bootloader.high_fuses=0xDA
hello.bootloader.extended_fuses=0x07
hello.bootloader.path=arduino:atmega
hello.bootloader.file=ATmegaBOOT_168_atmega328_pro_8MHz.hex
hello.bootloader.unlock_bits=0x3F
hello.bootloader.lock_bits=0x0F
hello.build.mcu=atmega328p
hello.build.f_cpu=8000000L
hello.build.core=arduino:arduino
hello.build.variant=arduino:standard
hello.upload.tool=arduino:avrdude
hello.bootloader.tool=arduino:avrdude

##############################################################

hello.build.board=atmega328p
hello.upload.tool=arduino:avrdude
hello.bootloader.tool=arduino:avrdude
```

**Arduino as ISP**

I used the Arduino Uno to program my board, So the two needs to be connected together. I then connect the Arduino with my Board.

1x6 Connecter     

- Reset
- Ground
- VCC
- SCK
- Miso
- Mosi

Arduino as Isp 

- Pin 10 connects to Reset
- Pin 11 connects to Mosi
- Pin 12 connects to Miso
- Pin 13 connects to SCK
- Ground connects to Ground
- VCC conncts to VCC

![40](../img/ed/40.jpg)

I connect the Arduino to my laptop

Open Arduino-> Select Tool-> Select Board As "Arduino Uno"

![41](../img/ed/41.jpg)


I Click on files -> Examples -> ArduinoISP -> ArduinoISP

![39](../img/ed/39.jpg)

This is the Arduino ISP code. Before uploading to the Arduino, I made a small change in the heartbeat() function that says "delay(40);" and change it to "delay(20);".
I made Sure I select my Port before Uploading
Then I selected the Board by Clicking on Tools -> Board -> Select the Hello.Arduino Atmega328p Board

![42](../img/ed/42.jpg)

Then I uploaded the Sketch to my Arduino

```
// ArduinoISP
// Copyright (c) 2008-2011 Randall Bohn
// If you require a license, see
// http://www.opensource.org/licenses/bsd-license.php
//
// This sketch turns the Arduino into a AVRISP using the following Arduino pins:
//
// Pin 10 is used to reset the target microcontroller.
//
// By default, the hardware SPI pins MISO, MOSI and SCK are used to communicate
// with the target. On all Arduinos, these pins can be found
// on the ICSP/SPI header:
//
//               MISO °. . 5V (!) Avoid this pin on Due, Zero...
//               SCK   . . MOSI
//                     . . GND
//
// On some Arduinos (Uno,...), pins MOSI, MISO and SCK are the same pins as
// digital pin 11, 12 and 13, respectively. That is why many tutorials instruct
// you to hook up the target to these pins. If you find this wiring more
// practical, have a define USE_OLD_STYLE_WIRING. This will work even when not
// using an Uno. (On an Uno this is not needed).
//
// Alternatively you can use any other digital pin by configuring
// software ('BitBanged') SPI and having appropriate defines for PIN_MOSI,
// PIN_MISO and PIN_SCK.
//
// IMPORTANT: When using an Arduino that is not 5V tolerant (Due, Zero, ...) as
// the programmer, make sure to not expose any of the programmer's pins to 5V.
// A simple way to accomplish this is to power the complete system (programmer
// and target) at 3V3.
//
// Put an LED (with resistor) on the following pins:
// 9: Heartbeat   - shows the programmer is running
// 8: Error       - Lights up if something goes wrong (use red if that makes sense)
// 7: Programming - In communication with the slave
//

#include "Arduino.h"
#undef SERIAL


#define PROG_FLICKER true

// Configure SPI clock (in Hz).
// E.g. for an ATtiny @ 128 kHz: the datasheet states that both the high and low
// SPI clock pulse must be > 2 CPU cycles, so take 3 cycles i.e. divide target
// f_cpu by 6:
//     #define SPI_CLOCK            (128000/6)
//
// A clock slow enough for an ATtiny85 @ 1 MHz, is a reasonable default:

#define SPI_CLOCK 		(1000000/6)


// Select hardware or software SPI, depending on SPI clock.
// Currently only for AVR, for other architectures (Due, Zero,...), hardware SPI
// is probably too fast anyway.

#if defined(ARDUINO_ARCH_AVR)

#if SPI_CLOCK > (F_CPU / 128)
#define USE_HARDWARE_SPI
#endif

#endif

// Configure which pins to use:

// The standard pin configuration.
#ifndef ARDUINO_HOODLOADER2

#define RESET     10 // Use pin 10 to reset the target rather than SS
#define LED_HB    9
#define LED_ERR   8
#define LED_PMODE 7

// Uncomment following line to use the old Uno style wiring
// (using pin 11, 12 and 13 instead of the SPI header) on Leonardo, Due...

// #define USE_OLD_STYLE_WIRING

#ifdef USE_OLD_STYLE_WIRING

#define PIN_MOSI	11
#define PIN_MISO	12
#define PIN_SCK		13

#endif

// HOODLOADER2 means running sketches on the ATmega16U2 serial converter chips
// on Uno or Mega boards. We must use pins that are broken out:
#else

#define RESET     	4
#define LED_HB    	7
#define LED_ERR   	6
#define LED_PMODE 	5

#endif

// By default, use hardware SPI pins:
#ifndef PIN_MOSI
#define PIN_MOSI 	MOSI
#endif

#ifndef PIN_MISO
#define PIN_MISO 	MISO
#endif

#ifndef PIN_SCK
#define PIN_SCK 	SCK
#endif

// Force bitbanged SPI if not using the hardware SPI pins:
#if (PIN_MISO != MISO) ||  (PIN_MOSI != MOSI) || (PIN_SCK != SCK)
#undef USE_HARDWARE_SPI
#endif


// Configure the serial port to use.
//
// Prefer the USB virtual serial port (aka. native USB port), if the Arduino has one:
//   - it does not autoreset (except for the magic baud rate of 1200).
//   - it is more reliable because of USB handshaking.
//
// Leonardo and similar have an USB virtual serial port: 'Serial'.
// Due and Zero have an USB virtual serial port: 'SerialUSB'.
//
// On the Due and Zero, 'Serial' can be used too, provided you disable autoreset.
// To use 'Serial': #define SERIAL Serial

#ifdef SERIAL_PORT_USBVIRTUAL
#define SERIAL SERIAL_PORT_USBVIRTUAL
#else
#define SERIAL Serial
#endif


// Configure the baud rate:

#define BAUDRATE	19200
// #define BAUDRATE	115200
// #define BAUDRATE	1000000


#define HWVER 2
#define SWMAJ 1
#define SWMIN 18

// STK Definitions
#define STK_OK      0x10
#define STK_FAILED  0x11
#define STK_UNKNOWN 0x12
#define STK_INSYNC  0x14
#define STK_NOSYNC  0x15
#define CRC_EOP     0x20 //ok it is a space...

void pulse(int pin, int times);

#ifdef USE_HARDWARE_SPI
#include "SPI.h"
#else

#define SPI_MODE0 0x00

class SPISettings {
  public:
    // clock is in Hz
    SPISettings(uint32_t clock, uint8_t bitOrder, uint8_t dataMode) : clock(clock) {
      (void) bitOrder;
      (void) dataMode;
    };

  private:
    uint32_t clock;

    friend class BitBangedSPI;
};

class BitBangedSPI {
  public:
    void begin() {
      digitalWrite(PIN_SCK, LOW);
      digitalWrite(PIN_MOSI, LOW);
      pinMode(PIN_SCK, OUTPUT);
      pinMode(PIN_MOSI, OUTPUT);
      pinMode(PIN_MISO, INPUT);
    }

    void beginTransaction(SPISettings settings) {
      pulseWidth = (500000 + settings.clock - 1) / settings.clock;
      if (pulseWidth == 0)
        pulseWidth = 1;
    }

    void end() {}

    uint8_t transfer (uint8_t b) {
      for (unsigned int i = 0; i < 8; ++i) {
        digitalWrite(PIN_MOSI, (b & 0x80) ? HIGH : LOW);
        digitalWrite(PIN_SCK, HIGH);
        delayMicroseconds(pulseWidth);
        b = (b << 1) | digitalRead(PIN_MISO);
        digitalWrite(PIN_SCK, LOW); // slow pulse
        delayMicroseconds(pulseWidth);
      }
      return b;
    }

  private:
    unsigned long pulseWidth; // in microseconds
};

static BitBangedSPI SPI;

#endif

void setup() {
  SERIAL.begin(BAUDRATE);

  pinMode(LED_PMODE, OUTPUT);
  pulse(LED_PMODE, 2);
  pinMode(LED_ERR, OUTPUT);
  pulse(LED_ERR, 2);
  pinMode(LED_HB, OUTPUT);
  pulse(LED_HB, 2);

}

int error = 0;
int pmode = 0;
// address for reading and writing, set by 'U' command
unsigned int here;
uint8_t buff[256]; // global block storage

#define beget16(addr) (*addr * 256 + *(addr+1) )
typedef struct param {
  uint8_t devicecode;
  uint8_t revision;
  uint8_t progtype;
  uint8_t parmode;
  uint8_t polling;
  uint8_t selftimed;
  uint8_t lockbytes;
  uint8_t fusebytes;
  uint8_t flashpoll;
  uint16_t eeprompoll;
  uint16_t pagesize;
  uint16_t eepromsize;
  uint32_t flashsize;
}
parameter;

parameter param;

// this provides a heartbeat on pin 9, so you can tell the software is running.
uint8_t hbval = 128;
int8_t hbdelta = 8;
void heartbeat() {
  static unsigned long last_time = 0;
  unsigned long now = millis();
  if ((now - last_time) < 20)
    return;
  last_time = now;
  if (hbval > 192) hbdelta = -hbdelta;
  if (hbval < 32) hbdelta = -hbdelta;
  hbval += hbdelta;
  analogWrite(LED_HB, hbval);
}

static bool rst_active_high;

void reset_target(bool reset) {
  digitalWrite(RESET, ((reset && rst_active_high) || (!reset && !rst_active_high)) ? HIGH : LOW);
}

void loop(void) {
  // is pmode active?
  if (pmode) {
    digitalWrite(LED_PMODE, HIGH);
  } else {
    digitalWrite(LED_PMODE, LOW);
  }
  // is there an error?
  if (error) {
    digitalWrite(LED_ERR, HIGH);
  } else {
    digitalWrite(LED_ERR, LOW);
  }

  // light the heartbeat LED
  heartbeat();
  if (SERIAL.available()) {
    avrisp();
  }
}

uint8_t getch() {
  while (!SERIAL.available());
  return SERIAL.read();
}
void fill(int n) {
  for (int x = 0; x < n; x++) {
    buff[x] = getch();
  }
}

#define PTIME 30
void pulse(int pin, int times) {
  do {
    digitalWrite(pin, HIGH);
    delay(PTIME);
    digitalWrite(pin, LOW);
    delay(PTIME);
  } while (times--);
}

void prog_lamp(int state) {
  if (PROG_FLICKER) {
    digitalWrite(LED_PMODE, state);
  }
}

uint8_t spi_transaction(uint8_t a, uint8_t b, uint8_t c, uint8_t d) {
  SPI.transfer(a);
  SPI.transfer(b);
  SPI.transfer(c);
  return SPI.transfer(d);
}

void empty_reply() {
  if (CRC_EOP == getch()) {
    SERIAL.print((char)STK_INSYNC);
    SERIAL.print((char)STK_OK);
  } else {
    error++;
    SERIAL.print((char)STK_NOSYNC);
  }
}

void breply(uint8_t b) {
  if (CRC_EOP == getch()) {
    SERIAL.print((char)STK_INSYNC);
    SERIAL.print((char)b);
    SERIAL.print((char)STK_OK);
  } else {
    error++;
    SERIAL.print((char)STK_NOSYNC);
  }
}

void get_version(uint8_t c) {
  switch (c) {
    case 0x80:
      breply(HWVER);
      break;
    case 0x81:
      breply(SWMAJ);
      break;
    case 0x82:
      breply(SWMIN);
      break;
    case 0x93:
      breply('S'); // serial programmer
      break;
    default:
      breply(0);
  }
}

void set_parameters() {
  // call this after reading parameter packet into buff[]
  param.devicecode = buff[0];
  param.revision   = buff[1];
  param.progtype   = buff[2];
  param.parmode    = buff[3];
  param.polling    = buff[4];
  param.selftimed  = buff[5];
  param.lockbytes  = buff[6];
  param.fusebytes  = buff[7];
  param.flashpoll  = buff[8];
  // ignore buff[9] (= buff[8])
  // following are 16 bits (big endian)
  param.eeprompoll = beget16(&buff[10]);
  param.pagesize   = beget16(&buff[12]);
  param.eepromsize = beget16(&buff[14]);

  // 32 bits flashsize (big endian)
  param.flashsize = buff[16] * 0x01000000
                    + buff[17] * 0x00010000
                    + buff[18] * 0x00000100
                    + buff[19];

  // AVR devices have active low reset, AT89Sx are active high
  rst_active_high = (param.devicecode >= 0xe0);
}

void start_pmode() {

  // Reset target before driving PIN_SCK or PIN_MOSI

  // SPI.begin() will configure SS as output, so SPI master mode is selected.
  // We have defined RESET as pin 10, which for many Arduinos is not the SS pin.
  // So we have to configure RESET as output here,
  // (reset_target() first sets the correct level)
  reset_target(true);
  pinMode(RESET, OUTPUT);
  SPI.begin();
  SPI.beginTransaction(SPISettings(SPI_CLOCK, MSBFIRST, SPI_MODE0));

  // See AVR datasheets, chapter "SERIAL_PRG Programming Algorithm":

  // Pulse RESET after PIN_SCK is low:
  digitalWrite(PIN_SCK, LOW);
  delay(20); // discharge PIN_SCK, value arbitrarily chosen
  reset_target(false);
  // Pulse must be minimum 2 target CPU clock cycles so 100 usec is ok for CPU
  // speeds above 20 KHz
  delayMicroseconds(100);
  reset_target(true);

  // Send the enable programming command:
  delay(50); // datasheet: must be > 20 msec
  spi_transaction(0xAC, 0x53, 0x00, 0x00);
  pmode = 1;
}

void end_pmode() {
  SPI.end();
  // We're about to take the target out of reset so configure SPI pins as input
  pinMode(PIN_MOSI, INPUT);
  pinMode(PIN_SCK, INPUT);
  reset_target(false);
  pinMode(RESET, INPUT);
  pmode = 0;
}

void universal() {
  uint8_t ch;

  fill(4);
  ch = spi_transaction(buff[0], buff[1], buff[2], buff[3]);
  breply(ch);
}

void flash(uint8_t hilo, unsigned int addr, uint8_t data) {
  spi_transaction(0x40 + 8 * hilo,
                  addr >> 8 & 0xFF,
                  addr & 0xFF,
                  data);
}
void commit(unsigned int addr) {
  if (PROG_FLICKER) {
    prog_lamp(LOW);
  }
  spi_transaction(0x4C, (addr >> 8) & 0xFF, addr & 0xFF, 0);
  if (PROG_FLICKER) {
    delay(PTIME);
    prog_lamp(HIGH);
  }
}

unsigned int current_page() {
  if (param.pagesize == 32) {
    return here & 0xFFFFFFF0;
  }
  if (param.pagesize == 64) {
    return here & 0xFFFFFFE0;
  }
  if (param.pagesize == 128) {
    return here & 0xFFFFFFC0;
  }
  if (param.pagesize == 256) {
    return here & 0xFFFFFF80;
  }
  return here;
}


void write_flash(int length) {
  fill(length);
  if (CRC_EOP == getch()) {
    SERIAL.print((char) STK_INSYNC);
    SERIAL.print((char) write_flash_pages(length));
  } else {
    error++;
    SERIAL.print((char) STK_NOSYNC);
  }
}

uint8_t write_flash_pages(int length) {
  int x = 0;
  unsigned int page = current_page();
  while (x < length) {
    if (page != current_page()) {
      commit(page);
      page = current_page();
    }
    flash(LOW, here, buff[x++]);
    flash(HIGH, here, buff[x++]);
    here++;
  }

  commit(page);

  return STK_OK;
}

#define EECHUNK (32)
uint8_t write_eeprom(unsigned int length) {
  // here is a word address, get the byte address
  unsigned int start = here * 2;
  unsigned int remaining = length;
  if (length > param.eepromsize) {
    error++;
    return STK_FAILED;
  }
  while (remaining > EECHUNK) {
    write_eeprom_chunk(start, EECHUNK);
    start += EECHUNK;
    remaining -= EECHUNK;
  }
  write_eeprom_chunk(start, remaining);
  return STK_OK;
}
// write (length) bytes, (start) is a byte address
uint8_t write_eeprom_chunk(unsigned int start, unsigned int length) {
  // this writes byte-by-byte, page writing may be faster (4 bytes at a time)
  fill(length);
  prog_lamp(LOW);
  for (unsigned int x = 0; x < length; x++) {
    unsigned int addr = start + x;
    spi_transaction(0xC0, (addr >> 8) & 0xFF, addr & 0xFF, buff[x]);
    delay(45);
  }
  prog_lamp(HIGH);
  return STK_OK;
}

void program_page() {
  char result = (char) STK_FAILED;
  unsigned int length = 256 * getch();
  length += getch();
  char memtype = getch();
  // flash memory @here, (length) bytes
  if (memtype == 'F') {
    write_flash(length);
    return;
  }
  if (memtype == 'E') {
    result = (char)write_eeprom(length);
    if (CRC_EOP == getch()) {
      SERIAL.print((char) STK_INSYNC);
      SERIAL.print(result);
    } else {
      error++;
      SERIAL.print((char) STK_NOSYNC);
    }
    return;
  }
  SERIAL.print((char)STK_FAILED);
  return;
}

uint8_t flash_read(uint8_t hilo, unsigned int addr) {
  return spi_transaction(0x20 + hilo * 8,
                         (addr >> 8) & 0xFF,
                         addr & 0xFF,
                         0);
}

char flash_read_page(int length) {
  for (int x = 0; x < length; x += 2) {
    uint8_t low = flash_read(LOW, here);
    SERIAL.print((char) low);
    uint8_t high = flash_read(HIGH, here);
    SERIAL.print((char) high);
    here++;
  }
  return STK_OK;
}

char eeprom_read_page(int length) {
  // here again we have a word address
  int start = here * 2;
  for (int x = 0; x < length; x++) {
    int addr = start + x;
    uint8_t ee = spi_transaction(0xA0, (addr >> 8) & 0xFF, addr & 0xFF, 0xFF);
    SERIAL.print((char) ee);
  }
  return STK_OK;
}

void read_page() {
  char result = (char)STK_FAILED;
  int length = 256 * getch();
  length += getch();
  char memtype = getch();
  if (CRC_EOP != getch()) {
    error++;
    SERIAL.print((char) STK_NOSYNC);
    return;
  }
  SERIAL.print((char) STK_INSYNC);
  if (memtype == 'F') result = flash_read_page(length);
  if (memtype == 'E') result = eeprom_read_page(length);
  SERIAL.print(result);
}

void read_signature() {
  if (CRC_EOP != getch()) {
    error++;
    SERIAL.print((char) STK_NOSYNC);
    return;
  }
  SERIAL.print((char) STK_INSYNC);
  uint8_t high = spi_transaction(0x30, 0x00, 0x00, 0x00);
  SERIAL.print((char) high);
  uint8_t middle = spi_transaction(0x30, 0x00, 0x01, 0x00);
  SERIAL.print((char) middle);
  uint8_t low = spi_transaction(0x30, 0x00, 0x02, 0x00);
  SERIAL.print((char) low);
  SERIAL.print((char) STK_OK);
}
//////////////////////////////////////////
//////////////////////////////////////////


////////////////////////////////////
////////////////////////////////////
void avrisp() {
  uint8_t ch = getch();
  switch (ch) {
    case '0': // signon
      error = 0;
      empty_reply();
      break;
    case '1':
      if (getch() == CRC_EOP) {
        SERIAL.print((char) STK_INSYNC);
        SERIAL.print("AVR ISP");
        SERIAL.print((char) STK_OK);
      }
      else {
        error++;
        SERIAL.print((char) STK_NOSYNC);
      }
      break;
    case 'A':
      get_version(getch());
      break;
    case 'B':
      fill(20);
      set_parameters();
      empty_reply();
      break;
    case 'E': // extended parameters - ignore for now
      fill(5);
      empty_reply();
      break;
    case 'P':
      if (!pmode)
        start_pmode();
      empty_reply();
      break;
    case 'U': // set address (word)
      here = getch();
      here += 256 * getch();
      empty_reply();
      break;

    case 0x60: //STK_PROG_FLASH
      getch(); // low addr
      getch(); // high addr
      empty_reply();
      break;
    case 0x61: //STK_PROG_DATA
      getch(); // data
      empty_reply();
      break;

    case 0x64: //STK_PROG_PAGE
      program_page();
      break;

    case 0x74: //STK_READ_PAGE 't'
      read_page();
      break;

    case 'V': //0x56
      universal();
      break;
    case 'Q': //0x51
      error = 0;
      end_pmode();
      empty_reply();
      break;

    case 0x75: //STK_READ_SIGN 'u'
      read_signature();
      break;

    // expecting a command, not CRC_EOP
    // this is how we can get back in sync
    case CRC_EOP:
      error++;
      SERIAL.print((char) STK_NOSYNC);
      break;

    // anything else we will return STK_UNKNOWN
    default:
      error++;
      if (CRC_EOP == getch())
        SERIAL.print((char)STK_UNKNOWN);
      else
        SERIAL.print((char)STK_NOSYNC);
  }
}
```

Now I could Burn Bootloader my Board. For that I clicked Tools-> And then Burn Bootloader

![43](../img/ed/43.jpg)

But I got an error so I had to check my Board
I then remove the pins from the Arduino to troubleshoot my Board. I took a multimeter to check for connections. My ground and VCC were connected together and my VCC track was a litlle burned.
I took a little solder to bridge my VCC. After that I checked it again with the multimeter and it was good.
So I tried Burning Bootloader again and it was all good.

![44](../img/ed/44.jpg)


**Atmega328p-au -Bootloader Burned**

For this we will use a 5V ftdi cable to connect the board with my laptop.The FTDI cable is a USB to Serial (TTL level) converter which allows for a simple way to connect TTL interface devices to USB. 
The I/O pins of this FTDI cable are configured to operate at 5V. And I had to remove the 1002 resistor and put the 4990 resistor.

I then opened Arduino-> I click on file-> Examples-> Basics-> Blink
I used the blink sketch for uploading 

![45](../img/ed/45.jpg)

I then click on tools-> I select my board to "hello.arduino.atmega328p"-> I changed my programmer to  "AVRISP mkll"
I connected my board to my laptop with the ftdi cable and I made sure my port was selected

![46](../img/ed/46.jpg)

I tried uploading the sketch by pressing the switch, but I got an error so I had to troubleshoot my board. After troubleshotting I found out that 
I had soldered the wrong capaciter. I had put the 1uf capaciter instead of the 10000pf. Some of the pins were not soldered enough so I had to put more solder.
And while trying to clean my reset track it was cut so I had to jumper my reset track with 0ohm resistor. I tried again to upload the sketch.

The 0ohm resistor on the board:

![47](../img/ed/47.jpg)



Connecting the board through the 5V ftdi cable:

![48](../img/ed/48.jpg)



I tried again to upload the sketch to my board by pressing the switch for uploading and it went. The led was blinking.

![49](../img/ed/49.jpg)


![50](../img/ed/50.jpg)

I succesfully programmed my board.



### References


- <ahref="myFile.js/" download>project1 kicad schema</a>

- <a href="myFile.js/" download>project1 kicad pcb</a>  

- <a href="myFile.js/" download>Gcode</a>  

- <a href="myFile.js/" download>netlist</a>  

- <a href="myFile.js/" download>kicadgbr</a>  



 








































