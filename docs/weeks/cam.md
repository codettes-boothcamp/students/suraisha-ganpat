## CAD-CAM

**CAD-Computer Aided Design**

This is the technology for design and technical documentation, which replaces manual drafting with an automated process. 
It is the process of creating product models / designs in 2D or 3D and constraints of both machine & materials.

For the CAD design I used the *OpenSCAD* software to design parametric.

**CAM-Computer Aided Manufacturing**

This is the process of creating Machine. For this the Gcode from the CAD  files is needed




### OpenSCAD

OpenSCAD is software for creating solid 3D CAD objects. It is free software and available for Linux/UNIX, MS Windows and Mac OS X.

*The machine:* 

- X,Y,Zmax
- Fmin-max
- S(Spindle)(rpm file)
- Controller: Grbl/Lpt/Serial/USB

*Material*

- Size
- Ty[pe materials

*Operation*

- Zmax=1/2d
- Smax=20K / Voor hout: Smax=8-10K
- Fmax=3000/ Voor hout: Fmax=1000
- Cut out: inside cut/outside cut/drill/mill



*OpenSCAD Libraries / Vitamins*

Vitamins are ready to use libraries of all sorts or components. 
Simply link library to your OpenSCAD environment and start using.

I downloaded the libraries from here [OpenSCADlibraries](https://github.com/nophead/NopSCADlib)

![1](../img/cam/1.jpg)



The location of the library is: this Pc-> Documents-> OpenSCAD-> libraries

![2](../img/cam/2.jpg)



Chose a few components from the Vitamin Library and align themin OpenSCAD. I didn't do that.

But make sure to include the library in the code.
For example:

```
include <NopSCADlib/vitamins/fuseholder.scad>
include <NopSCADlib/vitamins/geared_steppers.scad>

//fuse holder
translate([50, 0, 0])fuseholder(20);
//geared_stepper();
rotate([-90,0,0])geared_stepper(28BYJ_48);
```


**Speakerbox**

For exercising in OpenSCAD, I will try to design the back of a speakerbox.


Open the OpenSCAD software and click New

Then I tried to put the functions in it for the cube,the pocket and the electronicsport

```
difference(){  
    cube([200,150,9]);
    translate([20,020,0])
        cube([18,100,9]);
    //pocket
     translate([100,75,5])
        cylinder(10,12,12);
    //electronicsport
     translate([200-40,150/2,0])
        cylinder(30,30);
    
 }
```

After putting the function click on preview and render the design. The design will. if it doesn't look good try to play with the values of the sizes.
It looks like this:

![3](../img/cam/3.jpg)

But this is a 3D design, I can not export it to a 2D design. Now I have to make it a 2D design by putting 

```
projection(){
```

But the pocket wasn't showing, So I try to change the height of the cilinder, now the function is like this:

```
projection(){
difference(){  
    cube([200,150,9]);
    translate([20,020,0])
        cube([18,100,9]);
    //pocket
     translate([100,75,5])
        cylinder(4,12,12);
    //electronicsport
     translate([200-40,150/2,0])
        cylinder(30,30);
    
 }
}
```

After that, it becomes a 2D design and it looks like this:

![4](../img/cam/4.jpg)




After designing, I could export it to CAM as a SVG file.
Click on File-> Export-> Export as SVG

![5](../img/cam/5.jpg)




**CAM-Computer Aided Manufacturing**

- Process of creating Machine 
- Instructions (GCode) from CAD files
- Manage production rates
- Manage efficiency (materials & Time)


*CAM -  for 3D printing*

- [Download](https://ultimaker.com/software/ultimaker-cura) and install Cura
- Import STL (drag/drop)
- Resize and place parts
- Select printer, printer filament material & print settings
- Slice & Save gcode file to SD card


*Production -  3D print Anycubic 4Max Pro*

- Insert SD card on left side (upside down)
- Start printer
- Select “print” scroll to file of print
- Print and wait (printer will heat up first before starting)

Note: If printing ABS close upper lid but for PLA leave it off.

*CAM -  for CNC milling/cutting*

Aspects:
+ CNC Machine, Controller, constraints
+ Material (wood/plastic/steel)
+ Operations / Type (cut-out inner/outer, pocket,drilling)
+ Tools/Mills/Toolchange 

For each CNC/Material/Operation create table with:
- Tool type, diameter  (D-mm), 
- Feedrate (F - mm/min), Plunge Rate (mm/min), 
- Spindle RPM (S - rpm), 
- Pass-depth/step down (Z mm), Overlap if applicable (%)


After the 2D design in OpenScad, I will import the SVG file into the CAM. We didn't had a software for the CAM yet. So we worked online.

Open [MakercAM](https://www.makercam.com/). 

In MakerCAM generate GCode
(MakerCAM is discontinued, but to still  run it:
- enable flash in browser
- view page code -> Inspect Element
- Select HTML Element: Body ->Content -> Container
- Change CSS “height:100%” to “height:100vh” )

After opening Click on File-> Open SVG file

![6](../img/cam/6.jpg)




After opening the file, I will put the exact parameters for the productions.For this click on each part before editing the parameters.

Click on CAM and then on pocket operation.
Change the parameters:

Pocket

- tool diamter(mm): 6
- target depth(mm): -4
- safety height(mm): 10
- stock surface(mm): 0
- step over(%): 40
- step down(mm): 3
- roughing clearness(mm): 0
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

![7](../img/cam/7.jpg)




Click again on CAM and then on profile operation.

![8](../img/cam/8.jpg)

Change the parameters again:
Electrohole

- tool diamter(mm): 6
- target depth(mm): -9
- inside/outside cut: inside 
- safety height(mm): 10
- stock surface(mm): 0
- step down(mm): 3
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

![9](../img/cam/9.jpg)




Click again on CAM and then on profile operation.

![8](../img/cam/8.jpg)

Change the parameters again:
Basreflex

- tool diamter(mm): 6
- target depth(mm): -9
- inside/outside cut: inside 
- safety height(mm): 10
- stock surface(mm): 0
- step down(mm): 10
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

![10](../img/cam/10.jpg)


Click again on CAM and then on profile operation.

![8](../img/cam/8.jpg)

Change the parameters again:
Outside cut

- tool diamter(mm): 6
- target depth(mm): -9
- inside/outside cut: Outside 
- safety height(mm): 10
- stock surface(mm): 0
- step down(mm): 3
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

![11](../img/cam/11.jpg)




After editing the parameters, it looks like this:

![12](../img/cam/12.jpg)




Click on CAM-> And then click Calculate all:

![13](../img/cam/13.jpg)

![14](../img/cam/14.jpg)


Be sure to check the toolpath
Click on Toolpath-> And see if all part names is there that you selected.

![15](../img/cam/15.jpg)




If everything is there Click on CAM-> And then click on Export Gcode

![16](../img/cam/16.jpg)




I selected every Toolpath before exporting Gcode. After selected all toolpath click on *Export selected Toolpath*

![17](../img/cam/17.jpg)

I saved the Gcode and gave it a name. This Gcode I will need to cut the 2D design on a CNC machine.




**CNC machine**

CNC(Computer Numerical Control) is the automated control of machining tools (such as drills, boring tools, lathes) and 3D printers by means of a computer.
A CNC machine processes a piece of material (metal, plastic, wood, ceramic, or composite) to meet specifications by following a coded programmed instruction and without a manual operator.

The basic CNC machining process includes the following stages:

- Designing the CAD model
- Converting the CAD file to a CNC program
- Preparing the CNC machine
- Executing the machining operation

*CAD Model Design*

The CNC machining process begins with the creation of a 2D vector or 3D solid part CAD design either in-house or by a CAD/CAM design service company. 
Computer-aided design (CAD) software allows designers and manufacturers to produce a model or rendering of their parts and products along with the necessary technical specifications, such as dimensions and geometries, for producing the part or product.
And that is what I did. Designing in OpenScad and then Generate the Gcode in MakerCam.

*Machine Setup*
Before the operator runs the CNC program, they must prepare the CNC machine for operation. 
These preparations include affixing the workpiece directly into the machine, onto machinery spindles, or into machine vises or similar workholding devices, and attaching the required tooling, such as drill bits and end mills, to the proper machine components.

Once the machine is fully set up, the operator can run the CNC program.

So we loaded the Gcode to the CNC machine to cut the design.

The CNC machine. Always use a vacuum cleaner to clean all the dust.

![18](../img/cam/18.jpg)

![19](../img/cam/19.jpg)



The result:

![20](../img/cam/20.jpg)




### CAM -  LaserWEB4 / CNCWeb

LaserWeb / CNCWeb is an application for:

- generating GCODE from DXF/SVG/BITMAP/JPG/PNG files for Lasers and CNC Mills (= CAM Operations)
- controlling a connected CNC / Laser machine (running one of the supported firmwares)

Install laserWeb: [Install laserWeb](https://github.com/LaserWeb/LaserWeb4-Binaries/)

**Laser cutting**

Laser cutting is a type of thermal separation process. The laser beam hits the surface of the material and heats it so strongly that it melts or completely vaporizes. Once the laser beam has completely penetrated the material at one point, the actual cutting process begins. The laser system follows the selected geometry and separates the material in the process. 
Depending on the application, the use of process gases can positively influence the result.

The tooldiameter for laser cutting is 0.02 mm machine. Here we need a DKF/SVG file.

**The Laser Etching Process**

Laser etching, which is a subset of laser engraving, occurs when the heat from the beam causes the surface of the material to melt.

- The laser beam uses high heat to melt the surface of the material.
- The melted material expands and causes a raised mark.
- Unlike with engraving, the depth in etching is typically no more than 0.001”.

Here we need a DNG/BMP file


**The Laser Engraving Process**

Laser engraving is a process where the laser beam physically removes the surface of the material to expose a cavity that reveals an image at eye level.

- The laser creates high heat during the engraving process, which essentially causes the material to vaporize.
- It’s a quick process, as the material is vaporized with each pulse.
- This creates a cavity in the surface that is noticeable to the eye and touch.
- To form deeper marks with the laser engraver, repeat with several passes.

Here we need SVG/DXF file

**CNC operation VS Laser**

CNC operations:

- Outside cut
- Inside cut 
- Pocket- Drilling
- The tooldiamter here is 3-6 mm

Laser:

- Cutting
- Etching
- Engraving
- The tooldiameter here is 0.02 mm
 
I used the LaserWeb for laser cutting



**Laser Cutting**

Open LaserWeb software.

![21](../img/cam/21.jpg)




In MakerCam I Exported a Gcode and Saved it as SVG file. In the LaserWeb software add the SVG file.

![22](../img/cam/22.jpg)




Click on Setings-> There are the options for adding the exact parameters for the Laser cut.

![23](../img/cam/23.jpg)




Click on Machine to add the parameters

*Machine*

Dimensions:

- Machine Weight: 600 mm
- Machine Height: 840 mm

Origin offset
Show machine: OFF

- Machine left Y: 0 mm
- Machine buttom X: 0 mm

Tool Head:

- Beam: 0.2 mm

Probe tool:

- X/Y Probe OFFset: 0 mm
- Z Probe OFFset: 1.7 mm

Machine Z stage: OFF
Machine A stage: OFF
Air assit: ON

![24](../img/cam/24.jpg)


*File Setting*

Then click on File setting. 

![25](../img/cam/25.jpg)




*Gcode*

Now Click on Gcode

![26](../img/cam/26.jpg)

![27](../img/cam/27.jpg)




After adding all the parameters I made a New profile and named it: Stepcraft840-laser. And the I applied predefined machine profile

![28](../img/cam/28.jpg)

![29](../img/cam/29.jpg)

 
 
 
Drag the SVG file to the Gcode section. Select Laser cut.And gave it a name. Mine is Lasercut1. Change the laser poer to 100%, the passes to 1 and the cut Rate to 1500 mm/min

![30](../img/cam/30.jpg)




Generate the Gcode:

![31](../img/cam/31.jpg)




After generating the Gcode. Export the Gcode file and gave it a name:"Lasercut1"

![32](../img/cam/32.jpg)



Load the Gcode on the lasercut machine


**Production -  Stepcraft CNC**

StepCraft with UCCNC via UC100

+ Place and secure stock on CNC
+ Load GCode file into UCCNC
+ Set Workpiece zeros (X,Y zero)
+ Simulate to see if gcode will fit on stock by Jogging
+  Set Z- zero
+ Start spindle
+ Set Feed rate at 50%
+ Start Cycle on UCCNC
+ Gradually increase or decrease Feedrate%

If everything is set good. The design can be cut

![33](../img/cam/33.jpg)




The result:

![34](../img/cam/34.jpg)



### References

- [GCode the standard for Machine Control](https://roboticsandautomationnews.com/2018/01/26/how-to-become-a-g-code-master-with-a-complete-list-of-g-codes/15807/)

- [Install Cura](https://ultimaker.com/software/ultimaker-cura)

- [MakerCAM](https://www.makercam.com/)

- <a href="myFile.js/" download>OpenScad</a>

- <a href="myFile.js/" download>LaserWeb</a>
