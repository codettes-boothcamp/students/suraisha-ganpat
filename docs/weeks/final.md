## Emergency Keychain: **Safetywish**

As final project I want to build an emergency keychain for anyone. But especially for children that will give parents or any other person an alarm when they are in trouble. 

*I want the keychain to do is:*

- Send an alarm
- Send location
- Track

Then I want the keychain to show the time,date. It will need wifi/bluetooth to connect  to the other person. So I will have to program the watch to have a map. 

**Sensors**

- Esp12-E(Esp8266)
- Oled display
- 3-7 V battery
- TP4056
- Micro USB
- SMD switch
- SMD Tactile Switches(3)
- 10K resistors(9)
- Led(2)
- 3,3 V Regulator
- 2*10 uF Capaciter(2)

After that I research what sensors I will need for my electronic part. I found out I can use an ESP8266 for my electronic.
So first I try to build a simple electronic circuit with an OLED 0.91? 128×32 I2C White Display.


### My first Prototype

**ESP8266**

The ESP8266 is a low-cost Wi-Fi microchip, with a full TCP/IP stack and microcontroller capability, produced by Espressif Systems[
It is an highly integrated chip designed to provide full internet connectivity in a small package.

*Technical Features:*

- 802.11 b / g / n
- Wi-Fi Direct (P2P), soft-AP
- Built-in TCP / IP protocol stack
- Built-in TR switch, balun, LNA, power amplifier and matching network
- Built-in PLL, voltage regulator and power management components
- 802.11b mode + 19.5dBm output power
- Built-in temperature sensor
- Support antenna diversity
- off leakage current is less than 10uA
- Built-in low-power 32-bit CPU: can double as an application processor
- SDIO 2.0, SPI, UART
- STBC, 1×1 MIMO, 2×1 MIMO
- A-MPDU, A-MSDU aggregation and the 0.4 Within wake
- 2ms, connect and transfer data packets
- standby power consumption of less than 1.0mW (DTIM3)

![2](../img/fp/2.jpg)

![7](../img/fp/7.jpg)


*For the sake of simplicity, we will make groups of pins with similar functionalities*

**Power Pins** 
There are four power pins viz. one VIN pin & three 3.3V pins. The VIN pin can be used to directly supply the ESP8266 and its peripherals, if you have a regulated 5V voltage source. The 3.3V pins are the output of an on-board voltage regulator. These pins can be used to supply power to external components.

**GND** 
Is a ground pin of ESP8266 NodeMCU development board.

**I2C Pins** 
Are used to hook up all sorts of I2C sensors and peripherals in your project. Both I2C Master and I2C Slave are supported. I2C interface functionality can be realized programmatically, and the clock frequency is 100 kHz at a maximum. It should be noted that I2C clock frequency should be higher than the slowest clock frequency of the slave device.

**GPIO Pins**
SP8266 NodeMCU has 17 GPIO pins which can be assigned to various functions such as I2C, I2S, UART, PWM, IR Remote Control, LED Light and Button programmatically. Each digital enabled GPIO can be configured to internal pull-up or pull-down, or set to high impedance. When configured as an input, it can also be set to edge-trigger or level-trigger to generate CPU interrupts.

**ADC Channel** 
The NodeMCU is embedded with a 10-bit precision SAR ADC. The two functions can be implemented using ADC viz. Testing power supply voltage of VDD3P3 pin and testing input voltage of TOUT pin. However, they cannot be implemented at the same time.

**UART Pins** 
ESP8266 NodeMCU has 2 UART interfaces, i.e. UART0 and UART1, which provide asynchronous communication (RS232 and RS485), and can communicate at up to 4.5 Mbps. UART0 (TXD0, RXD0, RST0 & CTS0 pins) can be used for communication. It supports fluid control. However, UART1 (TXD1 pin) features only data transmit signal so, it is usually used for printing log.

**SPI Pins** 
ESP8266 features two SPIs (SPI and HSPI) in slave and master modes. 

These SPIs also support the following general-purpose SPI features:

- 4 timing modes of the SPI format transfer
- Up to 80 MHz and the divided clocks of 80 MHz
- Up to 64-Byte FIFO

SDIO Pins ESP8266 features Secure Digital Input/Output Interface (SDIO) which is used to directly interface SD cards. 4-bit 25 MHz SDIO v1.1 and 4-bit 50 MHz SDIO v2.0 are supported.

**PWM Pins** 
The board has 4 channels of Pulse Width Modulation (PWM). The PWM output can be implemented programmatically and used for driving digital motors and LEDs. PWM frequency range is adjustable from 1000 µs to 10000 µs, i.e., between 100 Hz and 1 kHz.

**Control Pins** 
Are used to control ESP8266. These pins include Chip Enable pin (EN), Reset pin (RST) and WAKE pin.

**EN pin** – The ESP8266 chip is enabled when EN pin is pulled HIGH. When pulled LOW the chip works at minimum power.
**RST pin** – RST pin is used to reset the ESP8266 chip.
**WAKE pin** – Wake pin is used to wake the chip from deep-sleep.



**OLED(Organic Light-Emitting Diode) 0.91" 128×32 I2C WHITE DISPLAY**

![8](../img/fp/8.jpg)


*DESCRIPTION*

This 0.91? OLED display has a resolution of 128 x 32 white pixels on a black background with an I2C interface for easy control by an MCU(Microcontrollers).


*KEY FEATURES:*

- Latest OLED light emitting technology
- 128 x 32 pixel resolution
- White on black monochrome display
- I2C interface uses only 2 pins on MCU
- SSD1306 OLED controller
- 3.3 or 5V operation
- These displays can pack a lot of information into a very small form factor.

*OLED Display*

OLED displays are the latest in display technology which is just now starting to be introduced in flat-panel televisions.  They emit light without requiring a backlight and so have excellent  viewing angle, brightness and contrast in a very thin and efficient package.
The display has a resolution of 128 x 32 pixels.  The pixels are white on a black background.
The module uses the SSD1306 controller and is compatible with software libraries that support that controller.
One thing to note about OLEDs is that they are similar to the old Plasma TVs in that they can retain an image if they are left on continuously displaying the same image.  They are best suited for applications where the screen is either turned off or the image is changed on occasion.

*I2C Interface*

This display incorporates an I2C interface that requires only 2 pins on the MCU to interface with and it has good library support to get up and running fast.
The default I2C address for these displays is 0x3C.  The address is marked on the bag that the module comes in.


*Module Connections*

Connection to the display is via a 4 pin header.

1 x 4 Header

GND –   Connect to system ground.  This ground needs to be in common with the MCU.
VCC –   Connect to 3.3 or 5V.  This can come from the MCU or be a separate power supply.
SCL –   Connect to the I2C / SCL pin on the MCU.
SDA –   Connect to the I2C / SDA pin on the MCU.


*Connect the display**

Connect VCC to 5V (or 3.3V) and GND to ground on the MCU.  The I2C lines just connect to the same lines on the MCU so SCL connects to SCL and SDA connects to SDA.

Install Libraries

Install the following 2 libraries if they are not already installed on your computer.  These can be installed from within the Arduino IDE.
- “Adafruit SSD1306”
- “Adafruit GFX Library”




**Setting up OLED Display I2C Connection with NodeMCU ESP8266**

I connected my ESP8266 to my OLED Display throught pins. I used four pins.

I connected:

- GND-> GND
- VCC( Voltage Common Collector-> 3V3
- SCL(Clock line)-> D1
- SDA(Data line)-> D2

![10](../img/fp/10.jpg)

I also connected my ESP8266 to my laptop throught a USB cable

To see if my ESP8266 is working, I just tried to display "hello world" on the OLED display.

For that I openend Arduino IDE. To begin I need to update the board manager with a custom URL. 
For this go to File-> Preferences

![3](../img/fp/3.jpg)


Copy this URL **http://arduino.esp8266.com/stable/package_esp8266com_index.json** into Additional Board Manager URLs text box situated on the bottom of the window:

![4](../img/fp/4.jpg)

After putting the URL I click "OK". 


Now I go Tools > Boards > Boards Manager. 

![5](../img/fp/5.jpg)


I Search for *ESP8266* for the board and install it.

![6](../img/fp/6.jpg)


After that I install the libraries that for the OLED display
For this I click on Sketch-> Include Library-> Manage Library

![9](../img/fp/9.jpg)


Then I search for *SSD1306* to install the library. After installing the library click Close.

![11](../img/fp/11.jpg)


After installing the library and the board for both ESP8266 and the OLED. I put the code in my Arduino IDE.

```
/*
 * https://circuits4you.com
 * ESP8266 NodeMCU Oled Display Code Example
 * 
 */

#include <ESP8266WiFi.h>
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`



// Initialize the OLED display using Wire library
SSD1306Wire  display(0x3c, D2, D1);  //D2=SDK  D1=SCK  As per labeling on NodeMCU

//=======================================================================
//                    Power on setup
//=======================================================================

void setup() {
  delay(1000);
  Serial.begin(115200);  
  Serial.println("");
  
  Serial.println("Initializing OLED Display");
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
}

//======================================================================
//                    Main Program Loop
//======================================================================

void loop() {
  drawFontFaceDemo();
  delay(1000);
  drawRectDemo();
  delay(1000);
  void drawCircleDemo();
  delay(1000);
}

void drawFontFaceDemo() {
  // clear the display
  display.clear();
    // Font Demo1
    // create more fonts at http://oleddisplay.squix.ch/
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "Hello world");
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 10, "Hello world");
    display.setFont(ArialMT_Plain_24);
    display.drawString(0, 26, "Hello world");
  // write the buffer to the display
  display.display();
}

void drawRectDemo() {
  // clear the display
  display.clear();
      // Draw a pixel at given position
    for (int i = 0; i < 10; i++) {
      display.setPixel(i, i);
      display.setPixel(10 - i, i);
    }
    display.drawRect(12, 12, 20, 20);

    // Fill the rectangle
    display.fillRect(14, 14, 17, 17);

    // Draw a line horizontally
    display.drawHorizontalLine(0, 40, 20);

    // Draw a line horizontally
    display.drawVerticalLine(40, 0, 20);
   // write the buffer to the display
  display.display();
}

void drawCircleDemo() {
  // clear the display
  display.clear();
  
  for (int i=1; i < 8; i++) {
    display.setColor(WHITE);
    display.drawCircle(32, 32, i*3);
    if (i % 2 == 0) {
      display.setColor(BLACK);
    }
    display.fillCircle(96, 32, 32 - i* 3);
  }

  // write the buffer to the display
  display.display();
}
```

After putting the code I selected my Port and my Board "NodeMCU 1.0(ESP-12E Module)".

![12](../img/fp/12.jpg)


After that I compiled my code to see if everything was correct. After that I upload my code to the ESP8266. 
And there it was:

![13](../img/fp/13.jpg)


So now my Oled display works. I will now try to put date and time on it which I will need on my watch.



**Internet Clock: Display Date and Time on OLED using ESP8266 NodeMCU with NTP**

I will use ESP8266 NodeMCU to get current time and date from NTP servers and display it on OLED display.


*NTP(Network Time Protocol)*

NTP is one of the oldest networking Internet Protocol (IP) for synchronizing clocks between computer networks. It was designed by David L. Mills of the University of Delaware in 1981. 
This protocol can be used to synchronize many networks to Coordinated Universal Time (UTC) within few milliseconds. 
UTC is the primary time standard by which world regulates clock and time. UTC does not changes and vary for different geographical locations. 
NTP uses UTC as the time reference and provides accurate and synchronized time across the Internet.

NTP works on a hierarchical client-server model. Top model has reference clocks known as “stratum0” like atomic clocks, radio waves, GPS, GSM which receives time from the satellite. 
The servers which receive time from stratum0 are called as “stratum1” and servers which receive time from stratum1 are called “stratum2” and so on. 
This goes on and the accuracy of time goes on decreasing after each stage. NTP automatically selects the best of several available time sources to synchronize which makes it fault-tolerant able protocol.

![14](../img/fp/14.jpg)

ESP8266 can access NTP servers using internet to get accurate time. Here NTP works in client-server mode, ESP8266 works as client device and connects with NTP servers using UDP (User Datagram Protocol). The client transmits a request packet to NTP servers and in return NTP sends a timestamp packet which consists information like accuracy, timezone, UNIX timestamp etc. 
Then client separates the date and time details which can be further used in applications according to requirement.


*Install Libraries*

I have installed NTPClient library by Taranais because it is easy to use and have functions to get date and time from NTP servers.

![15](../img/fp/15.jpg)


NTPClient library comes with examples. Open Arduino IDE and Go to Examples -> NTPClient _> Advanced. The code given in this sketch displays the time from NTP server on the serial monitor. 
I will use this sketch to display current time and date on OLED display. But this is code not complete. So I will have to add the specific code to let it ork

![16](../img/fp/16.jpg)


*Code explained*

ESP8266WiFi library provides ESP8266 specific Wi-Fi routines to connect to network. 
WiFiUDP.h handles sending and receiving UDP packages. I will use the SPI protocol to interface OLED with NodeMCU so I imported “SPI.h” library. 
And “Adafruit_GFX.h” and “Adafruit_SSD1306.h” are used for OLED Display.

```
#include <NTPClient.h>
#include <ESP8266WiFi.h> // provides ESP8266 specific Wi-Fi routines we are calling to connect to network
#include <WiFiUdp.h> //handles sending and receiving of UDP packages
#include <SPI.h> // SPI for interfacing OLED with NodeMCU
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
```


My OLED size is 128x32 so the setting screen width and height are 128 and 32 respectively. 
Define the variables for OLED pins connected to NodeMCU for SPI communication.



```
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
// Declaration for SSD1306 display connected using software SPI (default case):
#define OLED_SCL   D1
#define OLED_SDA   D2

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);
```


Replace “your_ssid” and “your_password” with your Wi-Fi SSID and password in the below lines of code.

```
const char *ssid     = "S.Abdoelaziz";
const char *password = "";
```


Set up WI-Fi connection by giving SSID and password to WiFi.begin function. 
The connection of ESP8266 takes some time to get connected to NodeMCU so we have to wait till it gets connected.


```
void setup(){
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  } 
```



To request date and time, initialize time client with address of NTP servers. For better accuracy choose the address of NTP servers which are close to your geographical area. 
Here we use “pool.ntp.org” which gives servers from worldwide. If you wish to choose servers from Asia you can use “asia.pool.ntp.org”. timeClient also takes UTC time offset in milliseconds of your timezone. 
The UTC of Suriname is -3.00. Converted this offset in milliseconds is equal (-3x60x60)+(0x60)= 10800

```
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", -10800,60000);
```



SSD1306_SWITCHCAPVCC is given to generate 3.3V internally to initialize the display. 
When the OLED starts it displays “WELCOME TO CIRCUIT DIGEST” with text size 2 and color BLUE for 3 seconds.
NTP client is initialized using  begin() function to set date and time from NTP servers.


```
  if(!display.begin(SSD1306_SWITCHCAPVCC))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  display.clearDisplay();
  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(5, 2);
  display.println("WELCOME TO");
  display.println("  CIRCUIT");
  display.println("  DIGEST");
  display.display();
  delay(3000);
  timeClient.begin();
}
```



Update() function is used to receive the date and time whenever we request to NTP servers.

```
void loop() {
  timeClient.update();
```


Baud rate of 115200 is set to print the time on serial monitor.

```
Serial.begin(115200);
```



getHours(), getMinutes(), getSeconds(), getDay are the library function and gives the current hour, minutes, seconds and day from NTP server. 
Code below is used to differentiate time between AM and PM. If the hour we get using getHours() is greater than 12 then we set that time as PM else its AM.

```
  int hh = timeClient.getHours();
  int mm = timeClient.getMinutes();
  int ss = timeClient.getSeconds();
  
  if(hh>12)
  {
    hh=hh-12;
    display.print(hh);
    display.print(":");
    display.print(mm);
    display.print(":");
    display.print(ss);
    display.println(" PM");
  }
  else
  {
    display.print(hh);
    display.print(":");
    display.print(mm);
    display.print(":");
    display.print(ss);
    display.println(" AM");   
  }

  int day = timeClient.getDay();
  display.println("'"+arr_days[day]+"'");
```



getFormattedDate() is used get date in “yyyy-mm-dd” format from NTP server. This function gives date and time in “yyyy-mm-dd T hh:mm:ss format. 
But I need only date so we have to split this string which is stored in date_time format till “T” which is done by substring() function and then store the date in “date” variable.

```
//date_time = timeClient.getFormattedDate();
    int index_date = date_time.indexOf("T");
    String date = date_time.substring(0, index_date);
    Serial.println(date);
    display.println(date);
    display.display();      // Show initial text
```



*The complete code*


```
#include <NTPClient.h>
#include <ESP8266WiFi.h> // provides ESP8266 specific Wi-Fi routines we are calling to connect to network
#include <WiFiUdp.h> //handles sending and receiving of UDP packages
#include <SPI.h> // SPI for interfacing OLED with NodeMCU
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
// Declaration for SSD1306 display connected using software SPI (default case):
#define OLED_SCL   D1
#define OLED_SDA   D2

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);


const char *ssid     = "S.Abdoelaziz";
const char *password = "";

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", -10800,60000);
String arr_days[]={"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
String date_time;
// You can specify the time server pool and the offset (in seconds, can be
// changed later with setTimeOffset() ). Additionaly you can specify the
// update interval (in milliseconds, can be changed using setUpdateInterval() ).
  

void setup(){
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }

  if(!display.begin(SSD1306_SWITCHCAPVCC))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  display.clearDisplay();
  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(5, 2);
  display.println("WELCOME TO");
  display.println("  CIRCUIT");
  display.println("  DIGEST");
  display.display();
  delay(3000);
  timeClient.begin();
}

void loop() {
  timeClient.update();
  
  display.clearDisplay();
  
  Serial.println(timeClient.getFormattedTime());
  
  display.setTextSize(1); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  int hh = timeClient.getHours();
  int mm = timeClient.getMinutes();
  int ss = timeClient.getSeconds();
  
  if(hh>12)
  {
    hh=hh-12;
    display.print(hh);
    display.print(":");
    display.print(mm);
    display.print(":");
    display.print(ss);
    display.println(" PM");
  }
  else
  {
    display.print(hh);
    display.print(":");
    display.print(mm);
    display.print(":");
    display.print(ss);
    display.println(" AM");   
  }

  int day = timeClient.getDay();
  display.println("'"+arr_days[day]+"'");
  

//date_time = timeClient.getFormattedDate();
    int index_date = date_time.indexOf("T");
    String date = date_time.substring(0, index_date);
    Serial.println(date);
    display.println(date);
    display.display();      // Show initial text
}
```



After that I made sure I select my Board: "NodeMCU 1.0(ESP-12E Module)" and my port before uploading. I compiled my sketch to see if it was okay. 
Then I upload my code.

I made my Oled show the date and time.

![17](../img/fp/17.jpg)


For the next step I want to put a GPS in my watch so for that I have to make my GPS work for. So now I am going to test that.


### GPS Module Interfacing with NodeMCU ESP8266: Showing the Latitude and Longitude on Webpage

*NodeMCU* is an open source IoT platform, contains firmware which runs on the ESP8266 Wi-Fi SoC from Espressif Systems and the hardware is based on the ESP-12 module. 
One of the speciality of NodeMCU is that it can be simply programmed using Arduino IDE
NodeMCU have the advantages of both ESP-8266 and Arduino. It got enough I/O pins and can connect to computer using a micro USB cable.

*Components Required*

- NodeMCU ESP8266
- GPS module
- Jumper wires
- Breadboard

I have already Configure my Arduino IDE to program NodeMCU ESP8266. And I have already installed my board for ESP8266.
So I started wiring my components

**NEO-6M GY-GPS6MV2 GPS module**

It comes with an external antenna, and does’t come with header pins. So, you’ll need to get and solder some.
GPS stands for Global Positioning System and it is used to find out location, altitude, speed, date and time in UTC. 

*It works like this:*

- The GPS module receives data from satellites;
- Nodemcu board receives GPS data from the GPS module;
- Nodemcu board prints GPS data on the white 0.96" I2C OLED display module


**Circuit Diagram**

![20](../img/fp/20.jpg)

I used this digram to connect my components.

The connection from GPS module to NodeMCU 

- GND -> GND
- VCC pin -> 3V3 pin
- RXD -> D1
- TXD-> D2

After connecting it looks like this 

![18](../img/fp/18.jpg)

The program is mainly divided into two parts. The first part is to take the location data from the GPS module and second part is to send it over WiFi to a webpage.


**Programming NodeMCU with Arduino IDE for sending GPS data to Local server**

NodeMCU can be easily programmed using ArduinoIDE, just connect NodeMCU to the computer using a microUSB cable.

I then downloaded the library for the GPS. Fo this go to the link <ahref="myFile.js/" download>TinyGPS library</a>.
After that Open Arduino IDE and include the library.
For this  go to Sketch-> Include library-> Add . ZIP library.

![19](../img/fp/19.jpg)

I did the same thing for SofwareSerial Library. Download it from here <ahref="myFile.js/" download>SoftwareSerial library</a>

So now I can upload my code. Before that I will explain it a little bit.

*Code explained*

Then include all the required libraries and define all the variables in program as shown below. To connect NodeMCU to your WiFi network we need to add the WiFi network’s SSID and Password inside the code. 
Make sure to change the SSID and WiFi password inside the code.

```
#include <TinyGPS++.h> // library for GPS module
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
TinyGPSPlus gps;  // The TinyGPS++ object
SoftwareSerial ss(4, 5); // The serial connection to the GPS device
const char* ssid = "S.Abdoelaziz"; //ssid of your wifi
const char* password = "your password"; //password of your wifi
float latitude , longitude;
int year , month , date, hour , minute , second;
String date_str , time_str , lat_str , lng_str;
int pm;
WiFiServer server(80);
```



Inside the setup() function, begin with the baud rate as 115200. After this initialize the WiFi connection using the WiFi SSID and password. 
Once the NodeMCU is connected to the WiFi network a local IP address will be generated and displayed on the serial monitor. 
This IP address will be used to access the GPS location data from your phone or laptop

```
void setup()
{
  Serial.begin(115200);
  ss.begin(9600);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password); //connecting to wifi
  while (WiFi.status() != WL_CONNECTED)// while wifi not connected
  {
    delay(500);
    Serial.print("."); //print "...."
  }
  Serial.println("");
  Serial.println("WiFi connected");
  server.begin();
  Serial.println("Server started");
  Serial.println(WiFi.localIP());  // Print the IP address
}
```



Inside the loop() function, we are continuously checking if the GPS data is available and if it is available we extract the latitude, longitude, date and time from the NMEA string received from the GPS module.
The data from the GPS module is continue string of 28 characters which will contain Latitude, Longitude, Date, Time and many other information. It is called [ NMEA string](http://aprs.gids.nl/nmea/)

```
$GPGGA,104534.000,7791.0381,N,06727.4434,E,1,08,0.9,510.4,M,43.9,M,,*47
$GPGGA,HHMMSS.SSS,latitude,N,longitude,E,FQ,NOS,HDP,altitude,M,height,M,,checksum data
```



TinyGPS library has inbuilt function to get the required data from the NMEA string. So here in the below code we extract the latitude, longitude, date and time from the string and stored the values in 
the variables “lat_str”, “lng_str, “date_str” and “time_str” respectively.
Time that we get from the GPS module will be in GMT format. So first separate the time data from the long string then convert this to local time format

```
void loop()
{
  while (ss.available() > 0) //while data is available
    if (gps.encode(ss.read())) //read gps data
    {
      if (gps.location.isValid()) //check whether gps location is valid
      {
        latitude = gps.location.lat();
        lat_str = String(latitude , 6); // latitude location is stored in a string
        longitude = gps.location.lng();
        lng_str = String(longitude , 6); //longitude location is stored in a string
      }
      if (gps.date.isValid()) //check whether gps date is valid
      {
        date_str = "";
        date = gps.date.day();
        month = gps.date.month();
        year = gps.date.year();
        if (date < 10)
         date_str = '0';
        date_str += String(date);// values of date,month and year are stored in a string
        date_str += " / ";
```



Now add HTML code to display the GPS data on the webpage. So here all the HTML code is embedded into a variable called “s”, 
then this variable is printed using client.print(s) to send all embedded HTML code to webpage.

```
WiFiClient client = server.available(); // Check if a client has connected
  if (!client)
  {
    return;
  }
  // Prepare the response
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n <!DOCTYPE html> <html> <head> <title>GPS DATA</title> <style>";
  s += "a:link {background-color: YELLOW;text-decoration: none;}";
  s += "table, th, td </style> </head> <body> <h1  style=";
  s += "font-size:300%;";
  s += " ALIGN=CENTER> GPS DATA</h1>";
  s += "<p ALIGN=CENTER style=""font-size:150%;""";
  s += "> <b>Location Details</b></p> <table ALIGN=CENTER style=";
  s += "width:50%";
  s += "> <tr> <th>Latitude</th>";
  s += "<td ALIGN=CENTER >";
  s += lat_str;
  s += "</td> </tr> <tr> <th>Longitude</th> <td ALIGN=CENTER >";
  s += lng_str;
  s += "</td> </tr> <tr>  <th>Date</th> <td ALIGN=CENTER >";
  s += date_str;
  s += "</td></tr> <tr> <th>Time</th> <td ALIGN=CENTER >";
  s += time_str;
  s += "</td>  </tr> </table> ";
  s += "</body> </html> \n";
  client.print(s); // all the values are send to the webpage
  delay(100);
}
```


**The complete code**

```
#include <TinyGPS++.h> // library for GPS module
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
TinyGPSPlus gps;  // The TinyGPS++ object
SoftwareSerial ss(4, 5); // The serial connection to the GPS device
const char* ssid = "S.Abdoelaziz"; //ssid of your wifi
const char* password = "shafsharfatfaz17"; //password of your wifi
float latitude , longitude;
int year , month , date, hour , minute , second;
String date_str , time_str , lat_str , lng_str;
int pm;
WiFiServer server(80);

void setup()
{
  Serial.begin(115200);
  ss.begin(9600);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password); //connecting to wifi
  while (WiFi.status() != WL_CONNECTED)// while wifi not connected
  {
    delay(500);
    Serial.print("."); //print "...."
  }
  Serial.println("");
  Serial.println("WiFi connected");
  server.begin();
  Serial.println("Server started");
  Serial.println(WiFi.localIP());  // Print the IP address
}


void loop()
{
  while (ss.available() > 0) //while data is available
    if (gps.encode(ss.read())) //read gps data
    {
      if (gps.location.isValid()) //check whether gps location is valid
      {
        latitude = gps.location.lat();
        lat_str = String(latitude , 6); // latitude location is stored in a string
        longitude = gps.location.lng();
        lng_str = String(longitude , 6); //longitude location is stored in a string
      }
      if (gps.date.isValid()) //check whether gps date is valid
      {
        date_str = "";
        date = gps.date.day();
        month = gps.date.month();
        year = gps.date.year();
        if (date < 10)
          date_str = '0';
        date_str += String(date);// values of date,month and year are stored in a string
        date_str += " / ";

        if (month < 10)
          date_str += '0';
        date_str += String(month); // values of date,month and year are stored in a string
        date_str += " / ";
        if (year < 10)
          date_str += '0';
        date_str += String(year); // values of date,month and year are stored in a string
      }
      if (gps.time.isValid())  //check whether gps time is valid
      {
        time_str = "";
        hour = gps.time.hour();
        minute = gps.time.minute();
        second = gps.time.second();
        minute = (minute + 30); // converting to IST
        if (minute > 59)
        {
          minute = minute - 60;
          hour = hour + 1;
        }
        hour = (hour + 5) ;
        if (hour > 23)
          hour = hour - 24;   // converting to IST
        if (hour >= 12)  // checking whether AM or PM
          pm = 1;
        else
          pm = 0;
        hour = hour % 12;
        if (hour < 10)
          time_str = '0';
        time_str += String(hour); //values of hour,minute and time are stored in a string
        time_str += " : ";
        if (minute < 10)
          time_str += '0';
        time_str += String(minute); //values of hour,minute and time are stored in a string
        time_str += " : ";
        if (second < 10)
          time_str += '0';
        time_str += String(second); //values of hour,minute and time are stored in a string
        if (pm == 1)
          time_str += " PM ";
        else
          time_str += " AM ";
      }
    }
 
 WiFiClient client = server.available(); // Check if a client has connected
  if (!client)
  {
    return;
  }
  // Prepare the response
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n <!DOCTYPE html> <html> <head> <title>GPS DATA</title> <style>";
  s += "a:link {background-color: YELLOW;text-decoration: none;}";
  s += "table, th, td </style> </head> <body> <h1  style=";
  s += "font-size:300%;";
  s += " ALIGN=CENTER> GPS DATA</h1>";
  s += "<p ALIGN=CENTER style=""font-size:150%;""";
  s += "> <b>Location Details</b></p> <table ALIGN=CENTER style=";
  s += "width:50%";
  s += "> <tr> <th>Latitude</th>";
  s += "<td ALIGN=CENTER >";
  s += lat_str;
  s += "</td> </tr> <tr> <th>Longitude</th> <td ALIGN=CENTER >";
  s += lng_str;
  s += "</td> </tr> <tr>  <th>Date</th> <td ALIGN=CENTER >";
  s += date_str;
  s += "</td></tr> <tr> <th>Time</th> <td ALIGN=CENTER >";
  s += time_str;
  s += "</td>  </tr> </table> ";
  
   if(gps.location.isValid())
  {
   s += "<a href=\"http://maps.google.com/maps?&z=15&mrt=yp&t=k&q=";
   s += lat_str;
   s += '+';
   s += lng_str;
   s += "\">Click here!To check the location in Google maps.</a>";
  }

  s += "</body> </html> \n";
  client.print(s); // all the values are send to the webpage
  delay(100);
}
```


After adding the code now I can upload it. For this I connected my USB cable to my laptop. I selected my Board which is "NodeMCU 0.9(ESp-12 Module). And then I sleected my port.
I uploaded my code. I got some error which I fixed. And then I uploaded my code succesfully

![21](../img/fp/21.jpg)


After that I opened my Serial Monitor to see the IP Adress:

![22](../img/fp/22.jpg)


I used the I adress to open my webpage

![23](../img/fp/23.jpg)


To see if it works> I used the latitude and longitude to display the location. I tried it with google maps. And it shows me the locatiom.

![24](../img/fp/24.jpg)


After this I put the link of the google maps in my HTML to display it on my webserver. With this you can go directly to google maps to see the location.

This is the piece of code that I put in my code:

```
   if(gps.location.isValid())
  {
   s += "<a href=\"http://maps.google.com/maps?&z=15&mrt=yp&t=k&q=";
   s += lat_str;
   s += '+';
   s += lng_str;
   s += "\">Click here!To check the location in Google maps.</a>";
  }
```

After uploading my code again I see this on my webpage:

![35](../img/fp/35.jpg)


### NodeMCU ESP8266 12e With GPS & OLED Display

Now I am going to try to display the coordinates from the GPS on my Oled Display. Fo that I am going to use the same Oled I had used before.
I have to connect both the GPS module and my Oled to my ESP8266. So first I tried to connect everything on a breadboard but after so many times it didn't work.
I also hadn't solder my pins enough to the GPS. So make sure to do that properly.

After that I wired evrything up without using a breadboard.

For GPS module to ESP8266:

- GND -> GND
- VCC -> 3V3
- TX -> D8
- RX -> D7

Before I was using D6 to connect to TX, Because that was what my research shows. But after failing so many times. I take a closer look at my ESP8266 pins. And there I saw it should 
be the D8 for the TX pin.

For Oled to ESP8266:

- GND -> GND
- VCC -> 3V3
- SCL -> D1
- SDA -> D2

After connecting both my Oled and the GPS module. It looks like this. It looks a little messy but this was working better for me. And some of my jumpers wasn't that good.

![25](../img/fp/25.jpg)

After this I opened my code:
In the code change longitudeand latitude coordinates. And the RXPin, TXPin.

```
/*
  Project: ESP8266 ESP-12E module, NEO-6M GY-GPS6MV2 GPS module, 0.96" I2C OLED display module
  Function: This sketch listen to the GPS serial port,
  and when data received from the module, it's displays GPS data on 0.96" I2C OLED display module.

  ESP8266 ESP-12E module -> NEO-6M GY-GPS6MV2 GPS module

  VV (5V)     VCC
  GND         GND
  D8 (GPIO12) TX
  D7 (GPIO13) RX

  ESP8266 ESP-12E module -> White 0.96" I2C OLED display module

  3V3        VCC
  GND        GND
  D1 (GPIO5) SCL
  D2 (GPIO4) SDA

*/
#include <SoftwareSerial.h>                                             // include library code to allow serial communication on other digital pins of the Arduino board
#include <TinyGPS++.h>                                                  // include the library code for GPS module
#include <Adafruit_ssd1306syp.h>                                        // include Adafruit_ssd1306syp library for OLED display

// constants defined here:
                                                                        
static const int RXPin = D7, TXPin = D8;                                // GPS module RXPin - GPIO 12 and TXPin - GPIO 13
static const uint32_t GPSBaud = 9600;                                   // GPS module default baud rate is 9600
static const double WAYPOINT_LAT = 5.841740 , WAYPOINT_LON = -55.154580; // change coordinates of your waypoint

Adafruit_ssd1306syp display(4, 5);                                      // create OLED display object (display (SDA, SCL)), SDA - GPIO 4, SCL - GPIO 5
TinyGPSPlus gps;                                                        // create the TinyGPS++ object
SoftwareSerial ss(RXPin, TXPin);                                        // set the software serial connection to the GPS module

void setup()
{
  display.initialize();                                 // initialize OLED display
  display.clear();                                      // clear OLED display
  display.setTextColor(WHITE);                          // set the WHITE color
  display.setTextSize(2);                               // set the font size 1
  display.setCursor(5, 2);                              // set the cursor to the OLED display top left corner (column 0, row 0)
  display.println("Acoptex GPS-Timing");                // print text on OLED display
  display.println("Check for more");                    //
  display.println("DIY projects on");                   //
  display.println("http://acoptex.com");                //
  display.update();                                     // update OLED display
  delay(3000);                                          // set delay for 3 seconds
  ss.begin(GPSBaud);                                    // initialise serial communication with GPS module

}
void displayinfo()
{
  display.clear();
  display.setCursor(5, 2);
  //display.print("DATE:    ");
  //display.print(gps.date.day());
  //display.print("-");
  //display.print(gps.date.month());
  //display.print("-");
  //display.println(gps.date.year());
  display.print("LATITUDE : ");
  display.println(gps.location.lat(), 6);
  display.print("LONGITUDE: ");
  display.println(gps.location.lng(), 6);
  display.print("UTC TIME : ");
  display.print(gps.time.hour());
  display.print(":");
  display.print(gps.time.minute());
  display.print(":");
  display.println(gps.time.second());
  display.print("ALTITUDE : ");
  display.print(gps.altitude.meters(), 2);
  display.println(" m");
  //display.print(gps.altitude.feet(), 2);
  //display.println(" ft");
  display.print("SATELLITE: ");
  display.println(gps.satellites.value());
  display.print("SPEED    : ");
  display.print(gps.speed.kmph(), 2);    
  display.println(" km/h");
  //display.print(gps.speed.mph()), 2);
  //display.println(" mph");
  display.print("COURSE   : ");
  display.print(gps.course.deg(), 2);
  display.println(" deg.");
  unsigned long distanceKmToWaypoint =
    (unsigned long)TinyGPSPlus::distanceBetween(
      gps.location.lat(),
      gps.location.lng(),
      WAYPOINT_LAT,
      WAYPOINT_LON) / 1000;
  // double courseToWaypoint =
  //   TinyGPSPlus::courseTo(
  //     gps.location.lat(),
  //     gps.location.lng(),
  //     WAYPOINT_LAT,
  //     WAYPOINT_LON);
  display.print("KM TO WPT: ");
  display.println(distanceKmToWaypoint);
  //display.print("HDG TO WPT: ");
  //display.println(courseToWaypoint);   
  display.update();
  delay(100);
}
void loop()
{
  displayinfo();                                        // run procedure displayinfo
  smartDelay(500);                                      // run procedure smartDelay
  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    display.setCursor(0, 0);
    display.println("No GPS detected:");
    display.println(" check wiring.");
    display.println("More projects are");
    display.println(" on Acoptex.com");
  } 
  } 
  static void smartDelay(unsigned long ms)             // custom version of delay() ensures that the gps object is being "fed".
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}
```

I already had the TinyGPS library installed. I only had to install the Adafruit_SSD1306syp library. Download from here: <ahref="myFile.js/" download> Adafruit_SSD1306syp</a>
After donwloading I inslude the library by going to Sketch-> Include Library-> Add Zip.Library

I try to uploading code to my ESP8266. First it wasn't showing anything. So I try remove my jumpers and put it again. Again it wasn't going. I research it and it said
I should take of the GPS antenna and put it again. After that I uploaded my code again. And then it shows something on my Oled. The text wasn't showing clearly.
So I change my textSize in my code to 2. Then I saw it better.
But still it wasn't showing the coordinates. I then search it again why the GPS wasn't responding. I found out I had to wait a few minutes. I waited a while and a yellow
light starts blinking. The GPS was searching for satellites. But I think It couldn't find them. Because the yellow blinking light should be blue. But it wasn't becoming blue.
On my Oled it it was showing for the longitude and latitude 0.00000. It wasn't giving me the coordinates.

![26](../img/fp/26.jpg)

Here it was showing 0.000

![27](../img/fp/27.jpg)


### GSM/GPRS module

SIM800L GSM/GPRS module is a miniature GSM modem, which can be integrated into a great number of IoT projects. You can use this module to accomplish almost anything a normal cell phone can; SMS text messages, Make or receive phone calls, connecting to internet through GPRS, TCP/IP, and more! 
To top it off, the module supports quad-band GSM/GPRS network, meaning it works pretty much anywhere in the world.

The operating voltage of the chip is from 3.4V to 4.4V, which makes it an ideal candidate for direct LiPo battery supply.

All the necessary data pins of SIM800L GSM chip are broken out to a 0.1″ pitch headers. This includes pins required for communication with a microcontroller over UART. 
The module supports baud rate from 1200bps to 115200bps with Auto-Baud detection.

![28](../img/fp/28.jpg)


*Specification*

- Recommended supply voltage: 4V
- Power consumption:
- sleep mode < 2.0mA
- idle mode < 7.0mA
- GSM transmission (avg): 350 mA
- GSM transmission (peek): 2000mA
- Module size: 25 x 23 mm
- Interface: UART (max. 2.8V) and AT commands
- SIM card socket: microSIM (bottom side)
- Supported frequencies: Quad Band (850 / 950 / 1800 /1900 MHz)
- Antenna connector: IPX
- Status signaling: LED
- Working temperature range: -40 do + 85 ° C

*LED Status Indicators*

There is an LED on the top right side of the SIM800L Cellular Module which indicates the status of your cellular network. 
It’ll blink at various rates to show what state it’s in.

- Blynk every 1 sec: The module is running but hasn’t made connection to the cellular network yet.
- Blynk every 2s: The GPRS data connection you requested is active.
- Blink every 3s: The module has made contact with the cellular network & can send/receive voice and SMS

3dBi GSM antenna along with a U.FL to SMA adapter works better for this Sim

![29](../img/fp/29.jpg)


*Supplying Power for SIM800L module*

One of the most important parts of getting the SIM800L module working is supplying it with enough power.

Depending on which state it’s in, the SIM800L can be a relatively power-hungry device. The maximum current draw of the module is around 2A during transmission burst. It usually won’t pull that much, but may require around 216mA during phone calls or 80mA during network transmissions.
This chart from the datasheet summarizes what you may expect:

![30](../img/fp/30.jpg)

Since SIM800L module doesn’t come with onboard voltage regulator, an external power supply adjusted to voltage between 3.4V to 4.4V (Ideal 4.1V) is required. 
The power supply should also be able to source 2A of surge current, otherwise the module will keep shutting down
The 3.7V lipo Battery is an option:

![31](../img/fp/31.jpg)


*Pinout (bottom side - right):*

- NET - antenna
- VCC - supply voltage
- RESET - reset
- RXD - serial communication
- TXD - serial communication
- GND - ground


**Setting up my GSM Module**

I want to test my GSM Module by simple sending a message to my phone. For that I used a arduino, my GSM module, a power supply and 10K ohm resistor. First I had tried it without
the power supply and without the resistor. I had just connected my GSM module to my arduino. I opened my sketch. Put a simcard in it and try to make it work. But 
I had alot of problem doing that. My GSM wasn't connecting to the network. I tried it several times. Still with no succes. I kept researching what my problem was. So I found 
out I had to bridge the my RX pin with 10K resistors and I needed a power supply. The GSM module didn't get enough power.

*Wiring – Connecting SIM800L GSM module to Arduino UNO*

I started connecting the antenna then I inserted a fully activated Micro SIM card in the socket. After that I connected Tx pin on module to digital pin#3 on Arduino as we’ll be using software serial to talk to the module.

I cannot directly connect Rx pin on module to Arduino’s digital pin as Arduino Uno uses 5V GPIO whereas the SIM800L module uses 3.3V level logic and is NOT 5V tolerant. This means the Tx signal coming from the Arduino Uno must be stepped down to 3.3V so as not to damage the SIM800L module. 
There are several ways to do this but the easiest way is to use a simple resistor divider. A 10K resistor between SIM800L Rx and Arduino D2, and 20K between SIM800L Rx and GND would work fine.
I used a 9V battery to power my GSM module. That wasn't still working for me and I took a bigger power supply. 

It should Look like this:

![33](../img/fp/33.jpg)

I wired up my GSM module to my arduino Uno:

![32](../img/fp/32.jpg)


After wiring it I tried a Code to see if it works:


```
#include <SoftwareSerial.h>

//Create software serial object to communicate with SIM800L
SoftwareSerial mySerial(3, 2); //SIM800L Tx & Rx is connected to Arduino #3 & #2

void setup()
{
  //Begin serial communication with Arduino and Arduino IDE (Serial Monitor)
  Serial.begin(115200);
  
  //Begin serial communication with Arduino and SIM800L
  mySerial.begin(9600);

  Serial.println("Initializing...");
  delay(1000);

  mySerial.println("AT"); //Once the handshake test is successful, it will back to OK
  updateSerial();
  mySerial.println("AT+CSQ"); //Signal quality test, value range is 0-31 , 31 is the best
  updateSerial();
  mySerial.println("AT+CCID"); //Read SIM information to confirm whether the SIM is plugged
  updateSerial();
  mySerial.println("AT+CREG?"); //Check whether it has registered in the network
  updateSerial();
}

void loop()
{
  updateSerial();
}

void updateSerial()
{
  delay(500);
  while (Serial.available()) 
  {
    mySerial.write(Serial.read());//Forward what Serial received to Software Serial Port
  }
  while(mySerial.available()) 
  {
    Serial.write(mySerial.read());//Forward what Software Serial received to Serial Port
  }
}
```

After compiling my code. I uploaded my code.I made sure I select my board: *Arduino Uno* and my port. After uploading. I opened my serial monitor and used the commands
in my serial monitor to check it worked. If it is connecting or working. It will reply with ok.

The commands I used:

- AT+CREG : Network registration

- AT+COPS? : For connecting to the network

- ATD+597yournumber; : For calling on a number

- AT+CHUP : For hanging up the call

- AT+CMGF=1 : for putting in text mode

- AT+CMGS="+5978819918" : For sending messages

See the list of commands here: [List of commands](https://doc.qt.io/archives/qtextended4.4/atcommands.html) 

I only could make a call. I couldn't send a message yet. I still have to find that out. After trying I could send a message using the commands>

![34](../img/fp/34.jpg)


### The Next Step Of My Prototype

The next step is to send the GPS location throught sms. For this I have to connect my GPS module to my circuit.
After connecting to my circuit. I again had problems with the power and my GSM module didn't worked. So after trying several times and failing several times. I change my mind and 
took een TTGP T Call ESP32 to work with it. This ESP32 has a sim module in it. So I thought it would be easer to work with it. But the problem was that this ESP32 is to big
to make a watch. I then again change my mind to make a keychain of it. The concept will stay the same.


**TTGO T-Call ESP32 with SIM800L GSM/GPRS**


This ESP can communicate over Wi-Fi and Bluetooth, but also using SMS or phone calls. You can also connect the ESP32 to the internet using the simcard and a data plan.

![36](../img/fp/36.jpg)

This works with a 2G network and a nano simcard.It needs a C cable to connect it to the computer. It needs a power supply. For this you can use a 3.7V battery.

*TTGO T-Call Sim800L Pinout*

![37](../img/fp/37.jpg)

First I tested the ESP32 to send a simple message to my phone. I only connect
The code I used:

```
/*

*/

// SIM card PIN (leave empty, if not defined)
const char simPIN[]   = "";

// Your phone number to send SMS: + (plus sign) and country code, for Portugal +351, followed by phone number
// SMS_TARGET Example for Portugal +351XXXXXXXXX
#define SMS_TARGET  "+5978819918"

// Configure TinyGSM library
#define TINY_GSM_MODEM_SIM800      // Modem is SIM800
#define TINY_GSM_RX_BUFFER   1024  // Set RX buffer to 1Kb

#include <Wire.h>
#include <TinyGsmClient.h>

// TTGO T-Call pins
#define MODEM_RST            5
#define MODEM_PWKEY          4
#define MODEM_POWER_ON       23
#define MODEM_TX             27
#define MODEM_RX             26
#define I2C_SDA              21
#define I2C_SCL              22

// Set serial for debug console (to Serial Monitor, default speed 115200)
#define SerialMon Serial
// Set serial for AT commands (to SIM800 module)
#define SerialAT  Serial1

// Define the serial console for debug prints, if needed
//#define DUMP_AT_COMMANDS

#ifdef DUMP_AT_COMMANDS
  #include <StreamDebugger.h>
  StreamDebugger debugger(SerialAT, SerialMon);
  TinyGsm modem(debugger);
#else
  TinyGsm modem(SerialAT);
#endif

#define IP5306_ADDR          0x75
#define IP5306_REG_SYS_CTL0  0x00

bool setPowerBoostKeepOn(int en){
  Wire.beginTransmission(IP5306_ADDR);
  Wire.write(IP5306_REG_SYS_CTL0);
  if (en) {
    Wire.write(0x37); // Set bit1: 1 enable 0 disable boost keep on
  } else {
    Wire.write(0x35); // 0x37 is default reg value
  }
  return Wire.endTransmission() == 0;
}

void setup() {
  // Set console baud rate
  SerialMon.begin(115200);

  // Keep power when running from battery
  Wire.begin(I2C_SDA, I2C_SCL);
  bool isOk = setPowerBoostKeepOn(1);
  SerialMon.println(String("IP5306 KeepOn ") + (isOk ? "OK" : "FAIL"));

  // Set modem reset, enable, power pins
  pinMode(MODEM_PWKEY, OUTPUT);
  pinMode(MODEM_RST, OUTPUT);
  pinMode(MODEM_POWER_ON, OUTPUT);
  digitalWrite(MODEM_PWKEY, LOW);
  digitalWrite(MODEM_RST, HIGH);
  digitalWrite(MODEM_POWER_ON, HIGH);

  // Set GSM module baud rate and UART pins
  SerialAT.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);
  delay(3000);

  // Restart SIM800 module, it takes quite some time
  //To skip it, call init() instead of restart()
  SerialMon.println("Initializing modem...");
  modem.restart();
  //use modem.init() if you don't need the complete restart;

  // Unlock your SIM card with a PIN if needed
  if (strlen(simPIN) && modem.getSimStatus() != 3 ) {
    modem.simUnlock(simPIN);
  }

  // To send an SMS, call modem.sendSMS(SMS_TARGET, smsMessage)
  String smsMessage = "Hello from ESP32!";
  if(modem.sendSMS(SMS_TARGET, smsMessage)){
    SerialMon.println(smsMessage);
  }
  else{
    SerialMon.println("SMS failed to send");
  }
}

void loop() {

}
```

After uploading the code, I selected my port and my board: "ESP32 Dev Module"

![38](../img/fp/38.jpg)

So now I have to connect the GPS module, the switch and the oled to the ESP32.
I had to solder the pins and the switch.

![39](../img/fp/39.jpg)

![40](../img/fp/40.jpg)


I then checked how I will connect my GPS module, my oled, and my switch to my ESP32.
I first connected 


**Enclosure**

I first designed my enclosure in tinkercad

![41](../img/fp/41.jpg)


I export it to cura to set the material setting and the machine setting of the printer

![42](../img/fp/42.jpg)


After setting it I printed my enclosure in the anycubic 3d printer

![43](../img/fp/43.jpg)


The result came out really good

![44](../img/fp/44.jpg)

![45](../img/fp/45.jpg)

My enclosure was a little small so I had to print it again. After printing I set everything together to test my project 

![47](../img/fp/47.jpg)

![48](../img/fp/48.jpg)

The link to my demo:

[Safetywish demo video](https://www.youtube.com/watch?v=bSfiJb4YuwA) 


### Photoslide

![46](../img/fp/46.jpg)



### References

- [NTP server](https://circuitdigest.com/microcontroller-projects/internet-clock-display-date-and-time-using-esp8266-with-ntp) 

- [GPS interfacing](https://iotdesignpro.com/projects/nodemcu-esp8266-gps-module-interfacing-to-display-latitude-and-longitude) 

- [GPS on Oled](https://iotdesignpro.com/projects/nodemcu-esp8266-gps-module-interfacing-to-display-latitude-and-longitude)
