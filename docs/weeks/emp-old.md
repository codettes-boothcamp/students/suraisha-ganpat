
Install Arduino and start with the basic Arduino KIt

**Upload Blink Sketch**
Open-> File-> Examples-> Basics-> Blink

![1](../img/ep/1.jpg)

![2](../img/ep/2.jpg)

Connect to the board:
Tool-> port-> (COM 16 Arduino/Genuino Uno)

![3](../img/ep/3.jpg)

Choose a serial port your Arduino is selected to:
Tool-> Board "Arduino/Genuino Uno"-> Arduino/Genuino Uno

![4](../img/ep/4.jpg)

Add a code for the blink
Compile the code

![5](../img/ep/5.jpg)

Upload the blink sketch to your Arduino

![6](../img/ep/6jpg)

The code for the blink

![8](../img/ep/8.jpg)

If succesfully uploaded, the yellow light on the Arduino start to blink

![7](../img/ep/7.jpg)

**Blink built in led 13**
  
First code it and then opload it to let it Blink. You can change the delay time so the led will blink faster.

![9](../img/ep/9.jpg)

The blink

![10](../img/ep/10.jpg)

**Build circuit**

- Wire up the breadboard to Arduino's 3V and ground connection. Use red and black jumper wires.
- Place a led on the bradboard
- Attach the cathode (short leg) of the led to ground through 220 ohm resistor
- Add a black wire that connect the led with the Arduino
No code-> led on

![11](../img/ep/11.jpg)

**Basic Circuit: Blink Sample**

- Wire up the board to Arduino's 5V and ground connection.
- Place 3 leds on the breadboard.
- Attach the cathode (short leg) of each leds to ground using a 220 ohm resistor
- Connect the anode (long leg) to pin 11, 12 and 13.

Add the code for this Blink

![12](../img/ep/12.jpg)

When the code is succesfully uploaded, the leds start blinking

![13](../img/ep/13.jpg)

**Spaceship Interfase**

- Wire up the breadboard to Arduino's 5V and ground connection.
- Place 3 leds on the breadboard.
- Attach the cathode (short leg) of each leds to ground using a 220 ohm resistor
- Connect the anode (long leg) to pin 3,4 and 5.

Switch on the breadboard
- Place the switch on the breadboard
- Attach one side to power and the other side to digital pin 2 on the Arduino
- Add a 10k-ohm resistor from the ground to the switch pin that connects to the Arduino
- This pull-down resistor connects the pin to the ground when the switch is open, so it reads low when there is no voltage coming throught the switch.

It should be look like this

![14](../img/ep/14.jpg)

Add the code: In the void set up, change the pinmode to 3,4,5 output and
pinmode 2 to input. This because when press on the switch button that is connect through pin 2, the lights are blinking one after one.
High and low is the voltage level. The delay time means that the program has to wait moving to the next line of code. You can also see it when the leds is blinking.

![15](../img/ep/15.jpg)

After the code is succesfully uploaded to the Arduino, the leds should blink one after one.

![16](../img/ep/16.jpg)

**Procedural Programming**

- Use another part of the breadboard
- Add the photoresister on the breadboard 
- Add a 220 ohm resistor from the ground to connect it to the Arduino 
- Add a Wire from the A0 to the Photoresistor

![17](../img/ep/17.jpg)

Add the code: In the void runningleds you put the ledsoff for pin 3,4 and 5. So after pushing the button it will run and the leds will off after 250 milliseconds. And so it will go on.

![18](../img/ep/17.jpg)

After adding the code, the photo resistor is blinking when you put a light on it.

**Files**
- Arduino Slides for week 3
- Arduino 1-8-9 Windows.exe

**Used**
- Arduino starter kit to build a circuit

I learned how to build a circuit using the Arduino starter kit and to code and upload the coding to the Arduino so the leds would blinking