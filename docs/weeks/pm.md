## Project Managment

**First week assignment**

**Introduction**

This week is all about learning about project management and build a personal site using Gitlab and Github.

**The assesment that should be done the first week are**:

[x] Writing down an about of ypurself

[x] Do research on your final project

[x] Basic documentation online

[x] Document your steps with screenshots

On the first day Mister Theo started with a presentation about what Iot is and some example of the IoT. Then he discuss the four common elements of IoT which are network,data,device and Sensor. Then he explain the Quadrant. After that he talked about the architecture and showed us a greentubes design. At the end he talked about some digital manufacturing and where the money goes.

**Build a personal site**

First we got the files from Julie to install Github desktop and Gitlab.Then open the file codettes bootcamp. Open Git and install it. Open gitlab online and make an account. When the account is finally made. You have to open it and Clone Gitlab with the link Julie gave us.


![gitlabclone](../img/pm/gitlabclone.jpg)


Then install Github desktop -> Click on file-> Clone respository-> Open


![githubfile](../img/pm/githubfile.jpg)



![gtclone](../img/pm/gtclone.jpg)



Copy paste the URL -> choose the local path -> Clone


![clone](../img/pm/clone.jpg)


Now with the cloning you can work online and local



Now you can work on notepad++
Open the github folder-> docs-> index.md


![notepad](../img/pm/notepad.jpg)

Write about yourself and your final project-> Save it-> Go to github desktop-> add description-> Commit to master-> Click on Push


![save](../img/pm/save.jpg)


![push](../img/pm/push.jpg)


Now you can see the changes you made to Gitlab
Open Gitlab-> Go to setting-> Open pages-> Click on the link-> there you see the changes you made.


![pages](../img/pm/pages.jpg)


![changes](../img/pm/changes.jpg)


**Files**

- Github

- Codettes bootcamp











