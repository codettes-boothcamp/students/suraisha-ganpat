## Week 2 Video Production

** Video Editing**

- Install Adobe premier

New project=> File-> New-> Project

![new](../img/vp/new.jpg)


Add a title=> Brows location-> capture format: HDV-> Other: Deafault

![title](../img/vp/title.jpg)

-Import a video
File-> Import-> Select video

![import](../img/vp/import.jpg)

-Wokspace
Drag video to timeline at the right

![timeline](../img/vp/timeline.jpg)

Select effects-> Search Ultra Key-> Drag to right to the timeline

![ultrakey](../imgvp/ultrakey.jpg)

-Effect control
-Change key color and add background

![color](../img/vp/color.jpg)

-Import a background
-Drag to right between video and audio
-if to small make it bigger with scales

![background](../img/vp/background.jpg)

