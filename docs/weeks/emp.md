## Week 3,4,5 Embedded Programming

Install Arduino and start with the basic Arduino KIt

**The Arduino Uno starter kit**

The Arduino Uno is a simple computer that can be used to built circuits and interface for interaction and telling the microcontroller to interface with other component.

Some of the components are:

- A breadboard: A board that can be used to built circuits.

- Resistor: Resist the flow of electrical energy in a circuit, changing the voltage level and current as a result

- Jumper Wires: Use this to connect components to each other on the breadboard

![19](../img/ep/19.jpg)


### Day 1 Upload Blink Sketch

Open-> File-> Examples-> Basics-> Blink

![1](../img/ep/1.jpg)

![2](../img/ep/2.jpg)

Connect to the board:
Tool-> port-> (COM 16 Arduino/Genuino Uno)

![3](../img/ep/3.jpg)

Choose a serial port your Arduino is selected to:

Tool-> Board "Arduino/Genuino Uno"-> Arduino/Genuino Uno

![4](../img/ep/4.jpg)

Add a code for the blink

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for a second
}
```

Compile the code

![5](../img/ep/5.jpg)

Upload the blink sketch to your Arduino

![6](../img/ep/6.jpg)

If succesfully uploaded, the yellow light on the Arduino start to blink

![7](../img/ep/7.jpg)

**Blink built in led 13**
  
First code it and then opload it to let it Blink. You can change the delay time so the led will blink faster.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for a second

   digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for a second
}
```

The blink

![10](../img/ep/10.jpg)

**Build circuit**

- Wire up the breadboard to Arduino's 3V and ground connection. Use red and black jumper wires.
- Place a led on the bradboard
- Attach the cathode (short leg) of the led to ground through 220 ohm resistor
- Add a black wire that connect the led with the Arduino
No code-> led on

![11](../img/ep/11.jpg)

**Basic Circuit: Blink Sample**

- Wire up the board to Arduino's 5V and ground connection.
- Place 3 leds on the breadboard.
- Attach the cathode (short leg) of each leds to ground using a 220 ohm resistor
- Connect the anode (long leg) to pin 11, 12 and 13.

Add the code for this Blink

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(11, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(11, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second

  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second

}
```

When the code is succesfully uploaded, the leds start blinking

![13](../img/ep/13.jpg)


### Day 2,3 Spaceship Interfase

- Wire up the breadboard to Arduino's 5V and ground connection.
- Place 3 leds on the breadboard.
- Attach the cathode (short leg) of each leds to ground using a 220 ohm resistor
- Connect the anode (long leg) to pin 3,4 and 5.

Switch on the breadboard
- Place the switch on the breadboard
- Attach one side to power and the other side to digital pin 2 on the Arduino
- Add a 10k-ohm resistor from the ground to the switch pin that connects to the Arduino
- This pull-down resistor connects the pin to the ground when the switch is open, so it reads low when there is no voltage coming throught the switch.

It should be look like this

![14](../img/ep/14.jpg)

Add the code: In the void set up, change the pinmode to 3,4,5 output and
pinmode 2 to input. This because when press on the switch button that is connect through pin 2, the lights are blinking one after one.
High and low is the voltage level. The delay time means that the program has to wait moving to the next line of code. You can also see it when the leds is blinking.


```
// the setup function runs once when you press reset or power the board
int switchState = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output-/inMode(12, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);  
}

void ledsOff(){
  digitalWrite(3,LOW); 
  digitalWrite(4,LOW);
  digitalWrite(5,LOW);
}

// the loop function runs over and over again forever
void loop() {
  switchState = digitalRead(2);
  // this is a comment
  if (switchState==LOW){
    digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(4, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(5, HIGH);   // turn the LED on (HIGH is the voltage level)
  }
  else{// wait for a second
    ledsOff();
    digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
    delay(250); 
    ledsOff();
    digitalWrite(4, HIGH);
 
    delay(250);
    ledsOff();
    digitalWrite(5, HIGH);
   
   delay(250);
   ledsOff();
   digitalWrite(3, HIGH);
   delay(250);
  }
}
```
  

After the code is succesfully uploaded to the Arduino, the leds should blink one after one.

![16](../img/ep/16.jpg)


### Day 4 Procedural Programming

- Use another part of the breadboard
- Add the photoresister on the breadboard 
- Add a 100 kohm resistor from the ground to connect it to the Arduino 
- Add a Wire from the A0 to the Photoresistor

![17](../img/ep/17.jpg)

Add the code: In the void runningleds you put the ledsoff for pin 3,4 and 5. So after pushing the button it will run and the leds will off after 250 milliseconds. And so it will go on.


```
// the setup function runs once when you press reset or power the board
int switchState = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output-/inMode(12, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);  
}

void ledsOff(){
  digitalWrite(3,LOW); 
  digitalWrite(4,LOW);
  digitalWrite(5,LOW);
}

void runningleds(){
    ledsOff();
    digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
    delay(250); 
    ledsOff();
    digitalWrite(4, HIGH);
 
    delay(250);
    ledsOff();
    digitalWrite(5, HIGH);
   
   delay(250);
   ledsOff();
   digitalWrite(3, HIGH);
   delay(250);
  }
```

After adding the code, the photo resistor is blinking when you put a light on it.


### Day 5 Serial Communication

**Serial Monitor**
 
The serial communicate with a speed of 9600 bits per second.The serial.printIn is what you see on the serial monitor.

The code 

```
void setup() {
  Serial.begin(9600);
  //Serial.println('hello world');
  
}
  // put your setup code here, to run once:



void loop() {
  Serial.println("hello codettes");
  delay(2000);
  
  // put your main code here, to run repeatedly:

}
```
Open serial post connection and send information
Tool-> Serial Monitor

![20](../img/ep/20.jpg)

The printIn in that can be seen on the Serial Monitor

![21](../img/ep/21.jpg)

**Ldr**

An LDR is a component that has a (variable) resistance that changes with the light intensity that falls upon it. This allows them to be used in light sensing circuits
In the serial monitor we see the minimum and maximum light value that must be declare, as well the lightPc, analogpin and the switchstate, because there is a switch added on the breadboard
Pin 3,4 and 5 are used as Output en pin 2 as Input.
With light intensity the leds are turn one after one. if you put the light resistor in completely darkness, all the leds are turn off. Playing with you hands, you can see that
when the resistor get 30%,60% and 90% light, the leds are turn on.
If the light intensity is greater than 99%, you see running leds.

The code 

```
int analogPin = A0;
int lightState = 0;
int minimumLight = 184;
int maximumLight = 1022;
int lightPc = 0;
int switchState = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print("hello");
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
}

 void ledsOff(){
  digitalWrite(3, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW

  }

  void runningLeds(){
  ledsOff();
  digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay (250);
  ledsOff();
  digitalWrite(4, HIGH);    // turn the LED off by making the voltage LOW
  delay (250);
  ledsOff();
  digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
  delay (250); 

  }

// the loop routine runs over and over again forever:

void loop() {
  // read the input on analog pin 0:
  lightState = analogRead(A0);
  Serial.println(lightState);
  lightPc = (lightState - minimumLight)*100L/(maximumLight-minimumLight);
  Serial.println(lightPc);
  
  ledsOff();
  //if light greater and equal to 30% led 3 is on
  //if light greater and equal to 60% led 4 is on
  //if light greater and equal to 90% led 5 is on
  if (lightPc>=30){digitalWrite(3,1);}
  if (lightPc>=60){digitalWrite(4,1);}
  if (lightPc>=90){digitalWrite(5,1);}
  if (lightPc>=99){runningLeds();}
 
  delay (1000);


    }
```
Without putting a hand on the resistor. All the leds are turn on.

![22](../img/ep/22.jpg)

Playing with light intensity, the leds are turning on one after one.

![23](../img/ep/23.jpg)

![24](../img/ep/24.jpg)

![25](../img/ep/25.jpg)

When the resistor don't get light, the leds are turn off.

![26](../img/ep/26.jpg)

To read value from LDR
Open Serial Monitor

**PWM**

PWM means Pulse Width Modilator. With this, the analogue is signalize to digital through a pulse. For this the LDR code is used. Pinmode 3 and 5 are used because in these pins the
PWM is connected on the Arduino. Place a potentiometer on the breadboard, and connect one side to 5V and the other to ground. A potentiometer is a type of voltage divider. Turning the
knob will change the ratio of the voltage between the pin and power. 

The potentiometer of 10K on the breadboard:

![27](../img/ep/27.jpg)

The code:

```
int analogPin = A0;
int lightState = 0;
int minimumLight = 184;
int maximumLight = 1022;
int lightPc = 0;
int switchState = 0;
int potValue=0;
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print("hello");
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
}

 void ledsOff(){
  digitalWrite(3, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW

  }

void runningLeds(){
  ledsOff();
  digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay (250);
  ledsOff();
  digitalWrite(4, HIGH);    // turn the LED off by making the voltage LOW
  delay (250);
  ledsOff();
  digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
  delay (250); 

  }

// the loop routine runs over and over again forever:

void dimLed(int ledPin, int dutyCycle){
  int cycleTime = 10 ;//in milliseconds
  digitalWrite(ledPin, 1);
  delay((dutyCycle*cycleTime)/100);
  digitalWrite(ledPin,0);
  delay((100-dutyCycle)* cycleTime/100);
}

void loop() {
  potValue= analogRead(A0);
  //dimLed(4,50);
  analogWrite(3,potValue/4);
  analogWrite(5,potValue/4);
//  for(int i=0;i<=255;i++){
//    //analogWrite(5,i);
//    delay(10);
//  }
  Serial.println(potValue);
  delay(50);
}

```
Turning the potentiometer, the led is on, then dimmed.

![28](../img/ep/28.jpg)



### Day 6 DHT11(Digital Humidity Temperature)

With the DHT11 sensor you can add humidity and temperature data to your DIY electronics projects. The DHT11 measures temperature and humidity that can be
seen on the Serial Monitor. 

Search for an example of how to build the circuit on an Arduino:

- Set up a DHT 11 sensor on the breadboard with 3 pins. 
- Use 3 wires to connect it to the Arduino. The DHT11 senor use just one signal wire to transmit data to the Arduino. The three pin
  DHT11 sensor already have a 10 kohm pull up resistor.
  
![29](../img/ep/29.jpg)

Before the DHT11 can be use on the Arduino, the DHT sensor Library must be installed. It has all the functions needed to get the humidity and temperature readings from the sensor. 
Download DHT sensor Library
Then go to Sketch-> Include Library-> Add ZIP Library and select the DHT sensor Library.zip file.

![30](../img/ep/30.jpg)

Add an example code from the DHT sensor Library

```
// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include "DHT.h"

#define DHTPIN 4    // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));
  Serial.print(hif);
  Serial.println(F("°F"));
}
```
Connect the Ardiuno to your laptop, then upload the code 
To see the humidity and temperature readings go to: tool-> Open Serial Monitor

![31](../img/ep/31.jpg)



### Day 7 Tinkercad

Tinkercad is a free, online 3D modeling program that runs in a web browser, known for its simple interface and ease of use

Open www.tinkercad.com on a web browser and make an account with your email adres

![32](../img/ep/32.jpg)

After makinig an account. Go to circuits and create a new circuit

![33](../img/ep/33.jpg)

![34](../img/ep/34.jpg)

Make a circuit and add a code to let the led blink:

- The code

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```
- The blink

![35](../img/ep/35.jpg)

**Running leds with transistor**

Build the circuit and add a code. Pin 3,4 and 5 are used. 200ohm resistor is used for the pins and and a 60kohm for the photoresistor. 
By playing with the light intensity of the photoresistor. The leds are turn on and off or keep running. It is the same as how the LDR works but this is
in tinkercad. 

Use the code for LDR

```
int analogPin = A0;
int lightState = 0;
int minimumLight = 184;
int maximumLight = 1022;
int lightPc = 0;
int switchState = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print("hello");
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
}

 void ledsOff(){
  digitalWrite(3, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW

  }

  void runningLeds(){
  ledsOff();
  digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay (250);
  ledsOff();
  digitalWrite(4, HIGH);    // turn the LED off by making the voltage LOW
  delay (250);
  ledsOff();
  digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
  delay (250); 

  }

// the loop routine runs over and over again forever:

void loop() {
  // read the input on analog pin 0:
  lightState = analogRead(A0);
  Serial.println(lightState);
  lightPc = (lightState - minimumLight)*100L/(maximumLight-minimumLight);
  Serial.println(lightPc);
  
  ledsOff();
  //if light greater and equal to 30% led 3 is on
  //if light greater and equal to 60% led 4 is on
  //if light greater and equal to 90% led 5 is on
  if (lightPc>=30){digitalWrite(3,1);}
  if (lightPc>=60){digitalWrite(4,1);}
  if (lightPc>=90){digitalWrite(5,1);}
  if (lightPc>=99){runningLeds();}
 
  delay (1000);

}

```

The blinking

![36](../img/ep/36.jpg)


** DC Motor control **

The DC Motor or Direct Current Motor is the most commonly used actuator for producing continuous movement and 
whose speed of rotation can easily be controlled, making them ideal for use in applications were speed control, servo type control, and/or positioning 
is required. A DC motor consists of two parts, a “Stator” which is the stationary part and a “Rotor” which is the rotating part.

The DC motor just have 2 leads, if the te loads are connected to a power supply, the DC motor will start to rotate.On the power supply you can see the voltage
of the DC motor and 79.9 mA cuurent. The RPM (Revolutions per minute) is 9994. 
This is the amount of times the shaft of a DC motor completes a full spin cycle per minute. A full spin cycle is when the shaft turns a full 360°. 
The amount of 360° turns, or revolutions, a motor does in a minute is the RPM value

![37](../img/ep/37.jpg)

If you switch the leads the motor will rotate in the opposite direction and the RPM -9994.

![38](../img/ep/38.jpg)


** Dc motor control(H-Bridge) **

To control the direction of the spin of DC motor, without changing the way that the leads are connected, you can use a circuit called an H-Bridge.
An H-bridge is an electronic circuit that can drive the motor in both directions.

Build a circuit using a L293D H-Bridge motor Driver. Connect the DC motor to Output 1 and Output 2 on the H-Bridge. 
Connect the Arduino to input 1(pin 8) and input 2(pin 9). Enable 1 to pin 3(Pin 3 needs a PWM signal to control the DC motor).


- Speed Control

Add the code:

```
const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
}

void loop() {
   //For Clock wise motion , in_1 = High , in_2 = Low
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,LOW) ;
   analogWrite(pwm,255) ;
   /* setting pwm of the motor to 255 we can change the speed of rotation
   by changing pwm input but we are only using arduino so we are using highest
   value to driver the motor */
   //Clockwise for 3 secs
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH
   digitalWrite(in_1,LOW) ;
   digitalWrite(in_2,HIGH) ;
   delay(3000) ;
  //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
}

```

On the voltagemeter you can see that the motor is rotating in 2 opposite direction.

![39](../img/ep/39.jpg)

![40](../img/ep/40.jpg)


- Jog button direction

Wiring speed & direction Control. Add a potentiometer and use A0 as the speedPin

![41](../img/ep/41.jpg)

Adding the speed control in the code:

```
const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
// Speed control
Const int speedPin = A0;
Int speed =0;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
}
void loop() {
   // Detect speedPin value
   speed=analogRead(speedPin)/4;
   //For Clock wise motion , in_1 = High , in_2 = Low
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,LOW) ;
   analogWrite(pwm,255) ;
   /* setting pwm of the motor to 255 we can change the speed of rotation
   by changing pwm input but we are only using arduino so we are using highest
   value to driver the motor */
   //Clockwise for 3 secs
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH
   digitalWrite(in_1,LOW) ;
   digitalWrite(in_2,HIGH) ;
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
}

```

On the voltagemeter you can see that the motor is rotating in 2 opposite direction:

![42](../img/ep/42.jpg)

![43](../img/ep/43.jpg)


Wiring with speed control

![44](../img/ep/44.jpg)

Adding the code:

```
// Declare ur variables
const int pwm = 3;
const int in_1 = 8;
const int in_2 = 9;


void setup(){
	pinMode(in_1, OUTPUT);	
  	pinMode(in_2, OUTPUT);
  	pinMode(3, OUTPUT);
  	Serial.begin(9600);
}

void loop(){
  	int duty = (analogRead(A0)-512)/2;
  	Serial.println(duty);
  	analogWrite(pwm,abs(duty));
  	if(duty>0){
        // turn CW
      digitalWrite(in_1,LOW);
      digitalWrite(in_2,HIGH); 
    }
  	if(duty<0){
        // turn CCW
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,LOW); 
    }
  	if(duty==0){
        // BRAKE
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,HIGH); 
    }  
}

```

Playing with the potentiometer, the motor is rotating both ways:

![45](../img/ep/45.jpg)


- Mapping function

The code for the mapping function

```
// Declare ur variables
const int pwm = 3;
Const int jogPin =A0; // connects the pot-meter
const int in_1 = 8;
const int in_2 = 9;
Const int deadZone-5; 	//after starts repond
Int duty=0; 		// duty cycle for PWM


void setup(){
	pinMode(in_1, OUTPUT);	
  	pinMode(in_2, OUTPUT);
  	pinMode(3, OUTPUT);
  	Serial.begin(9600);
}

void loop(){
 duty = map(analogRead(jogPin),0,1023,-255,255);
  	Serial.println(duty);
  	analogWrite(pwm,abs(duty)); // abs duty to PWM
  	if(duty> 0 - deadZone){
        // turn CW
      digitalWrite(in_1,LOW);
      digitalWrite(in_2,HIGH); 
    }
  	if(duty < 0 - deadZone){
        // turn CCW
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,LOW); 
    }
  	if(abs(duty) <= deadZone){
        // BRAKE
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,HIGH); 
    }  
}

```

** Motor control Servo map **

A Servo Motor is a small device that has an output shaft. This shaft can be positioned to specific angular positions by sending the servo a coded signal. 
As long as the coded signal exists on the input line, the servo will maintain the angular position of the shaft. 
If the coded signal changes, the angular position of the shaft changes.

Build the circuit using a servo motor:

- Attach 5V and ground to one side of the breadboard from the Arduino
- Place a potentiometer on the breadboard. A potentiometer is a type of voltage divider. Turning the knob, change the ratio of the voltage between the middle pin and power.
  Connect one side to the ground and the other to 5V. Connect the middle pin to Analog pin A0. This will control the position of the servo meter. 
- The Servo has three pins. One is the power, Second is ground and the third is the control line that will receive information from the Arduino.
  Connect the control line to pin 9 using a wire.

When a Servo motor starts to move, it draws more current than if it were already in motion. This will cause a dip in the voltage on the breadboard.
By placing a 100uF capaciter across power and ground, it smoothes any voltage that occur.

![46](../img/ep/46.jpg)

The code for the Servo motor:

```
#include <Servo.h>;
Servo myServo;
int const potPin=A0;
int potVal;
int angle;

void setup(){
  myServo.attach(9);
  Serial.begin(9600);
}

void loop(){
  potVal=analogRead(potPin);
  Serial.print("potVal: ");
  Serial.print(potVal);
  angle=map(potVal, 0, 1023, 0, 179);
  Serial.print(", angle: ");
  Serial.println(angle);
  myServo.write(angle);
  //analogWrite(9,map(potVal, 0, 1023, 0, 255));
  delay(15);
}

```

The Servo meter is rotating:

![47](../img/ep/47.jpg)


**Stepper Motor**

Stepper motors are motors that have multiple coils in them, so that they can be moved in small increments or steps. 
Stepper motors are typically either unipolar or bipolar, meaning that they have either one main power connection or two. 
Whether a stepper is unipolar or bipolar,it can be controlled with a H-bridge.

There is no stepper motor on tinkercad, so we are using 3 led light to see the blinking steps. 

Build the circuit:

- Connect power and ground on the breadboard to power and ground from the microcontroller. Use 5V and a ground on th Arduino.
- Place a L293D H-bridge on the breadboard. Stepper motors has 2 coils, so it is like driving to motors with the H-bridge.
- Connect pin 8 to input 1
- Connect pin 9 to input 2
- Connect pin 10 to input 4
- Connect pin 11 to input 3
- connect 4 leds to output 1,2,3,4
- Use 220 ohm resistor for all the leds
- Use a potentiometer and connect the wiper pin to A0

The circuit:

![48](../img/ep/48.jpg)

The code:

```
#include <Stepper.h>
 
const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
                                     // for your motor
 
// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8,9,10,11);

int stepcount=0; //the number of steps the motor has taken
 
void setup() {
  // initialize the serial port:
  Serial.begin(9600);
}
 
void loop() {
  Serial.print("steps:");
  Serial.printIn(stepCount);
  
  int stepDel= map(analogread(A0),1023,-500,500);
  if(stepDel >5){
  myStepper.step(1);
  stepCount++; //stepCount=stepCount+1
  }
  
  if(stepDel >-5){
  myStepper.step(-1)
  stepCount++; //stepCount=stepCount-1
  
```

The leds are blinking step by step:

![49](../img/ep/49.jpg)



**Files**

- Arduino Slides for week 3,4,5

- Arduino 1-8-9 Windows.exe

**Used**

- Arduino starter kit to build a circuit

I have learned how to build a circuit using the Arduino starter kit and to code and upload the coding to the Arduino so the leds would blinking

- Tinkercad