## Hack O'mation 2020 Premeet 6


**IoT Stack- ESP32**

ESP32 is a series of low-cost, low-power system on a chip microcontrollers with integrated Wi-Fi and dual-mode Bluetooth. 
The ESP32 series employs a Tensilica Xtensa LX6 microprocessor in both dual-core and single-core variations and includes built-in antenna switches, 
RF balun, power amplifier, low-noise receive amplifier, filters, and power-management modules

*The features of the ESP32:*

Processors:
- CPU: Xtensa dual-core (or single-core) 32-bit LX6 microprocessor, operating at 160 or 240 MHz and performing at up to 600 DMIPS
- Ultra low power (ULP) co-processor

Memory: 520 KiB SRAM
Wireless connectivity:
- Wi-Fi: 802.11 b/g/n
- Bluetooth: v4.2 BR/EDR and BLE (shares the radio with Wi-Fi)

Peripheral interfaces:
- 12-bit SAR ADC up to 18 channels
- 2 × 8-bit DACs
- 10 × touch sensors (capacitive sensing GPIOs)
- 4 × SPI
- 2 × I²S interfaces
- 2 × I²C interfaces
- 3 × UART
- SD/SDIO/CE-ATA/MMC/eMMC host controller
- SDIO/SPI slave controller
- Ethernet MAC interface with dedicated DMA and IEEE 1588 Precision Time Protocol support
- CAN bus 2.0
- Infrared remote controller (TX/RX, up to 8 channels)
- Motor PWM
- LED PWM (up to 16 channels)
- Hall effect sensor
- Ultra low power analog pre-amplifier

Security:
- IEEE 802.11 standard security features all supported, including WFA, WPA/WPA2 and WAPI
- Secure boot
- Flash encryption
- 1024-bit OTP, up to 768-bit for customers
- Cryptographic hardware acceleration: AES, SHA-2, RSA, elliptic curve cryptography (ECC), random number generator (RNG)

Power management:
- Internal low-dropout regulator
- Individual power domain for RTC
- 5μA deep sleep current
- Wake up from GPIO interrupt, timer, ADC measurements, capacitive touch sensor interrupt



**The ESP32 (MCU)**

![1](./img/hack2020/1.jpg)



### Setup ESP32 in Arduino IDE

For the set up I need to install the ESP32 in my Arduino IDE.

Open Arduino IDE-> Then go to file-> And then preferences

![3](./img/hack2020/3.jpg)



Enter https://dl.espressif.com/dl/package_esp32_index.json into the “Additional Board Manager URLs” field as shown in the figure below. Then, click the “OK” button

![2](./img/hack2020/2.jpg)



After that open the Board Manager. for that go to tools-> Board -> Click on  Boards Managers

![4](./img/hack2020/4.jpg)



The Board Managers screen will open and then search for the ESP32 library to install it

![5](./img/hack2020/5.jpg)

After a few secondes the Library will be installed.




### ESP32 Basic Over The Air (OTA) Programming In Arduino IDE

A fantastic feature of any WiFi-enabled microcontroller like ESP32 is the ability to update its firmware wirelessly. This is known as Over-The-Air (OTA) programming.


**What is OTA programming in ESP32?**

The OTA programming allows updating/uploading a new program to ESP32 using Wi-Fi instead of requiring the user to connect the ESP32 to a computer via USB to perform the update.
OTA functionality is extremely useful in case of no physical access to the ESP module. It helps reduce the amount of time spent for updating each ESP module at the time of maintenance.

One important feature of OTA is that one central location can send an update to multiple ESPs sharing same network.
The only disadvantage is that you have to add an extra code for OTA with every sketch you upload, so that you’re able to use OTA in the next update.


**Ways To Implement OTA In ESP32**

There are two ways to implement OTA functionality in ESP32.
- Basic OTA – Over-the-air updates are sent through Arduino IDE.
- Web Updater OTA – Over-the-air updates are sent through a web browser.

Each one has its own advantages. You can implement any one according to your project’s requirement.


**3 Simple Steps To Use Basic OTA with ESP32**

- Install Python 2.7.x seriesThe first step is to install Python 2.7.x series in your computer.
- Upload Basic OTA Firmware SeriallyUpload the sketch containing OTA firmware serially. It’s a mandatory step, so that you’re able to do the next updates/uploads over-the-air.
- Upload New Sketch Over-The-AirNow, you can upload new sketches to the ESP32 from Arduino IDE over-the-air.


**Step 1: Install Python 2.7.x series**

In order to use OTA functionality, you need to install the Python 2.7.x version, if not already installed on your machine.

Go to [Python's official website](https://www.python.org/downloads/) and download 2.7.x (specific release) for Windows (MSI installer)
Open the installer and follow the installation wizard.
In Customize Python 2.7.X section, make sure the last option Add python.exe to Path is enabled.

Make sure your ESP32 is connected with your computer.


**Step 2: Upload OTA Routine Serially**

The factory image in ESP32 doesn’t have an OTA Upgrade capability. So, you need to load the OTA firmware on the ESP32 through serial interface first.
It’s a mandatory step to initially update the firmware, so that you’re able to do the next updates/uploads over-the-air.

The ESP32 add-on for the Arduino IDE comes with a OTA library & BasicOTA example. 
For the basicOTA example gor File
You can access it through File -> Examples -> ArduinoOTA -> BasicOTA.

![6](./img/hack2020/6.jpg)



The following code should load. But, before you head for uploading the sketch, you need to make some changes to make it work for you. 
You need to modify the following two variables with your network credentials, so that ESP32 can establish a connection with existing network.

```
const char* ssid = "telew_f28";
const char* password = "(your code)";
```

The BasicOTA code:

```
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

const char* ssid = "..........";
const char* password = "..........";

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  ArduinoOTA.handle();
}
```



After that I uploaded my sketch to the ESP32> Before uploading make sure you select you port. For that go to tools and select your port

While uploading I got an error:

![8](./img/hack2020/8.jpg)

I research my error and it means that it failed to connect to the board so I had to press my boot/flash bottton on my ESP32 while uploading.

If everything is done properly, open the Serial Monitor at a baud rate of 115200. And press the EN button on ESP32. 
If everything is OK, it will output the dynamic IP address obtained from your router. Note it down

![7](./img/hack2020/7.jpg)



**Step 3: Upload New Sketch Over-The-Air**

Now, let’s upload a new sketch over-the-air.

Remember! you need to add the code for OTA in every sketch you upload. Otherwise, you’ll loose OTA capability and will not be able to do next uploads over-the-air. 
So, it’s recommended to modify the above code to include your new code.

As an example we will include a simple Blink sketch in the Basic OTA code. Remember to modify the SSID and password variables with your network credentials.


I used the same example for the basicOTA. I only add some code for the blinking:

```
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

const char* ssid = "CoronaVirus20";
const char* password = "(your code)";

//variabls for blinking an LED with Millis
const int led = 16; // ESP32 Pin to which onboard LED is connected
unsigned long previousMillis = 0;  // will store last time LED was updated
const long interval = 1000;  // interval at which to blink (milliseconds)
int ledState = LOW;  // ledState used to set the LED

void setup() {

  pinMode(led, OUTPUT);
  
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  ArduinoOTA.handle();
    
//loop to blink without delay
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
  // save the last time you blinked the LED
  previousMillis = currentMillis;
  // if the LED is off turn it on and vice-versa:
  ledState = not(ledState);
  // set the LED with the ledState of the variable:
  digitalWrite(led,  ledState);
  }
}

```


Now Select your board and your port before uploading:

![9](./img/hack2020/9.jpg)

![10](./img/hack2020/10.jpg)

Now the code can be uploaded. If everything is perfect. The led on the ESP32 will start blinking.
And there it is:

The blinking led:

![11](./img/hack2020/11.jpg)




### Create a webserver with websockets using an ESP32  in Arduino

Rather than just host a simple web page, we’re going to build on the WebSocket idea. Let’s have our ESP32 be an access point (AP) and host a web page. 
When a browser requests that page, the ESP32 will serve it. As soon as the page loads, the client will immediately make a WebSocket connection back to the ESP32. 
That allows us to have fast control of our hardware connected to the ESP32.

The WebSocket connection is two-way. Whenever the page loads, it first inquires about the state of the LED from the ESP32. If the LED is on, the page will update a circle (fill in red) to reflect that. The circle on the page will be black if the LED is off. 
Then, whenever a user presses the “Toggle LED” button, the client will send a WebSocket packet to the ESP32, telling it to toggle the LED. 
This packet is followed by another request asking about the LED state so that the client can keep the browser updated with the state of the LED.

I am going to use the led that is on the Arduino. So I don't have to connect a led to my ESP32.


**Install SPIFFS Plugin**

The Serial Peripheral Interface Flash File System, or SPIFFS for short. It's a light-weight file system for microcontrollers with an SPI flash chip. 
SPIFFS let's you access the flash memory as if it was a normal file system like the one on your computer (but much simpler of course): you can read and write files, create folders ...

We need to use a special program to upload files over SPI.
Go to [Arduino ESP32fs plugin](https://github.com/me-no-dev/arduino-esp32fs-plugin) and follow the instructions to install the Arduino plugin.


The instructions: **Arduino ESP32 filesystem uploader**

Arduino plugin which packs sketch data folder into SPIFFS filesystem image, and uploads the image to ESP32 flash memory.

**Installation**

- Make sure you use one of the supported versions of Arduino IDE and have ESP32 core installed.
- Download the tool archive from [Release page](https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/tag/1.0)

  ![12](./img/hack2020/12.jpg)
  
- In your Arduino sketchbook directory, create tools directory if it doesn't exist yet.
  In my Arduino directory, the tool directory was already created
- Unpack the tool into tools directory (by me the path look like <This Pc>/Documents/Arduino/tools/ESP32FS/tool/esp32fs.jar).
  
   ![13](./img/hack2020/13.jpg)

- Restart Arduino IDE

  To check if the plugin was successfully installed, open your Arduino IDE. Select your ESP32 board, go to Tools and check that you have the option “ESP32 Sketch Data Upload“.
  
  ![18](./img/hack2020/18.jpg)
  


**Usage**

- Open a sketch (or create a new one and save it).For this I opened a new sketch in Arduino IDE
  For this click on File-> Then New
  
   ![14](./img/hack2020/14.jpg)

   Delete the code from the new sketch and put the code for the webserver.
   
   *Arduino Webserver code*
   
   
   ```
    #include <WiFi.h>
    #include <SPIFFS.h>
    #include <ESPAsyncWebServer.h>
    #include <WebSocketsServer.h>
     
    // Constants
    const char *ssid = "ESP32-AP";
    const char *password = "letmeinplz";
    const char *msg_toggle_led = "toggleLED";
    const char *msg_get_led = "getLEDState";
    const int dns_port = 53;
    const int http_port = 80;
    const int ws_port = 1337;
    const int led_pin = 16;
     
    // Globals
    AsyncWebServer server(80);
    WebSocketsServer webSocket = WebSocketsServer(1337);
    char msg_buf[10];
    int led_state = 0;
     
    /***********************************************************
     * Functions
     */
     
    // Callback: receiving any WebSocket message
    void onWebSocketEvent(uint8_t client_num,
                          WStype_t type,
                          uint8_t * payload,
                          size_t length) {
     
      // Figure out the type of WebSocket event
      switch(type) {
     
        // Client has disconnected
        case WStype_DISCONNECTED:
          Serial.printf("[%u] Disconnected!\n", client_num);
          break;
     
        // New client has connected
        case WStype_CONNECTED:
          {
            IPAddress ip = webSocket.remoteIP(client_num);
            Serial.printf("[%u] Connection from ", client_num);
            Serial.println(ip.toString());
          }
          break;
     
        // Handle text messages from client
        case WStype_TEXT:
     
          // Print out raw message
          Serial.printf("[%u] Received text: %s\n", client_num, payload);
     
          // Toggle LED
          if ( strcmp((char *)payload, "toggleLED") == 0 ) {
            led_state = led_state ? 0 : 1;
            Serial.printf("Toggling LED to %u\n", led_state);
            digitalWrite(led_pin, led_state);
     
          // Report the state of the LED
          } else if ( strcmp((char *)payload, "getLEDState") == 0 ) {
            sprintf(msg_buf, "%d", led_state);
            Serial.printf("Sending to [%u]: %s\n", client_num, msg_buf);
            webSocket.sendTXT(client_num, msg_buf);
     
          // Message not recognized
          } else {
            Serial.println("[%u] Message not recognized");
          }
          break;
     
        // For everything else: do nothing
        case WStype_BIN:
        case WStype_ERROR:
        case WStype_FRAGMENT_TEXT_START:
        case WStype_FRAGMENT_BIN_START:
        case WStype_FRAGMENT:
        case WStype_FRAGMENT_FIN:
        default:
          break;
      }
    }
     
    // Callback: send homepage
    void onIndexRequest(AsyncWebServerRequest *request) {
      IPAddress remote_ip = request->client()->remoteIP();
      Serial.println("[" + remote_ip.toString() +
                      "] HTTP GET request of " + request->url());
      request->send(SPIFFS, "/index.html", "text/html");
    }
     
    // Callback: send style sheet
    void onCSSRequest(AsyncWebServerRequest *request) {
      IPAddress remote_ip = request->client()->remoteIP();
      Serial.println("[" + remote_ip.toString() +
                      "] HTTP GET request of " + request->url());
      request->send(SPIFFS, "/style.css", "text/css");
    }
     
    // Callback: send 404 if requested file does not exist
    void onPageNotFound(AsyncWebServerRequest *request) {
      IPAddress remote_ip = request->client()->remoteIP();
      Serial.println("[" + remote_ip.toString() +
                      "] HTTP GET request of " + request->url());
      request->send(404, "text/plain", "Not found");
    }
     
    /***********************************************************
     * Main
     */
     
    void setup() {
      // Init LED and turn off
      pinMode(led_pin, OUTPUT);
      digitalWrite(led_pin, LOW);
     
      // Start Serial port
      Serial.begin(115200);
     
      // Make sure we can read the file system
      if( !SPIFFS.begin()){
        Serial.println("Error mounting SPIFFS");
        while(1);
      }
     
      // Start access point
      WiFi.softAP(ssid, password);
     
      // Print our IP address
      Serial.println();
      Serial.println("AP running");
      Serial.print("AP IP address: ");
      Serial.println(WiFi.softAPIP());
     
      // On HTTP request for root, provide index.html file
      server.on("/", HTTP_GET, onIndexRequest);
     
      // On HTTP request for style sheet, provide style.css
      server.on("/style.css", HTTP_GET, onCSSRequest);
     
      // Handle requests for pages that do not exist
      server.onNotFound(onPageNotFound);
     
      // Start web server
      server.begin();
     
      // Start WebSocket server and assign callback
      webSocket.begin();
      webSocket.onEvent(onWebSocketEvent);
      
    }
     
    void loop() {
      
      // Look for and handle WebSocket data
      webSocket.loop();
    }
    
    ```
    
  
After putting the webserver code. Save it and give it a name "webserver"
  
![15](./img/hack2020/15.jpg)
  
- After saving it go back to Arduino IDE and then go to sketch directory (choose Sketch > Show Sketch Folder). Make sure you see where the sketch folder is.
  Mine is in This Pc-> Documents-> Arduino-> webserver folder

  ![16](./img/hack2020/16.jpg)

- In the webserver folder create another folder and name it "data". This folder is needed to put the files in it for the filesystem upload.
  
  ![17](./img/hack2020/17.jpg)



**Install Arduino Libraries**

Now I need libraries to let the webserver and websockets work.

I just donloaded these three libraries:

- [AsyncTCP](https://github.com/me-no-dev/AsyncTCP)
  
  ![19](./img/hack2020/19.jpg)


- [ESPAsyncWebServer](https://github.com/me-no-dev/ESPAsyncWebServer)

- [arduinoWebSockets](https://github.com/Links2004/arduinoWebSockets)


Now use the same Webserver code and take note of the SSID and password. We will need these to connect our phone/computer to the ESP32’s AP.

```
const char *ssid = "ESP32-AP";
const char *password = "letmeinplz";
```

You can enter your own password. "Just remember it"

After editing the code, I included the libraries that I downloaden

For this go to Sketch-> Include Library-> Add Zip library

![20](./img/hack2020/20.jpg)

Do this for all the three libraries

I then uploaded my code to my ESP32 by pressing on the boot button that is on my ESP32.

I succesfully uploaded my code.

![21](./img/hack2020/21.jpg)



Open a serial monitor with a baud rate of 115200. 

![22](./img/hack2020/22.jpg)


You should see the IP address of the ESP32 printed to the screen. 
When you run the Arduino soft access point software on the ESP32, the default IP address is 192.168.4.1.

![23](./img/hack2020/23.jpg)



**Webpage Code**

For the webpage code I created a file in the data folder that I created before in the webserver folder.
Go in the data folder and create a file. Name it index.html. In this data folder you can also add the CSS file. I didn't do that just to make it simple.

![24](./img/hack2020/24.jpg)

After created de index.html. Open the index.html with notepad or any editer. 

Edit this code in index.html

```
<!DOCTYPE html>
<meta charset="utf-8" />
<title>WebSocket Test</title>
 
<script language="javascript" type="text/javascript">
 
var url = "ws://192.168.4.1:1337/";
var output;
var button;
var canvas;
var context;
 
// This is called when the page finishes loading
function init() {
 
    // Assign page elements to variables
    button = document.getElementById("toggleButton");
    output = document.getElementById("output");
    canvas = document.getElementById("led");
    
    // Draw circle in canvas
    context = canvas.getContext("2d");
    context.arc(25, 25, 15, 0, Math.PI * 2, false);
    context.lineWidth = 3;
    context.strokeStyle = "black";
    context.stroke();
    context.fillStyle = "black";
    context.fill();
    
    // Connect to WebSocket server
    wsConnect(url);
}
 
// Call this to connect to the WebSocket server
function wsConnect(url) {
    
    // Connect to WebSocket server
    websocket = new WebSocket(url);
    
    // Assign callbacks
    websocket.onopen = function(evt) { onOpen(evt) };
    websocket.onclose = function(evt) { onClose(evt) };
    websocket.onmessage = function(evt) { onMessage(evt) };
    websocket.onerror = function(evt) { onError(evt) };
}
 
// Called when a WebSocket connection is established with the server
function onOpen(evt) {
 
    // Log connection state
    console.log("Connected");
    
    // Enable button
    button.disabled = false;
    
    // Get the current state of the LED
    doSend("getLEDState");
}
 
// Called when the WebSocket connection is closed
function onClose(evt) {
 
    // Log disconnection state
    console.log("Disconnected");
    
    // Disable button
    button.disabled = true;
    
    // Try to reconnect after a few seconds
    setTimeout(function() { wsConnect(url) }, 2000);
}
 
// Called when a message is received from the server
function onMessage(evt) {
 
    // Print out our received message
    console.log("Received: " + evt.data);
    
    // Update circle graphic with LED state
    switch(evt.data) {
        case "0":
            console.log("LED is off");
            context.fillStyle = "black";
            context.fill();
            break;
        case "1":
            console.log("LED is on");
            context.fillStyle = "red";
            context.fill();
            break;
        default:
            break;
    }
}
 
// Called when a WebSocket error occurs
function onError(evt) {
    console.log("ERROR: " + evt.data);
}
 
// Sends a message to the server (and prints it to the console)
function doSend(message) {
    console.log("Sending: " + message);
    websocket.send(message);
}
 
// Called whenever the HTML button is pressed
function onPress() {
    doSend("toggleLED");
    doSend("getLEDState");
}
 
// Call the init function as soon as the page loads
window.addEventListener("load", init, false);
 
</script>
 
<h2>LED Control</h2>
 
<table>
    <tr>
        <td><button id="toggleButton" onclick="onPress()" disabled>Toggle LED</button></td>
        <td><canvas id="led" width="50" height="50"></canvas></td>
    </tr>
</table>
```

Save this and go the Arduino IDE. The ESP32 shoulde be plugged in.
Go to Tools-> Click ESP32 Sketch Data Upload

![25](./img/hack2020/25.jpg)


By clicking that the image will upload. The SPIFFS succesfully uploaded

![26](./img/hack2020/26.jpg)


**Run it!**

With the index.html file uploaded and the Arduino code running, you should be able to connect to the ESP32’s access point. 
Using the computer, search for open WiFi access points and connect to the one named ESP32-AP. When asked for a password, enter "letmeinplz" (or whatever you set the AP password to in the Arduino code).

![27](./img/hack2020/27.jpg)

So with this I had alot of problems . My ESP32 acces point never connected. So I could open my page, but my Led Control wasn't working. SoI keep on trying to connect my ESP32. 
I search my problem and it said I should put my laptop on airplane mode. I did that and my wifi was also on  and then after that my Led Control worked. It was just a simple page. I will try to put a CSS file in it.

Clicking on Toggle Led. The Led on the ESP32 will go on and off


Black means On

![28](./img/hack2020/28.jpg)

![30](./img/hack2020/30.jpg)

Red means Off

![29](./img/hack2020/29.jpg)

![31](./img/hack2020/31.jpg)


### Display a Text On The Oled with OTA

I wanted to try display a text on the Oled and I did some research. I first downloaded a file that I used for the Oled. I search for 

ESP32-OLED0.96-ssd1306 and downloaded it.

![32](./img/hack2020/32.jpg)

I added this file in my Arduino Folder that is in my Documents.


After adding the File I open it aa
![33](./img/hack2020/33.jpg)


Before Uploading the sketch I need to install the library for the Oled. I had already installed the library, but I will put the names for the library that I used.

To install the library for the OLED display I clicked on Sketch-> Include Library-> Manage Library

![34](./img/hack2020/34.jpg)


Then I searched for SSD1306 Library and installed it:

![35](./img/hack2020/35.jpg)


I wanted the Oled to work over OTA. So I had to Combine the SSD1306SimpleDemo Sketch into my BasicOtA Sketch. This is the same sketch I used to blink the LED.
I combined to sketches into one.

*The complete sketch for the Oled with OTA*

```
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include "images.h"

const char* ssid = "telew_f28";
const char* password = "2c0bbba8";

//variabls for blinking an LED with Millis
const int led = 16; // ESP32 Pin to which onboard LED is connected
unsigned long previousMillis = 0;  // will store last time LED was updated
const long interval = 1000;  // interval at which to blink (milliseconds)
int ledState = LOW;  // ledState used to set the LED

SSD1306  display(0x3c, 5, 4);

#define DEMO_DURATION 3000
typedef void (*Demo)(void);

int demoMode = 0;
int counter = 1;

void setup() {
  
  // Initialising the UI will init the display too.
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  pinMode(led, OUTPUT);
  
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void drawFontFaceDemo() {
    // Font Demo1
    // create more fonts at http://oleddisplay.squix.ch/
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "Hello world");
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 10, "Hello world");
    display.setFont(ArialMT_Plain_24);
    display.drawString(0, 26, "Shewishka");
}

void drawTextFlowDemo() {
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawStringMaxWidth(0, 0, 128,
      "Lorem ipsum\n dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore." );
}

void drawTextAlignmentDemo() {
    // Text alignment demo
  display.setFont(ArialMT_Plain_10);

  // The coordinates define the left starting point of the text
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 10, "Left aligned (0,10)");

  // The coordinates define the center of the text
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 22, "Center aligned (64,22)");

  // The coordinates define the right end of the text
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(128, 33, "Right aligned (128,33)");
}

void drawRectDemo() {
      // Draw a pixel at given position
    for (int i = 0; i < 10; i++) {
      display.setPixel(i, i);
      display.setPixel(10 - i, i);
    }
    display.drawRect(12, 12, 20, 20);

    // Fill the rectangle
    display.fillRect(14, 14, 17, 17);

    // Draw a line horizontally
    display.drawHorizontalLine(0, 40, 20);

    // Draw a line horizontally
    display.drawVerticalLine(40, 0, 20);
}

void drawCircleDemo() {
  for (int i=1; i < 8; i++) {
    display.setColor(WHITE);
    display.drawCircle(32, 32, i*3);
    if (i % 2 == 0) {
      display.setColor(BLACK);
    }
    display.fillCircle(96, 32, 32 - i* 3);
  }
}

void drawProgressBarDemo() {
  int progress = (counter / 5) % 100;
  // draw the progress bar
  display.drawProgressBar(0, 32, 120, 10, progress);

  // draw the percentage as String
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 15, String(progress) + "%");
}

void drawImageDemo() {
    // see http://blog.squix.org/2015/05/esp8266-nodemcu-how-to-create-xbm.html
    // on how to create xbm files
    display.drawXbm(34, 14, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
}

Demo demos[] = {drawFontFaceDemo, drawTextFlowDemo, drawTextAlignmentDemo, drawRectDemo, drawCircleDemo, drawProgressBarDemo, drawImageDemo};
int demoLength = (sizeof(demos) / sizeof(Demo));
long timeSinceLastModeSwitch = 0;


void loop() {
  ArduinoOTA.handle();
    
//loop to blink without delay
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
  // save the last time you blinked the LED
  previousMillis = currentMillis;
  // if the LED is off turn it on and vice-versa:
  ledState = not(ledState);
  // set the LED with the ledState of the variable:
  digitalWrite(led,  ledState);
  }
  
    // clear the display
  display.clear();
  // draw the current demo method
  demos[demoMode]();

  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(10, 128, String(millis()));
  // write the buffer to the display
  display.display();

  if (millis() - timeSinceLastModeSwitch > DEMO_DURATION) {
    demoMode = (demoMode + 1)  % demoLength;
    timeSinceLastModeSwitch = millis();
  }
  counter++;
  delay(10);
}
```

After doing this I selected my board:*ESP32 Dev Module* and I selected my Port. Then I compiled and uploaded my Sketch. 
While uploading I got alot of errors in my code so I had to reorder my code till it was fine to upload. 

Then finally I uploaded my code. And I was glad it worked.

![36](./img/hack2020/36.jpg)


### ESP32-CAM

The ESP32-CAM is a very low cost small camera module with the ESP32-S chip  
Besides the OV2640 camera, and several GPIOs to connect peripherals, it also features a microSD card slot that can be useful to store images taken with the camera or to store files to serve to clients.

![37](./img/hack2020/37.jpg)

![38](./img/hack2020/38.jpg)

The ESP32-CAM doesn’t come with a USB connector, so you need an *FTDI programmer* to upload code through the *U0R* and *U0T* pins (serial pins).


*Features*

- The smallest 802.11b/g/n Wi-Fi BT SoC module
- Low power 32-bit CPU,can also serve the application processor
- Up to 160MHz clock speed, summary computing power up to 600 DMIPS
- Built-in 520 KB SRAM, external 4MPSRAM
- Supports UART/SPI/I2C/PWM/ADC/DAC
- Support OV2640 and OV7670 cameras, built-in flash lamp
- Support image WiFI upload
- Support TF card
- Supports multiple sleep modes
- Embedded Lwip and FreeRTOS
- Supports STA/AP/STA+AP operation mode
- Support Smart Config/AirKiss technology
- Support for serial port local and remote firmware upgrades (FOTA)


**ESP32-CAM Pinout**

![39](./img/hack2020/39.jpg)


There are three *GND pins* and two pins for power: either *3.3V* or *5V*.
*GPIO 1* and *GPIO 3* are the serial pins. You need these pins to upload code to your board. Additionally, *GPIO 0* also plays an important role, since it determines whether the ESP32 is in flashing mode or not. 
When *GPIO 0* is connected to *GND*, the ESP32 is in flashing mode.

*The following pins are internally connected to the microSD card reader:*

- GPIO 14: CLK
- GPIO 15: CMD
- GPIO 2: Data 0
- GPIO 4: Data 1 (also connected to the on-board LED)
- GPIO 12: Data 2
- GPIO 13: Data 3

**Video Streaming Server**

I have tried the video streaming server with the following steps:

1. Install ESP32 in your Arduino IDE. I already had done that.

2. *CameraWebServer Example Code*

    For this open Arduino IDE-> Go to File-> Examples-> ESP32 -> Camera and open the CameraWebServer example.
	
	![40](./img/hack2020/40.jpg)
	
	
	The following code should load:
	
	![41](./img/hack2020/41.jpg)
	
	Before uploading the code, you need to insert your network credentials in the following variables:
	
	```
	const char* ssid = "telew_f28";
    const char* password = "your password";
	````
	
	
	Then, make sure you select the right camera module. In this case, we’re using the AI-THINKER Model.
	If you are using a different model uncomment that.
	
	```
	// Select camera model
	//#define CAMERA_MODEL_WROVER_KIT
	//#define CAMERA_MODEL_ESP_EYE
	//#define CAMERA_MODEL_M5STACK_PSRAM
	//#define CAMERA_MODEL_M5STACK_WIDE
	#define CAMERA_MODEL_AI_THINKER
	```
	
	Now, the code is ready to be uploaded to your ESP32.
	

3.  *ESP32-CAM Upload Code*

    Connect the ESP32-CAM board to your computer using an FTDI programmer. I used this schematic diagram to connect the FTDI cable to my ESP32-CAM:
	
	![42](./img/hack2020/42.jpg)
	
	![43](./img/hack2020/43.jpg)
	
	
	Many FTDI programmers have a jumper that allows you to select 3.3V or 5V. Make sure the jumper is in the right place to select 5V.
	
	Here it shows how to connect the FTDI cable. *Important: GPIO 0 needs to be connected to GND so that you’re able to upload code*
	
	![44](./img/hack2020/44.jpg)
	
	
	This is how I connected my ESP32-CAM to my FTDI cable
	
	![45](./img/hack2020/45.jpg)
	
	
	This is the correct code:
	
	```
	#include "esp_camera.h"
	#include <WiFi.h>

	//
	// WARNING!!! Make sure that you have either selected ESP32 Wrover Module,
	//            or another board which has PSRAM enabled
	//

	// Select camera model
	//#define CAMERA_MODEL_WROVER_KIT
	//#define CAMERA_MODEL_ESP_EYE
	//#define CAMERA_MODEL_M5STACK_PSRAM
	//#define CAMERA_MODEL_M5STACK_WIDE
	#define CAMERA_MODEL_AI_THINKER

	#include "camera_pins.h"

	const char* ssid = "telew_f28";
	const char* password = "2c0bbba8";

	void startCameraServer();

	void setup() {
	  Serial.begin(115200);
	  Serial.setDebugOutput(true);
	  Serial.println();

	  camera_config_t config;
	  config.ledc_channel = LEDC_CHANNEL_0;
	  config.ledc_timer = LEDC_TIMER_0;
	  config.pin_d0 = Y2_GPIO_NUM;
	  config.pin_d1 = Y3_GPIO_NUM;
	  config.pin_d2 = Y4_GPIO_NUM;
	  config.pin_d3 = Y5_GPIO_NUM;
	  config.pin_d4 = Y6_GPIO_NUM;
	  config.pin_d5 = Y7_GPIO_NUM;
	  config.pin_d6 = Y8_GPIO_NUM;
	  config.pin_d7 = Y9_GPIO_NUM;
	  config.pin_xclk = XCLK_GPIO_NUM;
	  config.pin_pclk = PCLK_GPIO_NUM;
	  config.pin_vsync = VSYNC_GPIO_NUM;
	  config.pin_href = HREF_GPIO_NUM;
	  config.pin_sscb_sda = SIOD_GPIO_NUM;
	  config.pin_sscb_scl = SIOC_GPIO_NUM;
	  config.pin_pwdn = PWDN_GPIO_NUM;
	  config.pin_reset = RESET_GPIO_NUM;
	  config.xclk_freq_hz = 20000000;
	  config.pixel_format = PIXFORMAT_JPEG;
	  //init with high specs to pre-allocate larger buffers
	  if(psramFound()){
		config.frame_size = FRAMESIZE_UXGA;
		config.jpeg_quality = 10;
		config.fb_count = 2;
	  } else {
		config.frame_size = FRAMESIZE_SVGA;
		config.jpeg_quality = 12;
		config.fb_count = 1;
	  }

	#if defined(CAMERA_MODEL_ESP_EYE)
	  pinMode(13, INPUT_PULLUP);
	  pinMode(14, INPUT_PULLUP);
	#endif

	  // camera init
	  esp_err_t err = esp_camera_init(&config);
	  if (err != ESP_OK) {
		Serial.printf("Camera init failed with error 0x%x", err);
		return;
	  }

	  sensor_t * s = esp_camera_sensor_get();
	  //initial sensors are flipped vertically and colors are a bit saturated
	  if (s->id.PID == OV3660_PID) {
		s->set_vflip(s, 1);//flip it back
		s->set_brightness(s, 1);//up the blightness just a bit
		s->set_saturation(s, -2);//lower the saturation
	  }
	  //drop down frame size for higher initial frame rate
	  s->set_framesize(s, FRAMESIZE_QVGA);

	#if defined(CAMERA_MODEL_M5STACK_WIDE)
	  s->set_vflip(s, 1);
	  s->set_hmirror(s, 1);
	#endif

	  WiFi.begin(ssid, password);

	  while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	  }
	  Serial.println("");
	  Serial.println("WiFi connected");

	  startCameraServer();

	  Serial.print("Camera Ready! Use 'http://");
	  Serial.print(WiFi.localIP());
	  Serial.println("' to connect");
	}

	void loop() {
	  // put your main code here, to run repeatedly:
	  delay(10000);
	}
	```

    Now the code is ready to upload: Go Tools-> Select the Board *AI-Thinker ESP32-CAM* and make sure to slect your *Port*.
	Click upload-> When you start to see dots on the debugging window, press the ESP32-CAM on-board RST button. I didn't have to do that.
	
	If it succesfully upload. It must display the IP adress.
	
4.  *Getting the IP address*
    
	After uploading the code, disconnect *GPIO 0* from *GND*.
    Open the Serial Monitor at a baud rate of 115200. Press the ESP32-CAM on-board Reset button.
	The ESP32 IP address should be printed in the Serial Monitor.
	
	![46](./img/hack2020/46.jpg)
	
	
5.  *Accessing the Video Streaming Server*
    
	Now, you can access your camera streaming server on your local network. Open a browser and type the ESP32-CAM IP address. 
	Press the Start Streaming button to start video streaming.
	
	![47](./img/hack2020/47.jpg)
	
   
### References

[Setup ESP32](http://www.sumukhainfotech.com/installing-esp32-for-arduino-ide/) 

[ESP32 OTA programming in Arduino IDE](https://lastminuteengineers.com/esp32-ota-updates-arduino-ide/) 

[Create a webserver with ESP3](https://shawnhymel.com/1882/how-to-create-a-web-server-with-websockets-using-an-esp32-in-arduino/)

[Video streaming](https://randomnerdtutorials.com/esp32-cam-video-streaming-face-recognition-arduino-ide/) 



