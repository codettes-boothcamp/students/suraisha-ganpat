var Accessory = require('../').Accessory;
var Service = require('../').Service;
var Characteristic = require('../').Characteristic;
var uuid = require('../').uuid;
var mqtt = require('mqtt');

var State = false;

//change this//
var name = "Dashbutton3";
var UUID = "hap-nodejs:accessories:dashbutton3";
var USERNAME = "22:3C:3A:3B:A3:A4";
var MQTT_IP = 'localhost'
var topic = '/home/dashbutton3/state'
//change this//


var sensorUUID = uuid.generate(UUID);
var Sensor = exports.accessory = new Accessory(name, sensorUUID);


//MQTT Setup
var options = {
        port: 1883,
        host: MQTT_IP,
        clientId: 'c42'
};

var SENSOR = {
        currentState: 0,
        getState: function(){
                return SENSOR.currentState;
        }
}

var client = mqtt.connect(options);
client.subscribe(topic);
client.on('message', function(topic, message) {
        message = message.toString();
        if(message.includes('1')){
                SENSOR.currentState = 1;
        }
        else{
                SENSOR.currentState = 0;
        }

        Sensor.getService(Service.StatelessProgrammableSwitch).setCharacteristic(Characteristic.ProgrammableSwitchEven$
});


Sensor.addService(Service.StatelessProgrammableSwitch).getCharacteristic(Characteristic.ProgrammableSwitchEvent).on('g$
        SENSOR.getState();
        callback(null, SENSOR.currentState);
});
