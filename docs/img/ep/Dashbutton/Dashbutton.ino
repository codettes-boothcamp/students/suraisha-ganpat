#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "XXX";
const char* password = "XXX";
const char* mqtt_server = "192.168.2.120";

const char* mqtt_topic = "/dashbutton/#";
const char* mqtt_topic_state = "/dashbutton/state";
const char* mqtt_client = "dashbutton";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, LOW);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    if (millis() > 6000) exit();
  }

  client.setServer(mqtt_server, 1883);

  while (!client.connected()) {
    if (client.connect(mqtt_client)) {
      client.subscribe(mqtt_topic_state);
      delay(300);

      client.publish(mqtt_topic_state, "1");

      for (int i = 0; i < 5; i++) {
        digitalWrite(BUILTIN_LED, LOW);
        delay(30);
        digitalWrite(BUILTIN_LED, HIGH);
        delay(20);
      }
      ESP.deepSleep(0);
    }

    if (millis() > 6000) exit();
  }
}

void loop() {
  //never reaching this
}

void exit() {
  for (int i = 0; i < 3; i++) {
    digitalWrite(BUILTIN_LED, LOW);
    delay(100);
    digitalWrite(BUILTIN_LED, HIGH);
    delay(50);
  }
  ESP.deepSleep(0);
}
