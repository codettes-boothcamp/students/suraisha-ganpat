![suraisha](img/index/suraisha.jpg)

# About me

My name is Shewishka Ganpat. I am 23 years old. I study Mining and Resource Engineering at IBW University. And I am in my final year of my study. 
I find it important that women do know about ICT. And thats why I am here, to be an example for other women. And so we can make products thats can be effectivily used.

## Final Project

As final project I thought of making an emergency keycahin that can be usefull for children to give an alarm to their parents or any other person that can help them

I want the keychain to do is:

- Send their location
- Give parents a notification that they are in trouble

To make it more interesting I thought it should have other features. Such as sending messages, show the time, show loaction, maybe show the temperature.
It should have the option to insert a simcard to make it connect to the other person phone. It requires bluetooth or wifi for the loaction
It will work on batteries. My idea of making this is to help people who are in trouble and get help as soon as possible. 
Because out of experience I say that sometimes you do need help but can do nothing to tell someone or to make contact with them. 
Because at that moment you have nothing with you.


### Business Model Canvas

![BM](img/index/BM.jpg)

[Business Model Canvas](https://canvanizer.com/canvas/wkiTD3gSAOZDI) 

### Photoslide

![15](img/index/15.jpg)

[Safetywish Slides](https://docs.google.com/presentation/d/1D-x6CQ9BqOJpb1h5seAl2Xf2dKxyEYmSZDvzpD_BMBI/edit#slide=id.g9dfed48629_0_37) 



### Hack 'O mation 2020

Project Hack O'mation: **hydropower plant**

Electricity can be generated in different ways. One of the ways is through hydropower.
We are building a small bluewater Hydropower that can be use by everyone. It will be inexpensive so everybody can buy it.
This will be use to generate electricity for small devices when you are outdoor. This can be very usefull when you are at a place with no electricity. It has a magnet coupling so it
is easy to switch propellers.

**Logo**

![1](img/index/1.jpg)

**Problem**

General Problem: No Electricity outdoors.

For tourists: Most of the time when tourists go camping or there is no electricity. You start panicking about a low battery and want to charge your phone. And there is no electricity.


**Solutions**

Building a water tribune that will generate electricity through the flow of water. It can charge small devices easily. And it is nature friendly. 

**Features**

- 24 hour of power
- It stores energy

**Unique Values**

- Affordable IoT Solutions
- Convenience: It is easy to use
- It is small so easy to transport


### The product

After designing, printing, building our stack and made our electronic design we set everuthing together. We made sure that the DC motor is waterproof. So there won't go any water in it. We also made the floats for the waterturbine.

![70](img/index/70.jpg)

![71](img/index/71.jpg)


**The Poster**

![72](img/index/72.jpg)




